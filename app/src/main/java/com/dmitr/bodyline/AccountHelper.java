package com.dmitr.bodyline;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dmitr.bodyline.Auth.Restore;
import com.dmitr.bodyline.fragments.FragmentAbonement;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm.FragmentAlarm;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.DoPosle.FragmentDoPosle;
import com.dmitr.bodyline.fragments.FragmentInfo;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Profile.FragmentProfile;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Settings.FragmentSettings;
import com.dmitr.bodyline.fragments.FragmentSupport;

public class AccountHelper extends AppCompatActivity {

    FrameLayout container;
    Integer selected;

    Fragment fragment;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_helper);

        container = (FrameLayout) findViewById(R.id.main_container);

        selected = getIntent().getIntExtra("selected", 0);

        fragmentManager = getSupportFragmentManager();

        switch (selected) {
            case 0:
                fragment = new FragmentProfile();
                break;
            case 1:
                fragment = new FragmentAbonement();
                break;
            case 2:
                fragment = new FragmentAlarm();
                break;
            case 3:
                fragment = new FragmentDoPosle();
                break;
            case 4:
                fragment = new FragmentInfo();
                break;
            case 5:
                fragment = new FragmentSettings();
                break;
            case 6:
                fragment = new FragmentSupport();
                break;
            default:
                break;
        }

        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();

    }

    public void startChangePassword(View v) {
        Intent intent = new Intent(this, Restore.class);
        startActivity(intent);
    }

    public void startChangePhone(View v) {
        //Intent intent = new Intent(this, Restore.class);
        //startActivity(intent);
    }

}
