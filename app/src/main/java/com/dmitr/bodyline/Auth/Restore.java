package com.dmitr.bodyline.Auth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import cz.msebera.android.httpclient.Header;

public class Restore extends AppCompatActivity {

    ConstraintLayout phoneEnterLayout;
    ConstraintLayout changeLayout;

    String token;

    Button sendSmsBtn;

    MaskedEditText phoneField;
    EditText hashField;
    EditText newPassField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore);

        phoneEnterLayout = findViewById(R.id.phoneEnterLayout);

        changeLayout = findViewById(R.id.changeLayout);
        changeLayout.setVisibility(View.INVISIBLE);

        phoneField = findViewById(R.id.passPhoneField);
        hashField = findViewById(R.id.hashField);
        newPassField = findViewById(R.id.newPassField);

        sendSmsBtn = findViewById(R.id.sendSmsButton);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");
    }

    public void close(View v) {
        finish();
    }

    public void sendSms(View v) {
        ((Button)v).setEnabled(false);
        runSendSms();
    }

    public void resendTapped(View v) {
        runSendSms();
    }

    private void runSendSms() {

        String phone = phoneField.getText().toString();
        String clearPhone = phone.replaceAll("[\\(\\)\\-\\+]","");

        sendSms(token, clearPhone);

    }

    private void sendSms(String token, String phone) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        params.put("phone", phone);

        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/requestpass", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                sendSmsBtn.setEnabled(true);
                if (statusCode == 200){
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){
                            phoneEnterLayout.setVisibility(View.INVISIBLE);
                            changeLayout.setVisibility(View.VISIBLE);
                        }
                        else {
                            showErrID(err_id, sendSmsBtn);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                    Log.d("Restore:",""+json);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                String Message = "Интернет соединение отсутствует";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sendSmsBtn.setEnabled(true);
                            }
                        });

                AlertDialog alert = builder.create();
                if (!isFinishing()) {
                    alert.show();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }

        });

    }


    public void changeTapped(View v) {
        runUpdatePass();
    }

    private void runUpdatePass() {

        updatePass(token, phoneField.getText().toString(), newPassField.getText().toString(), hashField.getText().toString());

    }

    private void updatePass(String token, String phone, String newPass, String secret) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final String clearPhone = phone.replaceAll("[\\(\\)\\-\\+]","");

        params.put("phone", clearPhone);
        params.put("newpass", newPass);
        params.put("secret", secret);

        Log.d("Rstore", "phone: " + phone + ", newpass: " + newPass + ", secret: " + secret);

        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/updatepass", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                sendSmsBtn.setEnabled(true);
                if (statusCode == 200){
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){
                            String Message = "Ваш пароль успешно изменен. Войдите в систему используя новый пароль.";
                            builder.setTitle("Успешно")
                                    .setMessage(Message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }
                        }
                        else {
                            showErrID(err_id, sendSmsBtn);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                    Log.d("Restore:",""+json);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                String Message = "Интернет соединение отсутствует";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sendSmsBtn.setEnabled(true);
                            }
                        });

                AlertDialog alert = builder.create();
                if (!isFinishing()) {
                    alert.show();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }

        });

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("Login:","Ошибка работы токена. Переход в Register.java");
                        //Intent intent = new Intent(getApplicationContext(), Register.class);

                        //startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

}
