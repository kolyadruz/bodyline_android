package com.dmitr.bodyline.Auth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;

import com.dmitr.bodyline.Auth.Chooser.Chooser;
import com.dmitr.bodyline.BuildConfig;
import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Profile.ProfileActivity;
import com.dmitr.bodyline.OFFLINE.AppModeChooser;
import com.dmitr.bodyline.R;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import permission.auron.com.marshmallowpermissionhelper.ActivityManagePermission;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;

public class AutoLogin extends ActivityManagePermission {

    String token;
    String s_token;

    String platform;
    String version;
    String timezone;

    String fbtoken;

    @Override
    protected void onStart() {
        super.onStart();
        askCompactPermissions(new String[]{PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE}, new PermissionResult() {
            @Override
            public void permissionGranted() {
                startApp();
            }

            @Override
            public void permissionDenied() {
                showDialog();
            }

            @Override
            public void permissionForeverDenied() {
                showDialog();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_login);

        FirebaseApp.initializeApp(getApplicationContext());

    }

    @Override
    protected void onResume() {
        super.onResume();

        String tkn = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        String msg = getString(R.string.msg_token_fmt, tkn);
        fbtoken = tkn;

        sendFBToken(fbtoken);

        Log.d("AutoLogin", "Resumed");
    }

    private void sendFBToken(final String fbtoken) {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String tkn = mSettings.getString("token", "");

        Handler mainHandler = new Handler(Looper.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                //Code that uses AsyncHttpClient in your case ConsultaCaract()

                AsyncHttpClient client = new AsyncHttpClient();
                final RequestParams params = new RequestParams();

                client.addHeader("Authorization","Bearer "+tkn);
                params.add("fb_token", fbtoken);
                client.post("http://bodyline14.ru/mobile/updatefbtoken", params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        // called before request is started
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        // called when response HTTP status is "200 OK"
                        if (statusCode == 200){
                            String str = new String(responseBody);
                            try {
                                final JSONObject json = new JSONObject(str);
                                Integer err_id = json.getInt("err_id");
                                if (err_id == 0){
                                    Log.d("FB INSTANCE SERVICE", "FBTOKEN UPDATED: " + fbtoken);
                                } else {

                                }
                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        // called when request is retried
                    }
                });

            }
        };
        mainHandler.post(myRunnable);
    }

    private void startApp() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        if (token.equals("")) {
            Log.d("AutoLogin", "Токен не найден. Переход в Login");

            //Intent intent = new Intent(getApplicationContext(), Login.class);
            Intent intent = new Intent(getApplicationContext(), Chooser.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            startActivity(intent);
        } else {
            Log.d("AutoLogin", "Токен найден "+ token +". Запуск проверки токена...");
            runCheckLogin();
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(AutoLogin.this);
        builder.setTitle("Внимание");
        builder.setMessage("Для корректной работы приложения требуются все необходимые разрешения. Пожалуйста, перейдите в настройки и предоставьте все запрашиваемые разрешения.");
        builder.setPositiveButton("Перейти", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                openSettingsApp(AutoLogin.this);
            }
        });
        builder.setNegativeButton("Отмена",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                AutoLogin.this.finish();
            }
        });

        if (!isFinishing()) {
            builder.show();
        }
    }

    private void runCheckLogin() {

        platform = getResources().getString(R.string.platform);
        version = BuildConfig.VERSION_NAME;
        timezone = String.valueOf(TimeZone.getDefault().getID());

        checkLogin(token, timezone, version, platform);

    }

    private void checkLogin(final String token, final String timezone, String version, String platform) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final RequestParams params = new RequestParams();
        params.put("platform", platform);
        params.put("version", version);
        params.put("timezone", timezone);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/autologin", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200){
                    String str = new String(responseBody);
                    try {
                        final JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){
                            try {
                                final Integer opt = json.getInt("opt");

                                AsyncHttpClient clientGetSeason = new AsyncHttpClient();
                                clientGetSeason.addHeader("Authorization","Bearer "+token);
                                clientGetSeason.post("http://bodyline14.ru/mobile/getpurchasedseasontoken", new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        if (statusCode == 200) {
                                            String str = new String(responseBody);
                                            try {
                                                JSONObject json = new JSONObject(str);
                                                Integer err_id = json.getInt("err_id");
                                                if (err_id == 0) {
                                                    s_token = json.getString("s_token");
                                                }
                                                Log.d("s_token", ""+json);
                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                            }

                                            if (opt > 2) {

                                                try{

                                                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                                    SharedPreferences.Editor editor = mSettings.edit();

                                                    //String s_token = json.getString("s_token");
                                                    String phone = json.getString("phone");

                                                    editor.putString("s_token", s_token);
                                                    editor.putString("phone", phone);

                                                    String rowData = json.getString("data");
                                                    JSONObject data;
                                                    data = new JSONObject(rowData);

                                                    String fam = data.getString("fam");
                                                    String imya = data.getString("imya");
                                                    String otch = data.getString("otch");
                                                    Integer gender = data.getInt("gender");
                                                    String birthday = data.getString("birthday");
                                                    Integer fam_status = data.getInt("fam_status");
                                                    String work = data.getString("work");
                                                    Integer childs = data.getInt("childs");
                                                    String address = data.getString("address");
                                                    Integer lifestyle = data.getInt("lifestyle");
                                                    Integer age = data.getInt("age");
                                                    Integer isOffline = json.getInt("isOffline");

                                                    editor.putInt("isOffline", isOffline);
                                                    editor.putString("fam", fam);
                                                    editor.putString("imya", imya);
                                                    editor.putString("otch", otch);
                                                    editor.putInt("gender", gender);
                                                    editor.putString("birthday", birthday);
                                                    editor.putInt("fam_status", fam_status);
                                                    editor.putString("work", work);
                                                    editor.putInt("childs", childs);
                                                    editor.putString("address", address);
                                                    editor.putInt("lifestyle", lifestyle);
                                                    editor.putInt("age", age);

                                                    editor.commit();

                                                    Log.d("Array: ", ""+fam);

                                                    //Intent intent = new Intent(getApplicationContext(), Main.class);

                                                    //if (isOffline == 1) {

                                                        Intent intent = new Intent(getApplicationContext(), AppModeChooser.class);

                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                                        intent.putExtra("opt", opt);

                                                        startActivity(intent);

                                                    /*} else {

                                                        Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);

                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                                        intent.putExtra("opt", opt);

                                                        startActivity(intent);

                                                    }*/

                                                } catch (Exception ex){
                                                    ex.printStackTrace();
                                                }

                                            } else {

                                                //showOptID(opt);

                                                switch (opt) {
                                                    case 1:
                                                        try {

                                                            String phone = json.getString("phone");
                                                            Intent activation = new Intent(getApplicationContext(), Activation.class);
                                                            activation.putExtra("phone", phone);
                                                            activation.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            activation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            activation.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                            startActivity(activation);

                                                        } catch (JSONException ex) {
                                                            ex.printStackTrace();
                                                        }
                                                        break;
                                                    case 2:

                                                        //Intent infoPage = new Intent(getApplicationContext(), Main.class);

                                                        Intent infoPage = new Intent(getApplicationContext(), MainWithBottom.class);

                                                        infoPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        infoPage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        infoPage.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                        startActivity(infoPage);

                                                        break;
                                                    default:
                                                        break;
                                                }

                                            }
                                        }
                                        Log.d("AutoLogin:",""+json);
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        if (!isFinishing()) {
                                            String Message = "Интернет соединение отсутствует";
                                            builder.setMessage(Message)
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {

                                                        }
                                                    });

                                            AlertDialog alert = builder.create();
                                            if (!isFinishing()) {
                                                alert.show();
                                            }
                                        }
                                    }
                                });

                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                        else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                    Log.d("timezone",""+timezone);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("AutoLogin:", "Ошибка работы токена. Переход в Login.java");
                        //Intent intent = new Intent(getApplicationContext(), Login.class);
                        Intent intent = new Intent(getApplicationContext(), Chooser.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                        startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

    private void showOptID(final Integer opt) {
        AlertDialog.Builder builderopt = new AlertDialog.Builder(this);

        String optMessage = "";

        if (opt == 1) {
            optMessage = "Активация профиля";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Autologin", "Ошибок нет. Переход в Activation.java");
                            Intent intent = new Intent(getApplicationContext(), Activation.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        } else if (opt == 2) {
            optMessage = "Основные данные не заполнены";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            /*Log.d("Autologin", "Ошибок нет. Переход в Main.java");
                            //Intent intent = new Intent(getApplicationContext(), Main.class);
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);*/

                            Log.d("Autologin", "Ошибок нет. Переход в ProfileActivity.java");
                            //Intent intent = new Intent(getApplicationContext(), Main.class);
                            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        }  else if (opt == 3) {
            optMessage = "Все данные есть, но не куплен абонемент";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Main.java");
                            //Intent intent = new Intent(getApplicationContext(), Main.class);
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        } else if (opt == 4) {
            optMessage = "Все данные есть, куплен абонемент";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Main.java");
                            //Intent intent = new Intent(getApplicationContext(), Main.class);
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        }
        AlertDialog alert = builderopt.create();
        if(!isFinishing()) {
            alert.show();
        }
    }


}
