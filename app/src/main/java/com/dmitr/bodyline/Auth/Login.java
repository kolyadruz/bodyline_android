package com.dmitr.bodyline.Auth;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.BuildConfig;
import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.OFFLINE.AppModeChooser;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.TimeZone;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import cz.msebera.android.httpclient.Header;

public class Login extends AppCompatActivity {

    //EditText phoneField;
    MaskedEditText phoneField;
    EditText passField;

    Button enterButton;
    Button createButton;

    String platform;
    String version;
    String timezone;

    String token;

    Handler mHandler;

    Integer isOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mHandler = new Handler();

        platform = getResources().getString(R.string.platform);
        version = BuildConfig.VERSION_NAME;
        timezone = String.valueOf(TimeZone.getDefault().getID());

        phoneField = findViewById(R.id.phoneField);

        passField = findViewById(R.id.passField);

        enterButton = findViewById(R.id.enterButton);
        createButton = findViewById(R.id.createButton);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isOffline = mSettings.getInt("isOffline", 0);

    }

    public void enter(View view) {
        view.setEnabled(false);

        if (phoneField.length() < 16) {
            if (passField.length() == 0) {
                showErrID(4, enterButton);
            } else {
                showErrID(3, enterButton);
            }
        } else {
            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = mSettings.edit();
            final String phone = phoneField.getText().toString();
            final String clearPhone = phone.replaceAll("[\\(\\)\\-\\+]","");
            String pass = passField.getText().toString();
            editor.putString("phone", clearPhone);
            editor.commit();
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("phone", clearPhone);
            params.put("pass", pass);
            params.put("platform", platform);
            params.put("version", version);
            params.put("timezone", timezone);
            client.post("http://bodyline14.ru/mobile/login", params, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    // called before request is started
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    // called when response HTTP status is "200 OK"
                    if (statusCode == 200) {
                        String str = new String(responseBody);
                        JSONObject json = new JSONObject();
                        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = mSettings.edit();
                        try {
                            json = new JSONObject(str);
                            Integer err_id = json.getInt("err_id");

                            Log.d("LoginResponse:", ""+json);

                            if (err_id == 0) {
                                try {

                                    token = json.getString("token");

                                    editor.putString("token", token);
                                    editor.commit();

                                    Integer opt = json.getInt("opt");

                                    try {

                                        String s_token = json.getString("s_token");
                                        String token = json.getString("token");
                                        String fb_token = json.getString("fb_token");
                                        String avatar = json.getString("avatar");

                                        String rodata = json.getString("data");
                                        JSONObject data = new JSONObject();
                                        data = new JSONObject(rodata);

                                        String fam = data.getString("fam");
                                        String imya = data.getString("imya");
                                        String otch = data.getString("otch");
                                        Integer gender = data.getInt("gender");
                                        String birthday = data.getString("birthday");
                                        Integer fam_status = data.getInt("fam_status");
                                        String work = data.getString("work");
                                        Integer childs = data.getInt("childs");
                                        String address = data.getString("address");
                                        Integer lifestyle = data.getInt("lifestyle");
                                        Integer age = data.getInt("age");


                                        isOffline = json.getInt("isOffline");


                                        editor.putInt("isOffline", isOffline);
                                        editor.putString("s_token", s_token);
                                        editor.putString("token", token);
                                        editor.putString("fb_token", fb_token);
                                        editor.putString("avatar", avatar);
                                        editor.putString("fam", fam);
                                        editor.putString("imya", imya);
                                        editor.putString("otch", otch);
                                        editor.putInt("gender", gender);
                                        editor.putString("birthday", birthday);
                                        editor.putInt("fam_status", fam_status);
                                        editor.putString("work", work);
                                        editor.putInt("childs", childs);
                                        editor.putString("address", address);
                                        editor.putInt("lifestyle", lifestyle);
                                        editor.putInt("age", age);

                                        editor.commit();

                                        Picasso.get().load(avatar).into(getTarget(avatar, getApplicationContext(), opt, enterButton, clearPhone));

                                    } catch (JSONException ex){

                                        ex.printStackTrace();
                                        showOptID(opt, enterButton, clearPhone, isOffline);

                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                showErrID(err_id, enterButton);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        Log.d("Login:", "" + json);
                    }

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    String Message = "Интернет соединение отсутствует";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    enterButton.setEnabled(true);
                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!isFinishing()) {
                        alert.show();
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    // called when request is retried
                }
            });
        }

    }

    private Target getTarget(final String url, final Context ctx, final int opt, final Button btn, final String phone) {

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                mHandler.post(new Runnable() {
                      @Override
                      public void run() {

                          //File file = new File(Environment.getExternalStorageDirectory().getPath() + "/avatar.jpg");
                          File file = new File(ctx.getFilesDir() + "/avatar.jpg");

                          Log.d("Login", "try to save ava to: " + ctx.getFilesDir() + "/avatar.jpg");

                          try {
                              file.createNewFile();
                              FileOutputStream ostream = new FileOutputStream(file);

                              bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                              ostream.flush();
                              ostream.close();

                              showOptID(opt, btn, phone, isOffline);

                          } catch (IOException ex) {
                              ex.printStackTrace();
                          }

                      }
                  });
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }

    public void close(View v) {
        this.finish();
    }

    public void restoreTapped(View v) {
        Intent intent = new Intent(this, Restore.class);
        startActivity(intent);
    }

    public void enterCreate(View view) {
        ((Button) view).setEnabled(false);

        Log.d("Login", "Ошибок нет. Переход в Register.java");
        //Intent intent = new Intent(getApplicationContext(), Register.class);

        //startActivity(intent);
        //((Chooser)getParent()).needToStartReg = true;

        Intent intent = new Intent();
        intent.putExtra("needToStartReg", true);
        setResult(RESULT_OK, intent);
        this.finish();

    }

    public void onBackPressed(){
        //super.onBackPressed();
        //Intent intent = new Intent(getApplicationContext(), Register.class);
        //startActivity(intent);
        Intent intent = new Intent();
        intent.putExtra("needToStartReg", true);
        setResult(RESULT_CANCELED, intent);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("Login:","Ошибка работы токена. Переход в Register.java");
                        //Intent intent = new Intent(getApplicationContext(), Register.class);

                        //startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    public void showOptID(final Integer opt, Button optbutton, String phone, Integer isOffline) {

        optbutton.setEnabled(true);

        if (opt > 2) {

            /*Intent intent = new Intent(Login.this, Main.class);
            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("opt",opt);

            startActivity(intent);*/

            //if (isOffline == 1) {

                Intent intent = new Intent(getApplicationContext(), AppModeChooser.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("opt", opt);

                startActivity(intent);

            /*} else {

                Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("opt", opt);

                startActivity(intent);

            }*/


        } else {
            switch (opt) {
                case 1:

                    Intent activation = new Intent(Login.this, Activation.class);
                    activation.putExtra("phone", phone);
                    //activation.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //activation.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //activation.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    startActivity(activation);

                    break;
                case 2:

                    //Intent infoPage = new Intent(Login.this, Main.class);

                    if (isOffline == 1) {

                        Intent intent = new Intent(getApplicationContext(), AppModeChooser.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("opt", opt);

                        startActivity(intent);

                    } else {

                        Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("opt", opt);

                        startActivity(intent);

                    }

                    break;
                default:
                    break;
            }
        }
    }

}