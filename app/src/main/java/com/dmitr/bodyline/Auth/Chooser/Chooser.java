package com.dmitr.bodyline.Auth.Chooser;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.dmitr.bodyline.Auth.Login;
import com.dmitr.bodyline.Auth.Register;
import com.dmitr.bodyline.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class Chooser extends AppCompatActivity {

    LayoutInflater layoutInflater;

    ViewPager pager;
    MyPagerAdapter pagerAdapter;
    List<View> pages = new ArrayList<View>();

    //PermissionManager permissionManager;

    Button logBtn;
    Button regBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);


        layoutInflater = getLayoutInflater();

        logBtn = findViewById(R.id.logBtn);
        regBtn = findViewById(R.id.regBtn);


        pager = findViewById(R.id.viewPager);
        pagerAdapter = new MyPagerAdapter(pages);

        pager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(pager, true);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        logBtn.setEnabled(true);
        regBtn.setEnabled(true);

        pages.clear();

        View page1 = layoutInflater.inflate(R.layout.page_only_logo, null);
        pages.add(page1);

        View page2 = layoutInflater.inflate(R.layout.page_image_text, null);
        ImageView img2 = (ImageView)page2.findViewById(R.id.imgView);
        img2.setImageResource(R.drawable.first);
        TextView title2 = (TextView)page2.findViewById(R.id.title);
        title2.setText("Тренируйся везде");
        TextView text2 = (TextView)page2.findViewById(R.id.text);
        text2.setText("У многих людей не хватает времени на тренировки в зале. " +
                "В этой ситуации наше приложение может стать для вас настоящим спасением, " +
                "ведь наш личный онлайн-тренер всегда с вами в вашем телефоне.");
        pages.add(page2);

        View page3 = layoutInflater.inflate(R.layout.page_image_text, null);
        ImageView img3 = (ImageView)page3.findViewById(R.id.imgView);
        img3.setImageResource(R.drawable.fourth);
        TextView title3 = (TextView)page3.findViewById(R.id.title);
        title3.setText("ЕДИНОМЫШЛЕННИКИ");
        TextView text3 = (TextView)page3.findViewById(R.id.text);
        text3.setText("Чат с кураторами и участниками, где вы делитесь своими достижениями" +
                " и видите результаты других.");
        pages.add(page3);

        View page4 = layoutInflater.inflate(R.layout.page_image_text, null);
        ImageView img4 = (ImageView)page4.findViewById(R.id.imgView);
        img4.setImageResource(R.drawable.third);
        TextView title4 = (TextView)page4.findViewById(R.id.title);
        title4.setText("ДАВАЙ НАЧНЕМ ЭТОТ ПУТЬ ВМЕСТЕ");
        TextView text4 = (TextView)page4.findViewById(R.id.text);
        text4.setText("С тебя только желание стать лучше, а мотивацию, " +
                "план онлайн-тренировок и питание мы берём на себя.");
        pages.add(page4);

        pagerAdapter.notifyDataSetChanged();
    }

    public void regTapped(View v) {
        regBtn.setEnabled(false);
        Intent intent = new Intent(this, Register.class);
        startActivityForResult(intent, 1);
    }

    public void logTapped(View v) {
        logBtn.setEnabled(false);
        Intent intent = new Intent(this, Login.class);
        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Log.d("Chooser", "isFromReg");
                    Intent log = new Intent(this, Login.class);
                    startActivityForResult(log, 2);
                }
                break;
            case 2:
                if (resultCode == RESULT_OK) {
                    Log.d("Chooser", "isFromLog");
                    Intent reg = new Intent(this, Register.class);
                    startActivityForResult(reg, 1);
                }
                break;
            default:
                break;
        }

    }

}
