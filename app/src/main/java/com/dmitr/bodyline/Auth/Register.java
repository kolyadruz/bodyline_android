package com.dmitr.bodyline.Auth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import br.com.sapereaude.maskedEditText.MaskedEditText;
import cz.msebera.android.httpclient.Header;

public class Register extends AppCompatActivity {

    public static AsyncHttpClient Client = new AsyncHttpClient();

    //EditText phoneField;
    MaskedEditText phoneField;
    EditText passField;
    EditText passFieldConfirm;

    Button nextButton;
    Button enterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        phoneField = findViewById(R.id.phoneField);

        passField = findViewById(R.id.passField);
        passFieldConfirm = findViewById(R.id.passFieldConfirm);

        enterButton = findViewById(R.id.enterButton);
        nextButton = findViewById(R.id.nextButton);

    }

    public void nextbtn(View view) {
        ((Button) view).setEnabled(false);

        if (phoneField.length() < 16) {
            showErrID(3, nextButton);
            ((Button) view).setEnabled(true);
        } else if (passField.length() == 0) {
            showErrID(4, nextButton);
            ((Button) view).setEnabled(true);
        } else if (passFieldConfirm.length() == 0) {
            showErrID(4, nextButton);
            ((Button) view).setEnabled(true);
        } else if (passField.equals(passFieldConfirm)) {
            showErrID(3, nextButton);
            ((Button) view).setEnabled(true);
        } else {
            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = mSettings.edit();
            String phone = phoneField.getText().toString();
            String clearPhone = phone.replaceAll("[\\(\\)\\-\\+]","");
            String pass = passField.getText().toString();
            editor.putString("phone", clearPhone);
            editor.commit();
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.put("phone", clearPhone);
            params.put("pass", pass);
            client.post("http://bodyline14.ru/mobile/registration", params, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    // called before request is started
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    // called when response HTTP status is "200 OK"
                    if (statusCode == 200) {
                        String str = new String(responseBody);
                        JSONObject json = new JSONObject();
                        try {
                            json = new JSONObject(str);
                            Integer err_id = json.getInt("err_id");
                            if (err_id == 0) {
                                Integer opt = json.getInt("opt");
                                try {
                                    String token = json.getString("token");

                                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                    SharedPreferences.Editor editor = mSettings.edit();
                                    editor.putString("token", token);
                                    editor.commit();
                                }  catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                showOptID(opt, nextButton);
                            } else {
                                showErrID(err_id, nextButton);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        Log.d("Register:", "" + json);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    String Message = "Интернет соединение отсутствует";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    nextButton.setEnabled(true);
                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!isFinishing()) {
                        alert.show();
                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    // called when request is retried
                }
            });
        }
    }

    public void close(View v) {
        this.finish();
    }

    public void enterbtn(View view) {
        ((Button) view).setEnabled(false);

        Log.d("Register", "Ошибок нет. Переход в Login.java");
        //Intent intent = new Intent(getApplicationContext(), Login.class);


        //startActivity(intent);

        Intent intent = new Intent();
        intent.putExtra("needToStartLog", true);
        setResult(RESULT_OK, intent);
        this.finish();

    }

    public void onBackPressed(){
        //super.onBackPressed();
        //Intent intent = new Intent(getApplicationContext(), Login.class);
        //startActivity(intent);
        Intent intent = new Intent();
        intent.putExtra("needToStartReg", true);
        setResult(RESULT_CANCELED, intent);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("Register:","Ошибка работы токена. Переход в Login.java");
                        //Intent intent = new Intent(getApplicationContext(), Login.class);

                        //startActivity(intent);
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    private void showOptID(final Integer opt, Button optbutton) {
        AlertDialog.Builder builderopt = new AlertDialog.Builder(this);

        String optMessage = "";

        if (opt == 1) {
            optMessage = "Активация профиля";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Activation.java");
                            Intent intent = new Intent(getApplicationContext(), Activation.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        } else if (opt == 2) {
            optMessage = "Основные данные не заполнены";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Main.java");
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        } else if (opt == 3) {
            optMessage = "Все данные есть, но не куплен абонемент";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Main.java");
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        } else if (opt == 4) {
            optMessage = "Все данные есть, куплен абонемент";
            builderopt.setMessage(optMessage)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.d("Login", "Ошибок нет. Переход в Main.java");
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                    });
        }
        AlertDialog alert = builderopt.create();
        if (!isFinishing()) {
            alert.show();
        }

        optbutton.setEnabled(true);
    }
}

