package com.dmitr.bodyline.Auth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Profile.ProfileActivity;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;

public class Activation extends AppCompatActivity {


    String token;
    String phone;
    String timezone;

    Integer opt;

    EditText smsField;

    Button checkButton;
    Button reSmsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);

        smsField = findViewById(R.id.smsField);
        checkButton = findViewById(R.id.checkButton);
        reSmsButton = findViewById(R.id.reSmsButton);

        timezone = String.valueOf(TimeZone.getDefault().getID());

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        phone = mSettings.getString("phone", "");
        AsyncHttpClient onHashClient = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        onHashClient.addHeader("Authorization","Bearer "+token);
        onHashClient.post("http://bodyline14.ru/mobile/gethash", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    public void checkbtn(View view) {
        ((Button) view).setEnabled(false);

        if (smsField.length() == 4){
            onCheck();
        } else {
            showErrID(3, checkButton);
        }

    }

    public void resmsbtn (View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String reSmsMessage = "СМС отправлено повторно";

        builder.setMessage(reSmsMessage)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        reSmsButton.setEnabled(true);

        onResume();
    }

    public void close(View v) {
        this.finish();
    }

    public void onCheck(){
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        Intent intent = getIntent();
        opt = intent.getIntExtra("opt", 0);
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        phone = mSettings.getString("phone", "");
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String hash = smsField.getText().toString();
        params.put("phone", phone);
        params.put("hash", hash);
        params.put("timezone", timezone);
        client.post("http://bodyline14.ru/mobile/checkhash", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){

                            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = mSettings.edit();

                            token = json.getString("token");

                            editor.putString("token", token);
                            editor.commit();

                            /*Log.d("Activation","Ошибок нет. Переход в Main.java");
                            Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);
                            intent.putExtra("opt", opt);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);*/

                            Log.d("Activation","Ошибок нет. Переход в Profile.java");
                            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);
                        }
                        else {
                            showErrID(err_id, checkButton);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                    Log.d("Activation:",""+json);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                String Message = "Интернет соединение отсутствует";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                checkButton.setEnabled(true);
                            }
                        });

                AlertDialog alert = builder.create();
                if(!isFinishing()) {
                    alert.show();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }

        });

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    private void showOptID(Integer opt_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String optMessage = "";

        if (opt_id == 1) {
            optMessage = "Профиль не активирован";
        } else if (opt_id == 2) {
            optMessage = "Заполните личные данные";
        } else if (opt_id == 3) {
            optMessage = "Данные неверны";
        } else if (opt_id == 4) {
            optMessage = "Заполните все поля";
        }

        builder.setMessage(optMessage)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }
}