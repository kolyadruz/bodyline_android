package com.dmitr.bodyline.MainMenu.Pages.Season;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.dmitr.bodyline.MainMenu.Pages.Season.Diet.Diet;
import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.R;
import com.dmitr.bodyline.MainMenu.Pages.Season.Users.Users;
import com.dmitr.bodyline.MainMenu.Pages.Season.Event.CalendarDay;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;

public class FragmentCalendar extends Fragment {

    private OnFragmentInteractionListener mListener;

    int today = 0, countdays = 0;
    boolean isSeasonInfoAttached = false;


    MyAdapter pagerAdapter;
    List<FragmentDayWithTable> pages = new ArrayList<FragmentDayWithTable>();

    String token, s_token, imagePath;

    RelativeLayout mainCard;
    ConstraintLayout noPurchaseLayout;

    ViewPager mViewPager;

    LayoutInflater layoutInflater;

    Button goStoreBtn;

    ImageButton usersBtn, kcalBtn;

    List<CalendarDay> days = new ArrayList<CalendarDay>();

    TextView courseTitle, courseDescription;
    ImageView courseImage;

    TextView kcalLabel, membersLabel, progressLabel;

    ProgressBar seasonProgress;

    Activity activity;

    String nextStartDateInString;
    Date nextStartDate;
    Timer timer;
    TimerTask mPLayWithInterval = new playWithInterval();
    TextView timerTxt;

    public FragmentCalendar() {
    }

    public static FragmentCalendar newInstance(String param1, String param2) {
        FragmentCalendar fragment = new FragmentCalendar();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        imagePath = getActivity().getApplicationContext().getFilesDir() + "/avatar.jpg";
        Log.d("FragmentCalendar", "try to load ava from: "+ imagePath);
        token = mSettings.getString("token", "");
        s_token = mSettings.getString("s_token", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        layoutInflater = getLayoutInflater();


        isSeasonInfoAttached = false;

        courseTitle = rootView.findViewById(R.id.course_title);
        courseDescription =rootView.findViewById(R.id.course_description);
        courseImage =rootView.findViewById(R.id.courseImage);

        progressLabel = rootView.findViewById(R.id.percentViewTxt);
        kcalLabel = rootView.findViewById(R.id.kcalTxt);
        membersLabel = rootView.findViewById(R.id.membersTxt);

        seasonProgress = rootView.findViewById(R.id.seasonProgress);

        noPurchaseLayout = rootView.findViewById(R.id.noPurchaseLayout);
        timerTxt = rootView.findViewById(R.id.timerTxt);

        mainCard = rootView.findViewById(R.id.fragcalendar);
        mainCard.bringToFront();

        goStoreBtn = rootView.findViewById(R.id.goStoreBtn);
        goStoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainWithBottom)getActivity()).openStore();
            }
        });

        kcalBtn = rootView.findViewById(R.id.kcalImg);
        kcalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kcalBtn.setEnabled(false);
                Intent intent = new Intent(activity.getApplicationContext(), Diet.class);
                intent.putExtra("day", today);
                intent.putExtra("s_token", s_token);
                startActivity(intent);
                kcalBtn.setEnabled(true);
            }
        });

        usersBtn = rootView.findViewById(R.id.goStoreBtnSmall);
        usersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersBtn.setEnabled(false);
                Intent intent = new Intent(activity.getApplicationContext(), Users.class);
                startActivity(intent);
                usersBtn.setEnabled(true);
            }
        });

        mViewPager = rootView.findViewById(R.id.viewPager);
        pagerAdapter = new MyAdapter(getChildFragmentManager(), days);
        mViewPager.setAdapter(pagerAdapter);

        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(60, 0, 60 ,0 );

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (s_token.length() != 0){
            noPurchaseLayout.setVisibility(View.INVISIBLE);
            mViewPager.setVisibility(View.VISIBLE);
        } else {
            noPurchaseLayout.setVisibility(View.VISIBLE);
            mViewPager.setVisibility(View.INVISIBLE);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        token = mSettings.getString("token", "");

        getPurchasedSeasonToken(token);

        if (noPurchaseLayout.getVisibility() == View.VISIBLE) {
            if (nextStartDateInString != null) {
                scheduleStartTimer();
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static class MyAdapter extends FragmentPagerAdapter {
        List<CalendarDay> days = new ArrayList<CalendarDay>();

        public MyAdapter(FragmentManager fm, List<CalendarDay> days) {
            super(fm);
            this.days = days;
        }

        @Override
        public Fragment getItem(int position) {
            CalendarDay calday = days.get(position);

            int day = calday.day;
            int measures = calday.measures;
            int lessons = calday.lessons;
            int diets = calday.diets;
            int tasks = calday.tasks;
            int weight = calday.weight;

            int today = calday.today;

            String s_token = calday.s_token;

            return FragmentDayWithTable.newInstance(position,"page" + position, day, today, measures, lessons, diets, tasks, weight, s_token);
        }

        @Override
        public int getCount() {
            return days.size();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void getPurchasedSeasonToken(final String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getpurchasedseasontoken", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200){



                    String str = new String(responseBody);
                    try {
                        final JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");

                        Log.d("FragmentCalendar", "getpurchasedseasontoken: "+json);

                        if (err_id == 0){
                            try {

                                String s_token = json.getString("s_token");
                                getActiveUserSeason(token, s_token);

                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        } else {
                            noPurchaseLayout.setVisibility(View.VISIBLE);
                            mViewPager.setVisibility(View.INVISIBLE);

                            nextStartDateInString = json.getString("time");
                            scheduleStartTimer();

                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void getActiveUserSeason(final String token, final String s_token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getactiveuserseason", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200){
                    String str = new String(responseBody);
                    try {
                        final JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");

                        Log.d("FragmentCalendar", "getactiveuserseason: "+json);

                        if (err_id == 0){
                            try {

                                JSONObject season_info = json.getJSONObject("season_info");

                                int s_id = season_info.getInt("id");
                                int st_id = season_info.getInt("st_id");

                                SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
                                SharedPreferences.Editor editor = mSettings.edit();

                                editor.putInt("s_id", s_id);
                                editor.putInt("st_id", st_id);
                                editor.commit();

                                JSONObject season_composition = json.getJSONObject("season_composition");

                                int countdays = season_info.getInt("count_days");
                                int progress = season_info.getInt("progress");
                                int daycalories = season_info.getInt("daycalories");
                                int today = season_info.getInt("today");
                                int count_members =  season_info.getInt("count_members");

                                Log.d("FragmentCalendar", "today = "+ today);

                                FragmentCalendar.this.today = today;

                                if (!isSeasonInfoAttached) {

                                    isSeasonInfoAttached = true;
                                    FragmentCalendar.this.countdays = countdays;

                                    String image = season_info.getString("image");

                                    String name = season_info.getString("name");
                                    String description = season_info.getString("description");

                                    courseTitle.setText(name);
                                    courseDescription.setText(description);

                                    Picasso.get() //
                                            .load(image) //
                                            .placeholder(R.drawable.logowhsmall) //
                                            .error(R.drawable.ic_action_alarm) //
                                            .fit()
                                            .centerCrop()//
                                            .tag(activity) //
                                            .into(courseImage);

                                    days.clear();

                                    for (int i = 1; i < countdays + 1; i++) {
                                        JSONObject composition = season_composition.getJSONObject(""+i);

                                        int diets = composition.getInt("diets");
                                        int lessons = composition.getInt("lessons_id");
                                        int measures = composition.getInt("measure_id");
                                        int tasks = composition.getInt("tasks_id");
                                        int weight = composition.getInt("weight");

                                        CalendarDay day = new CalendarDay(today, i, diets, lessons, measures, tasks, weight, s_token);

                                        days.add(day);

                                    }

                                    pagerAdapter.notifyDataSetChanged();
                                    if (today > 0) {
                                        mViewPager.setCurrentItem(today - 1);
                                    } else {
                                        mViewPager.setCurrentItem(0);
                                    }

                                }

                                updateView(progress, today, daycalories, count_members);

                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        } else {
                            //showErrID(err_id);
                            noPurchaseLayout.setVisibility(View.VISIBLE);
                            mViewPager.setVisibility(View.INVISIBLE);

                            nextStartDateInString = json.getString("time");
                            scheduleStartTimer();

                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                AlertDialog alert = builder.create();
                if (!activity.isFinishing()) {
                    alert.show();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void scheduleStartTimer() {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            nextStartDate = format.parse(nextStartDateInString);

            timer = new Timer();
            mPLayWithInterval = new playWithInterval();
            timer.schedule(mPLayWithInterval, 0, 1000);

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

    }

    class playWithInterval extends TimerTask {

        @Override
        public void run() {

            activity.runOnUiThread(setTimerRunnable);

        }
    }

    final Runnable setTimerRunnable = new Runnable() {
        public void run() {

            Date currentTime = Calendar.getInstance().getTime();
            Long timeDiff = nextStartDate.getTime() - currentTime.getTime();

            long days = TimeUnit.MILLISECONDS.toDays(timeDiff);
            long hours = TimeUnit.MILLISECONDS.toHours(timeDiff) - TimeUnit.DAYS.toHours(days);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff));
            long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeDiff));

            String str = String.format("%d:%02d:%02d", hours, minutes, seconds);
            timerTxt.setText(days + " дн.  " + str);

        }
    };

    private void updateView(int progress, int today, int daycalories, int count_members) {

        kcalLabel.setText(""+daycalories);

        membersLabel.setText(""+count_members);

        progressLabel.setText(progress + "%");

        seasonProgress.setProgress(progress);

    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

}
