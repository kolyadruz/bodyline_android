package com.dmitr.bodyline.MainMenu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dmitr.bodyline.AccountHelper;
import com.dmitr.bodyline.Auth.Chooser.Chooser;
import com.dmitr.bodyline.MainMenu.Pages.Account.FragmentAccount;
import com.dmitr.bodyline.R;
import com.dmitr.bodyline.MainMenu.Pages.Season.FragmentCalendar;
import com.dmitr.bodyline.MainMenu.Pages.Chat.FragmentChat;
import com.dmitr.bodyline.MainMenu.Pages.CuratorQuestion.FragmentCurator;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Profile.FragmentProfile;
import com.dmitr.bodyline.MainMenu.Pages.Store.FragmentShop;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainWithBottom extends AppCompatActivity {

    public boolean isFromCamera = false;

    Fragment fragment;
    FragmentManager fragmentManager;

    Toolbar toolbar;
    BottomNavigationView navigation;

    CircleImageView avatar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.curator:
                    fragment = new FragmentCurator();
                    toolbar.setTitle("Вопрос кураторам");
                    avatar.setVisibility(View.VISIBLE);
                    break;
                case R.id.shop:
                    fragment = new FragmentShop();
                    toolbar.setTitle("Магазин");
                    avatar.setVisibility(View.VISIBLE);
                    break;
                case R.id.fitness:
                    fragment = new FragmentCalendar();
                    toolbar.setTitle("Сезон");
                    avatar.setVisibility(View.VISIBLE);
                    break;
                case R.id.chat:
                    fragment = new FragmentChat();
                    toolbar.setTitle("Общий чат");
                    avatar.setVisibility(View.VISIBLE);
                    break;
                case R.id.account:
                    fragment = new FragmentAccount();
                    toolbar.setTitle("Аккаунт");
                    avatar.setVisibility(View.GONE);
                    break;
            }

            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_with_bottom);

        avatar = findViewById(R.id.logoXmarks);

        navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.fitness);
        DisplayMetrics disp = getResources().getDisplayMetrics();
        BottomNavigationViewHelper.removeShiftMode(navigation, disp);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = mSettings.edit();
        isFromCamera = mSettings.getBoolean("isFromCamera", false);

        toolbar = findViewById(R.id.toolbar);

        fragmentManager = getSupportFragmentManager();
        fragment = new FragmentCalendar();

        navigation.setVisibility(View.VISIBLE);

        editor.putBoolean("isFirstTimeSaving", false);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (isFromCamera) {
            //getSupportActionBar().setTitle(R.string.nav_cont_drawer_profile);
            FragmentProfile fragmentProfile = new FragmentProfile();
            fragmentTransaction.add(R.id.main_container, fragmentProfile);

            toolbar.setTitle("Мой профиль");

            editor.putBoolean("isFromCamera", false);
            editor.commit();

        } else {

            toolbar.setTitle("Сезон");
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();

            editor.commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        avatar.setImageBitmap(BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/avatar.jpg"));

    }

    public void openAccount(View v) {
        navigation.setSelectedItemId(R.id.account);
    }

    public void openStore() {

        navigation.setSelectedItemId(R.id.shop);

    }

    public void openProfile(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 0);
        startActivity(intent);
    }

    public void openInfo(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 1);
        startActivity(intent);
    }

    public void openAlarm(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 2);
        startActivity(intent);
    }

    public void openBefAft(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 3);
        startActivity(intent);
    }

    public void openAbout(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 4);
        startActivity(intent);
    }

    public void openSettings(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 5);
        startActivity(intent);
    }

    public void openSupport(View v) {
        Intent intent = new Intent(this, AccountHelper.class);
        intent.putExtra("selected", 6);
        startActivity(intent);
    }

    public void endSession(View v) {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("token", "");
        editor.commit();

        Intent intent = new Intent(this, Chooser.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

}
