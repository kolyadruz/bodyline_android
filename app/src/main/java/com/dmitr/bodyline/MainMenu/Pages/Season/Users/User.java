package com.dmitr.bodyline.MainMenu.Pages.Season.Users;

public class User {

    public int id;
    public String lastname;
    public String name;

    public String ava;

    public int age;
    public int gender;

    public User(int id, String lastname, String name, String ava, int age, int gender){

        this.id = id;
        this.lastname = lastname;
        this.name = name;

        this.ava = ava;

        this.age = age;

        this.gender = gender;

    }

}
