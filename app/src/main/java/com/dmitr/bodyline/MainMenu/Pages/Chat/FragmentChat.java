package com.dmitr.bodyline.MainMenu.Pages.Chat;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.App.DApplication;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentChat.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentChat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentChat extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    EditText messageTxt;
    ImageButton sendBtn;

    ChatAdapter adapter;

    List<Message> messages = new ArrayList<Message>();

    ListView chats;

    String nickname;
    String token;

    Integer s_id;
    Integer st_id;
    Integer page = 1;

    private Socket mSocket;

    Activity activity;

    public FragmentChat() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentChat.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentChat newInstance(String param1, String param2) {
        FragmentChat fragment = new FragmentChat();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        DApplication app = (DApplication) getActivity().getApplication();
        mSocket = app.getSocket();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Integer s_id = mSettings.getInt("s_id", 0);
        Integer st_id = mSettings.getInt("st_id", 0);
        mSocket.on("chat_" + st_id + "_" + s_id , onNewMessage);
        mSocket.connect();

        View root = inflater.inflate(R.layout.fragment_chat, container, false);

        messageTxt = (EditText) root.findViewById(R.id.messageTxt);
        sendBtn = (ImageButton) root.findViewById(R.id.sendBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTapped((ImageButton) v);
            }
        });

        activity = getActivity();

        adapter = new ChatAdapter(getActivity(), messages, R.layout.cell_chat_message, R.layout.cell_chat_selfmessage, R.layout.cell_chat_curatormessage);
        chats = (ListView) root.findViewById(R.id.chatListView);

        chats.setAdapter(adapter);

        return root;
    }

    /*private void scrollToBottom() {
        chats.scrollToPosition(adapter.getItemCount() - 1);
    }*/

    private void sendTapped(ImageButton sender) {

        if (!messageTxt.getText().equals("")) {

            sender.setEnabled(false);

            //JSONObject data = new JSONObject();
            JSONObject dataparams = new JSONObject();
            StringEntity entity = new StringEntity("", "");
            try {
                dataparams.put("chatToken", "6ac8cd41-92d6-4843-8489-9704b3a1da2b");
                dataparams.put("name", nickname);
                dataparams.put("chat", messageTxt.getText().toString());
                dataparams.put("s_id", s_id);
                dataparams.put("st_id", st_id);
                dataparams.put("token", token);
                //data.put("data", dataparams);

                //entity = new StringEntity(data.toString(), "UTF-8");
                entity = new StringEntity(dataparams.toString(), "UTF-8");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(getActivity().getApplicationContext(), "http://bodyline14.ru:3020/chats", entity, "application/json; charset=utf-8", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    if (statusCode == 200) {

                        messageTxt.setText("");

                    }

                    sendBtn.setEnabled(true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d("FragmentChat", "PostResponseFail");
                    sendBtn.setEnabled(true);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        String name = mSettings.getString("imya", "");
        String lastname = mSettings.getString("fam", "");
        nickname = name + " " + lastname;
        token = mSettings.getString("token", "");

        s_id = mSettings.getInt("s_id", 0);
        st_id = mSettings.getInt("st_id", 0);
        page = 1;
        runGetChats();
    }

    private void runGetChats() {
        getChats(s_id, st_id, page);
    }

    public void getChats(Integer s_id, Integer st_id, final Integer page) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_id", s_id);
        params.put("st_id", st_id);
        params.put("page", page);
        params.put("chat_token", "6ac8cd41-92d6-4843-8489-9704b3a1da2b");
        client.get("http://bodyline14.ru:3020/chats?s_id="+s_id+"&st_id="+st_id+"&page="+page+"&chatToken=6ac8cd41-92d6-4843-8489-9704b3a1da2b", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    try {
                        JSONArray jsonArray = new JSONArray(str);

                        Log.d("jsonArray", "" + jsonArray);

                        //List<Message> lastchats = messages;

                        if (jsonArray.length() > 0) {

                            Log.d("Chat", "jsonArray is not null");

                            messages.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject data = jsonArray.getJSONObject(i);

                                try {

                                    String name = data.getString("name");
                                    String message = data.getString("chat");

                                    String tkn = data.getString("token");

                                    Integer isAdmin = data.getInt("isAdmin");
                                    String date = data.getString("date");

                                    boolean self = (tkn.equals(token));

                                    Message msg = new Message(name, message, tkn, self, isAdmin, date);

                                    messages.add(msg);

                                } catch (JSONException ex) {

                                    String name = data.getString("name");
                                    String message = data.getString("chat");

                                    Integer isAdmin = data.getInt("isAdmin");
                                    String date = data.getString("date");

                                    Message msg = new Message(name, message, "", false, isAdmin, date);

                                    messages.add(msg);

                                }

                            }

                            /*for (int i = 0; i < lastchats.size(); i++) {

                                messages.add(lastchats.get(i));

                            }*/

                            adapter.notifyDataSetChanged();

                            /*if (page == 1) {
                                FragmentChat.this.page = 2;
                                runGetChats();
                            }*/

                            if (page < 3) {
                                scrollMyListViewToBottom();
                            }

                        }

                    } catch (Exception ex) {

                        ex.printStackTrace();

                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("FragmentChat", "GetResponseFail");
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mSocket.disconnect();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        mSocket.disconnect();
        super.onDestroy();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void scrollMyListViewToBottom() {
        chats.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                chats.setSelection(adapter.getCount() - 1);
            }
        });
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        JSONObject data = (JSONObject) args[0];

                        Log.d("New Message", ""+data);

                        try {
                            String name = data.getString("name");
                            String message = data.getString("chat");
                            String tkn = data.getString("token");

                            boolean self = tkn.equals(token);

                            Integer isAdmin = data.getInt("isAdmin");

                            String date = data.getString("date");

                            Message msg = new Message(name, message, tkn, self, isAdmin, date);

                            messages.add(msg);

                            adapter.notifyDataSetChanged();
                            scrollMyListViewToBottom();

                        } catch (JSONException ex) {

                            String name = data.getString("name");
                            String message = data.getString("chat");

                            Integer isAdmin = data.getInt("isAdmin");

                            String date = data.getString("date");

                            Message msg = new Message(name, message, "", false, isAdmin, date);

                            messages.add(msg);

                            adapter.notifyDataSetChanged();
                            scrollMyListViewToBottom();
                        }
                    } catch (JSONException ex) {
                        //Log.e("FragmentChat", e.getMessage());

                        ex.printStackTrace();

                    }

                    //removeTyping(username);
                    //addMessage(username, message);
                }
            });
        }
    };

}
