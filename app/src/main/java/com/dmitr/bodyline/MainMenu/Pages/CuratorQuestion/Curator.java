package com.dmitr.bodyline.MainMenu.Pages.CuratorQuestion;

public class Curator {

    String question;
    String questionDate;
    String answer;
    String answerDate;
    boolean isAnswered;

    public Curator(){

    }

    public Curator(String question, String questionDate, String answer, String answerDate, boolean isAnswered){

        this.question = question;
        this.questionDate = questionDate;
        this.answer = answer;
        this.answerDate = answerDate;
        this.isAnswered = isAnswered;
    }
}
