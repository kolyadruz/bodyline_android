package com.dmitr.bodyline.MainMenu.Store;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.dmitr.bodyline.R;

public class SSLTolerentWebViewClient  extends WebViewClient {

    Context ctx;

    public SSLTolerentWebViewClient(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        builder.setMessage("Ошибка безопасности SSL");
        builder.setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.proceed();
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.cancel();
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        view.loadUrl(url);
        return true;
    }

    public void onPageFinished(WebView view, String url) {
        View parent = view.getRootView();

        RelativeLayout loading = (RelativeLayout)parent.findViewById(R.id.loading);
        loading.setVisibility(View.INVISIBLE);

    }

}