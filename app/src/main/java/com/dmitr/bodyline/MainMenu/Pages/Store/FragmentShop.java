package com.dmitr.bodyline.MainMenu.Pages.Store;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentShop.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentShop#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentShop extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //ConstraintLayout noProdLayout;
    ImageButton closeStoreBtn;
    TextView storeTopTxt;

    ConstraintLayout storeNoDataLayout;
    TextView noProductTxt;

    ConstraintLayout loadingLayout;
    ProgressBar loadingDataPB;

    ConstraintLayout dataViewLayout;
    TextView selectTrainTxt;
    ListView buyListCell;

    List<Day> days = new ArrayList<Day>();

    DayAdapter dayAdapter;

    String token;

    ListView buyListView;

    Activity activity;

    String nextStartDateInString;
    Date nextStartDate;
    Timer timer;
    TimerTask mPLayWithInterval = new playWithInterval();
    TextView timerTxt;

    public FragmentShop() {
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentShop newInstance(String param1, String param2) {
        FragmentShop fragment = new FragmentShop();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_shop, container, false);

        activity = getActivity();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        token = mSettings.getString("token", "");

        storeNoDataLayout = root.findViewById(R.id.storeNoDataLayout);

        timerTxt = root.findViewById(R.id.timerTxt);

        loadingLayout = root.findViewById(R.id.loadingLayout);
        dataViewLayout = root.findViewById(R.id.dataViewLayout);

        buyListView =(ListView) root.findViewById(R.id.buyListCell);
        dayAdapter = new DayAdapter(getActivity(),days,R.layout.cell_train_buy);
        buyListView.setAdapter(dayAdapter);

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        days.clear();

        loadingLayout.setVisibility(View.VISIBLE);
        storeNoDataLayout.setVisibility(View.INVISIBLE);
        dataViewLayout.setVisibility(View.INVISIBLE);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getstseasons", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    try {
                        JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        Log.d("Store err_id: ", "" +json);

                        nextStartDateInString = json.getString("time");
                        scheduleStartTimer();

                        if (err_id == 0) {
                            try {
                                String rowData = json.getString("seasons");
                                JSONArray seasons = new JSONArray(rowData);
                                Integer st_id = json.getInt("st_id");

                                loadingLayout.setVisibility(View.INVISIBLE);
                                storeNoDataLayout.setVisibility(View.INVISIBLE);
                                dataViewLayout.setVisibility(View.VISIBLE);


                                if (seasons.length() > 0) {
                                    for (int i = 0; i < seasons.length(); i++) {
                                        JSONObject dayNum = seasons.getJSONObject(i);
                                        int id = dayNum.getInt("id");
                                        String name = dayNum.getString("name");
                                        String count_days = dayNum.getString("count_days");
                                        String price = dayNum.getString("price");
                                        String description = dayNum.getString("description");
                                        String date = json.getString("date");

                                        String image = dayNum.getString("image");

                                        Log.d("Store Strings: ", "id: " + id + " name: " + name + " count_days: " + count_days + " price: " + price + " description: " + description + " st_id: " + st_id);

                                        Day day = new Day(id, name, count_days, price, description, date, st_id, image);

                                        days.add(day);

                                        dayAdapter.notifyDataSetChanged();

                                    }
                                } else {
                                    loadingLayout.setVisibility(View.INVISIBLE);
                                    storeNoDataLayout.setVisibility(View.VISIBLE);
                                    dataViewLayout.setVisibility(View.INVISIBLE);

                                    nextStartDateInString = json.getString("time");
                                    scheduleStartTimer();

                                }
                            } catch (JSONException ex){

                                loadingLayout.setVisibility(View.INVISIBLE);
                                storeNoDataLayout.setVisibility(View.VISIBLE);
                                dataViewLayout.setVisibility(View.INVISIBLE);

                                nextStartDateInString = json.getString("time");
                                scheduleStartTimer();

                                ex.printStackTrace();
                            }

                        } else {

                            loadingLayout.setVisibility(View.INVISIBLE);
                            storeNoDataLayout.setVisibility(View.VISIBLE);
                            dataViewLayout.setVisibility(View.INVISIBLE);

                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void scheduleStartTimer() {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            nextStartDate = format.parse(nextStartDateInString);

            timer = new Timer();
            mPLayWithInterval = new playWithInterval();
            timer.schedule(mPLayWithInterval, 0, 1000);

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

    }

    class playWithInterval extends TimerTask {

        @Override
        public void run() {

            activity.runOnUiThread(setTimerRunnable);

        }
    }

    final Runnable setTimerRunnable = new Runnable() {
        public void run() {

            Date currentTime = Calendar.getInstance().getTime();
            Long timeDiff = nextStartDate.getTime() - currentTime.getTime();

            long days = TimeUnit.MILLISECONDS.toDays(timeDiff);
            long hours = TimeUnit.MILLISECONDS.toHours(timeDiff) - TimeUnit.DAYS.toHours(days);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff));
            long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeDiff));

            String str = String.format("%d:%02d:%02d", hours, minutes, seconds);
            timerTxt.setText(days + " дн.  " + str);

        }
    };


    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
