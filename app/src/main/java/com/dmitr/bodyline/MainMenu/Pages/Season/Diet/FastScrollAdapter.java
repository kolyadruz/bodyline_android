package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

import android.content.Context;
import android.widget.SectionIndexer;

import java.util.List;

public class FastScrollAdapter extends DietAdapter implements SectionIndexer {

    private DietItem[] sections;

    public FastScrollAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public FastScrollAdapter(Context context, int resource, int textViewResourceId,
                             List<Food> diet1Array,
                             List<Food> diet2Array,
                             List<Food> diet3Array,
                             List<Food> diet4Array,
                             List<Food> diet5Array,

                             List<Dish> diet1dishArray,
                             List<Dish> diet2dishArray,
                             List<Dish> diet3dishArray,
                             List<Dish> diet4dishArray,
                             List<Dish> diet5dishArray
    ) {
        super(context, resource, textViewResourceId);
    }

    public FastScrollAdapter(Context context, int resource, int textViewResourceId,
                             List<DietItem> diet1Array
    ) {
        super(context, resource, textViewResourceId);
    }

    @Override protected void prepareSections(int sectionsNumber) {
        sections = new DietItem[sectionsNumber];
    }

    @Override protected void onSectionAdded(DietItem section, int sectionPosition) {
        sections[sectionPosition] = section;
    }

    @Override public DietItem[] getSections() {
        return sections;
    }

    @Override public int getPositionForSection(int section) {
        if (section >= sections.length) {
            section = sections.length - 1;
        }
        return sections[section].listPosition;
    }

    @Override public int getSectionForPosition(int position) {
        if (position >= getCount()) {
            position = getCount() - 1;
        }
        return getItem(position).sectionPosition;
    }

}
