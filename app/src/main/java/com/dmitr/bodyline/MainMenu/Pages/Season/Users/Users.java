package com.dmitr.bodyline.MainMenu.Pages.Season.Users;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Users extends AppCompatActivity {

    ListView usersList;

    UsersAdapter adapter;

    List<User> users = new ArrayList<User>();

    String token;
    String s_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");
        s_token = mSettings.getString("s_token", "");

        usersList = (ListView) findViewById(R.id.userslist);

        adapter = new UsersAdapter(this, users, R.layout.cell_users);

        usersList.setAdapter(adapter);

        usersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = users.get(position);

                Intent intent = new Intent(Users.this, UserDetail.class);
                intent.putExtra("id", user.id);
                intent.putExtra("s_token", s_token);
                intent.putExtra("avatar", user.ava);
                intent.putExtra("name", user.name + " " + user.lastname);
                String info = "";
                if (user.gender == 1) {
                    info = "жен, " + user.age + " лет";
                } else {
                    info = "муж, " + user.age + " лет";
                }
                intent.putExtra("info", info);

                startActivity(intent);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        runGetSeasonUsersList();
    }

    private void runGetSeasonUsersList() {

        getSeasonUsersList(token, s_token);

    }

    private void getSeasonUsersList(String token, String s_token) {

        Log.d("Users", "token: "+token+" s_token: "+s_token);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getseasonuserslist", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("Users", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                users.clear();

                                JSONArray data = json.getJSONArray("data");

                                for (int i = 0; i < data.length(); i++) {

                                    JSONObject userData = data.getJSONObject(i);

                                    String lastname = userData.getString("fam");
                                    String name = userData.getString("imya");

                                    String ava = userData.getString("avatar");

                                    int age = userData.getInt("age");
                                    int gender = userData.getInt("gender");

                                    int id = userData.getInt("id");

                                    User userObj = new User(id, lastname, name, ava, age, gender);

                                    users.add(userObj);

                                }

                                adapter.notifyDataSetChanged();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showErrID(Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

}
