package com.dmitr.bodyline.MainMenu.Pages.Season;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.MainMenu.Pages.Season.Diet.Diet;
import com.dmitr.bodyline.MainMenu.Pages.Season.Measure.Measure;
import com.dmitr.bodyline.TrainAndTask;
import com.dmitr.bodyline.Weight;
import com.dmitr.bodyline.MainMenu.Pages.Season.Event.Event;
import com.dmitr.bodyline.MainMenu.Pages.Season.Event.EventAdapter;
import com.dmitr.bodyline.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDayWithTable.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDayWithTable#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDayWithTable extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String DAY_PARAM = "param3";
    private static final String TODAY_PARAM = "param4";
    private static final String RESULT_PARAM = "param5";
    private static final String MEASURE_PARAM = "param6";
    private static final String LESSONS_PARAM = "param7";
    private static final String DIET_PARAM = "param8";
    private static final String TASK_PARAM = "param9";
    private static final String WEIGHT_PARAM = "param10";

    private static final String STOKEN_PARAM = "param11";

    // TODO: Rename and change types of parameters
    private int page;
    private String title;

    private int day;
    private int today;
    private int measure_id;
    private int lessons_id;
    private int tasks_id;
    private int diet_id;
    private int weight;

    private String s_token;

    private int accessLevel = 0;

    private OnFragmentInteractionListener mListener;

    EventAdapter eventAdapter;

    List<Event> events = new ArrayList<Event>();

    public FragmentDayWithTable() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentInfo.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDayWithTable newInstance(int page, String title, int day, int today, int measure_id, int lessons_id, int diet_id, int tasks_id, int weight, String s_token) {
        FragmentDayWithTable fragment = new FragmentDayWithTable();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, page);
        args.putString(ARG_PARAM2, title);
        args.putInt(DAY_PARAM, day);
        args.putInt(TODAY_PARAM, today);
        args.putInt(MEASURE_PARAM, measure_id);
        args.putInt(LESSONS_PARAM, lessons_id);
        args.putInt(DIET_PARAM, diet_id);
        args.putInt(TASK_PARAM, tasks_id);
        args.putInt(WEIGHT_PARAM, weight);

        args.putString(STOKEN_PARAM, s_token);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            page = getArguments().getInt(ARG_PARAM1);
            title = getArguments().getString(ARG_PARAM2);
            day = getArguments().getInt(DAY_PARAM);
            today = getArguments().getInt(TODAY_PARAM);
            measure_id = getArguments().getInt(MEASURE_PARAM);
            lessons_id = getArguments().getInt(LESSONS_PARAM);
            diet_id = getArguments().getInt(DIET_PARAM);
            tasks_id = getArguments().getInt(TASK_PARAM);
            weight = getArguments().getInt(WEIGHT_PARAM);

            s_token = getArguments().getString(STOKEN_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_day_with_table, container, false);
        events.clear();

        /*for (int i = 0; i < 4; i++){
            Event event = new Event(i);
            events.add(event);
        }*/

        if (tasks_id ==  1) {
            Event event = new Event(1);
            events.add(event);
        }

        if (diet_id ==  1) {
            Event event = new Event(2);
            events.add(event);
        }

        if (lessons_id ==  1) {
            Event event = new Event(3);
            events.add(event);
        }

        if (measure_id ==  1) {
            Event event = new Event(4);
            events.add(event);
        }

        if (weight ==  1) {
            Event event = new Event(5);
            events.add(event);
        }



        GridView eventsGridView = (GridView) rootView.findViewById(R.id.taskList);

        if (events.size() < 3) {
            eventsGridView.setNumColumns(1);
        } else {
            eventsGridView.setNumColumns(2);
        }

        TextView numdaytxt = (TextView) rootView.findViewById(R.id.numdaytxt);
        numdaytxt.setText("День "+ day);

        final TextView todaytxt = (TextView) rootView.findViewById(R.id.todaytxt);
        if (day == today) {
            todaytxt.setText("Сегодня");
            accessLevel = 2;
        } else if (day == today - 1) {
            todaytxt.setText("Вчера");
            accessLevel = 1;
        } else if (day == today + 1) {
            todaytxt.setText("Завтра");
            accessLevel = 1;
        } else {

            //высчитать дату дня

            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM");  //EE, d MMM
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, - today + day);
            dateFormat.format(cal.getTime());

            if (day < today - 1) {
                accessLevel = 1;
                todaytxt.setText(""+dateFormat.format(cal.getTime()));
            } else if (day > today + 1) {
                accessLevel = 0;
                todaytxt.setText("Будет доступно в " + dateFormat.format(cal.getTime()));
                numdaytxt.setTextColor(getResources().getColor(R.color.numdaytxt_gray));
            }

        }

        eventAdapter = new EventAdapter(getActivity(), events, accessLevel, R.layout.cell_event);
        eventsGridView.setAdapter(eventAdapter);

        eventsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Event event = events.get(position);
                int type = event.type;

                Log.d("FragmentDayWithTable", "day: "+day);
                Log.d("FragmentDayWithTable", "today: "+today);

                Log.d("FragmentDayWithTable", "tapped type: "+type);

                if (accessLevel > 0 || type == 2) {

                    switch (type) {
                        case 1:
                            //Задания

                            Intent task = new Intent(getActivity().getApplicationContext(), TrainAndTask.class);
                            task.putExtra("accessLevel", accessLevel);
                            task.putExtra("isNeedToBeUpdated", true);
                            task.putExtra("descriptionText", "");
                            task.putExtra("day", day);
                            task.putExtra("type", type);
                            task.putExtra("s_token", s_token);


                            startActivity(task);

                            break;
                        case 2:
                            //Питание

                            Intent intent = new Intent(getActivity().getApplicationContext(), Diet.class);
                            intent.putExtra("day", day);
                            intent.putExtra("s_token", s_token);

                            startActivity(intent);

                            break;
                        case 3:
                            //Тренировки

                            Intent train = new Intent(getActivity().getApplicationContext(), TrainAndTask.class);
                            train.putExtra("accessLevel", accessLevel);
                            train.putExtra("isNeedToBeUpdated", true);
                            train.putExtra("descriptionText", "");
                            train.putExtra("day", day);
                            train.putExtra("type", type);
                            train.putExtra("s_token", s_token);

                            startActivity(train);

                            break;
                        case 4:
                            //Замеры

                            Intent measure = new Intent(getActivity().getApplicationContext(), Measure.class);
                            measure.putExtra("accessLevel", accessLevel);
                            measure.putExtra("isNeedToBeUpdated", true);
                            measure.putExtra("descriptionText", "");
                            measure.putExtra("day", day);
                            measure.putExtra("type", type);
                            measure.putExtra("s_token", s_token);

                            startActivity(measure);

                            break;
                        case 5:
                            //Замеры

                            Intent weight = new Intent(getActivity().getApplicationContext(), Weight.class);
                            weight.putExtra("accessLevel", accessLevel);
                            weight.putExtra("day", day);
                            weight.putExtra("s_token", s_token);

                            startActivity(weight);

                            break;
                        default:
                            break;
                    }
                }
            }

        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
