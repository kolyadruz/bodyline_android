package com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.dmitr.bodyline.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AlarmReceiver extends BroadcastReceiver{

    @Override public void onReceive(Context context, Intent intent) {

        PowerManager pm=(PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl= pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"WakeLock:tag");
        //Осуществляем блокировку
        wl.acquire();

        StringBuilder msgStr = new StringBuilder();

        String title = intent.getStringExtra("title");
        String msg = intent.getStringExtra("msg");
        Integer requestCode = intent.getIntExtra("rC", 0);

        msgStr.append(msg);

        msgStr.append(" rC:");
        msgStr.append(""+requestCode);

        Toast.makeText(context, msgStr, Toast.LENGTH_LONG).show();


        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        String channelId = ((Long)(System.currentTimeMillis() + new Long(requestCode))).toString();

        Uri SoundUri= Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bell);

        context.getApplicationContext().grantUriPermission("com.android.systemui", SoundUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context.getApplicationContext(), channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setAutoCancel(true)
                        .setSound(SoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
        Log.d("AlarmReceiver", "Message Notification Sended");

        //Разблокируем поток.
        wl.release();
    }

    public void SetAlarm(Context context, String title, String msg, Integer requestCode)
    {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("title", title);
        intent.putExtra("msg", msg);
        intent.putExtra("rC", requestCode);

        Log.d("AlarmReceiver", "requestCode="+requestCode+" title="+title);

        PendingIntent pi= PendingIntent.getBroadcast(context, requestCode, intent, 0);
        //Устанавливаем интервал срабатывания в 5 секунд.
        am.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),1000*5,pi);
    }

    public void CancelAlarm(Context context, Integer requestCode)
    {
        Intent intent=new Intent(context, AlarmReceiver.class);
        PendingIntent sender= PendingIntent.getBroadcast(context, requestCode, intent,0);
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
        //Отменяем будильник, связанный с интентом данного класса
    }

}