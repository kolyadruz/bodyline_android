package com.dmitr.bodyline.MainMenu.Pages.Season;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dmitr.bodyline.trainandtask.Gif;
import com.dmitr.bodyline.trainandtask.Video;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.MutableExtras;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class Data {

    private Data() {
    }

    @NonNull
    public static List<Request> getFetchRequests(List<Video> videosList) {
        final List<Request> requests = new ArrayList<>();

        for (Video video : videosList) {
            String strUrl = video.getUrl();

            final Request request = new Request(strUrl, getFilePath(strUrl));
            request.setDownloadOnEnqueue(false);

            final MutableExtras extras = new MutableExtras();
            extras.putBoolean("isImage", video.getIsImage());
            extras.putString("url", video.getUrl());
            extras.putString("thumbs", video.getThumbs());
            extras.putString("title", video.getTitle());
            extras.putString("description", video.getDescription());
            extras.putString("fileName", video.getFilename());
            request.setExtras(extras);

            requests.add(request);
        }
        return requests;
    }

    @NonNull
    public static List<Request> getFetchRequestWithGroupId(final int groupId, List<Video> videosList) {
        final List<Request> requests = getFetchRequests(videosList);
        for (Request request : requests) {
            request.setGroupId(groupId);
        }
        return requests;
    }

    @NonNull
    public static String getFilePath(@NonNull final String url) {
        final Uri uri = Uri.parse(url);
        final String fileName = uri.getLastPathSegment();
        final String dir = getSaveDir();
        return (dir + "/DownloadList/" + fileName);
    }

    @NonNull
    public static String getNameFromUrl(final String url) {
        return Uri.parse(url).getLastPathSegment();
    }

    @NonNull
    public static List<Request> getGifUpdates(List<Gif> gifsToLoad) {
        final List<Request> requests = new ArrayList<>();

        for (Gif gif: gifsToLoad) {
            final String filePath = getSaveDir() + "/gifsAssets/";
            final String fileName = gif.getFilename();
            File file = new File(filePath, fileName);
            if (!file.exists()) {
                final Request request = new Request(gif.getUrl(), filePath + fileName);
                request.setPriority(Priority.HIGH);
                requests.add(request);
            } else {
                Log.d("Data", "file " + gif.getFilename() + " exists");
            }
        }
        return requests;
    }

    @NonNull
    public static String getSaveDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/fetch";
    }

}
