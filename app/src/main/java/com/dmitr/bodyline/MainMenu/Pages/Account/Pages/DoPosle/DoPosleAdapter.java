package com.dmitr.bodyline.doposle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dmitr.bodyline.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DoPosleAdapter extends BaseAdapter {
    private final Context context;
    private List<String> urls = new ArrayList<>();

    int layoutID;
    private LayoutInflater inflater=null;

    DoPosleAdapter(Context context) {
        this.context = context;

        // Ensure we get a different ordering of images on each run.
        Collections.addAll(urls, Data.URLS);
        Collections.shuffle(urls);

        // Triple up the list.
        ArrayList<String> copy = new ArrayList<>(urls);
        urls.addAll(copy);
        urls.addAll(copy);
    }

    public DoPosleAdapter(Context context, List<String> urls, int layoutID) {
        this.context = context;

        this.urls = urls;

        this.layoutID = layoutID;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override public View getView(int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        PhotocardCell photocardCell;
        if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            photocardCell = new PhotocardCell(cellView);
            cellView.setTag(photocardCell);
        } else {
            photocardCell = (PhotocardCell) cellView.getTag();
        }

        // Get the image URL for the current position.
        String url = getItem(position);

        // Trigger the download of the URL asynchronously into the image view.
        Picasso.get() //
                .load(url) //
                .placeholder(R.drawable.logowhsmall) //
                .error(R.drawable.ic_action_alarm) //
                .fit() //
                .tag(context) //
                .into(photocardCell.imgView);

        return cellView;
    }

    @Override public int getCount() {
        return urls.size();
    }

    @Override public String getItem(int position) {
        return urls.get(position);
    }

    @Override public long getItemId(int position) {
        return position;
    }

    class PhotocardCell {

        public ImageView imgView;

        public PhotocardCell(View base) {

            imgView = (ImageView) base.findViewById(R.id.imgView);

        }
    }

}