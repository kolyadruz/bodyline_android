package com.dmitr.bodyline.MainMenu.Pages.Store;

public class Day {


    int id;
    String name;
    String count_days;
    String price;
    String description;
    String date;
    int st_id;

    String image;

    public Day(){

    }

    public Day (int id, String name, String count_days, String price, String description, String date, int st_id, String image){

        this.id = id;
        this.name = name;
        this.count_days = count_days;
        this.price = price;
        this.description = description;
        this.date = date;

        this.st_id = st_id;

        this.image = image;

    }


}
