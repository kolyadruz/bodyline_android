package com.dmitr.bodyline.MainMenu.Pages.Season.Users;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmitr.bodyline.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UsersAdapter extends BaseAdapter {

    private Activity activity;

    List<User> users = new ArrayList<User>();

    private int layoutID;
    private LayoutInflater inflater=null;

    //public event_id;

    public UsersAdapter(Activity activity, List<User> users, int layoutID) {
        this.activity = activity;

        this.users = users;

        this.layoutID = layoutID;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {

        return users.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        EventsListCell eventsListCell;
        if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            eventsListCell = new EventsListCell(cellView);
            cellView.setTag(eventsListCell);
        } else {
            eventsListCell = (EventsListCell) cellView.getTag();
        }


        User user = this.users.get(position);

        Picasso.get() //
                .load(user.ava) //
                .placeholder(R.drawable.logowhsmall) //
                .error(R.drawable.ic_action_alarm) //
                .fit().centerCrop() //
                .tag(activity) //
                .into(eventsListCell.avatar);

        eventsListCell.name.setText(user.name + " " + user.lastname);

        if (user.gender == 1) {
            eventsListCell.info.setText("жен, " + user.age +" лет");
        } else {
            eventsListCell.info.setText("муж, " + user.age + "лет");
        }

        return cellView;
    }

    class EventsListCell {

        public CircleImageView avatar;
        public TextView name;
        public TextView info;


        public EventsListCell(View base) {

            avatar = (CircleImageView) base.findViewById(R.id.account_ava);

            name = (TextView) base.findViewById(R.id.account_name);
            info = (TextView) base.findViewById(R.id.account_info);
        }
    }

}