package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.halfbit.pinnedsection.PinnedSectionListView;

public class DietAdapter extends ArrayAdapter<DietItem> implements PinnedSectionListView.PinnedSectionListAdapter {

    List<Food> diet1Array = new ArrayList<Food>();
    List<Food> diet2Array = new ArrayList<Food>();
    List<Food> diet3Array = new ArrayList<Food>();
    List<Food> diet4Array = new ArrayList<Food>();
    List<Food> diet5Array = new ArrayList<Food>();

    List<Dish> diet1dishArray = new ArrayList<Dish>();
    List<Dish> diet2dishArray = new ArrayList<Dish>();
    List<Dish> diet3dishArray = new ArrayList<Dish>();
    List<Dish> diet4dishArray = new ArrayList<Dish>();
    List<Dish> diet5dishArray = new ArrayList<Dish>();

    private static final int[] COLORS = new int[] {
            R.color.green_light, R.color.orange_light,
            R.color.blue_light, R.color.red_light };

    public DietAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        generateDataset('A', 'Z', false);
    }

    public DietAdapter(Context context, int resource, int textViewResourceId,
                       List<Food> diet1Array,
                       List<Food> diet2Array,
                       List<Food> diet3Array,
                       List<Food> diet4Array,
                       List<Food> diet5Array,

                       List<Dish> diet1dishArray,
                       List<Dish> diet2dishArray,
                       List<Dish> diet3dishArray,
                       List<Dish> diet4dishArray,
                       List<Dish> diet5dishArray
                    ) {

        super(context, resource, textViewResourceId);

        //generateDataset('A', 'Z', false);

        this.diet1Array = diet1Array;
        this.diet2Array = diet2Array;
        this.diet3Array = diet3Array;
        this.diet4Array = diet4Array;
        this.diet5Array = diet5Array;

        this.diet1dishArray = diet1dishArray;
        this.diet2dishArray = diet2dishArray;
        this.diet3dishArray = diet3dishArray;
        this.diet4dishArray = diet4dishArray;
        this.diet5dishArray = diet5dishArray;

        generateDietDataset();

    }

    public void generateDataset(char from, char to, boolean clear) {

        if (clear) clear();

        final int sectionsNumber = to - from + 1;
        prepareSections(sectionsNumber);

        int sectionPosition = 0, listPosition = 0;
        for (char i=0; i<sectionsNumber; i++) {
            DietItem section = new DietItem(DietItem.SECTION, String.valueOf((char)('A' + i)));
            section.sectionPosition = sectionPosition;
            section.listPosition = listPosition++;
            onSectionAdded(section, sectionPosition);
            add(section);

            final int itemsNumber = (int) Math.abs((Math.cos(2f*Math.PI/3f * sectionsNumber / (i+1f)) * 25f));
            for (int j=0;j<itemsNumber;j++) {
                DietItem item = new DietItem(DietItem.FOOD, section.name.toUpperCase(Locale.ENGLISH) + " - " + j);
                item.sectionPosition = sectionPosition;
                item.listPosition = listPosition++;
                add(item);
            }

            sectionPosition++;
        }
    }

    public void generateDietDataset() {

        final int sectionsNumber = 5;
        prepareSections(sectionsNumber);

        String[] sectionNames = {"Завтрак", "Первый перекус", "Обед", "Второй перекус", "Ужин"};

        int sectionPosition = 0, listPosition = 0;
        for (char i=0; i<sectionsNumber; i++) {
            DietItem section = new DietItem(DietItem.SECTION, sectionNames[i]);//String.valueOf((char)('A' + i)));
            section.sectionPosition = sectionPosition;
            section.listPosition = listPosition++;
            onSectionAdded(section, sectionPosition);
            add(section);

            int count = 0;
            switch (sectionPosition) {
                case 0:
                    count = diet1Array.size() + diet1dishArray.size();
                    break;
                case 1:
                    count = diet2Array.size() + diet2dishArray.size();
                    break;
                case 2:
                    count = diet3Array.size() + diet3dishArray.size();
                    break;
                case 3:
                    count = diet4Array.size() + diet4dishArray.size();
                    break;
                case 4:
                    count = diet5Array.size() + diet5dishArray.size();
                    break;
                default:
                    break;
            }

            final int itemsNumber = count;//(int) Math.abs((Math.cos(2f*Math.PI/3f * sectionsNumber / (i+1f)) * 25f));
            for (int j=0;j<itemsNumber;j++) {

                List<Food> dietArray = new ArrayList<>();
                List<Dish> dietDishArray = new ArrayList<>();

                switch (sectionPosition) {
                    case 0:
                        dietArray = diet1Array;
                        dietDishArray = diet1dishArray;
                        break;
                    case 1:
                        dietArray = diet2Array;
                        dietDishArray = diet2dishArray;
                        break;
                    case 2:
                        dietArray = diet3Array;
                        dietDishArray = diet3dishArray;
                        break;
                    case 3:
                        dietArray = diet4Array;
                        dietDishArray = diet4dishArray;
                        break;
                    case 4:
                        dietArray = diet5Array;
                        dietDishArray = diet5dishArray;
                        break;
                    default:
                        break;
                }

                if (j < dietArray.size()) {
                    DietItem item = new DietItem(DietItem.FOOD, dietArray.get(j).name + " - " + j);
                    Log.d("DietAdapter", "row for food: " + dietArray.get(j).name);
                    item.sectionPosition = sectionPosition;
                    item.listPosition = listPosition++;
                    add(item);
                } else if (dietDishArray.size() > 0){
                    DietItem item = new DietItem(DietItem.FOOD, dietDishArray.get(j).name + " - " + j);
                    Log.d("DietAdapter", "row for dish: " + dietDishArray.get(j).name);
                    item.sectionPosition = sectionPosition;
                    item.listPosition = listPosition++;
                    add(item);
                }

            }

            sectionPosition++;
        }
    }

    protected void prepareSections(int sectionsNumber) { }

    protected void onSectionAdded(DietItem section, int sectionPosition) { }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextColor(Color.DKGRAY);
        view.setTag("" + position);
        DietItem item = getItem(position);
        if (item.type == DietItem.SECTION) {
            //view.setOnClickListener(PinnedSectionListActivity.this);
            view.setBackgroundColor(parent.getResources().getColor(COLORS[item.sectionPosition % COLORS.length]));
        }
        return view;
    }

    @Override public int getViewTypeCount() {
        return 2;
    }

    @Override public int getItemViewType(int position) {
        return getItem(position).type;
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == DietItem.SECTION;
    }
}
