package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

public class Food {

    public String name;
    public Integer id;
    public String calories;
    public String count;
    public Integer measure;

    public Food(String name, int id, String calories, String count, int measure) {

        this.name = name;
        this.id = id;
        this.calories = calories;
        this.count = count;
        this.measure = measure;

    }

}
