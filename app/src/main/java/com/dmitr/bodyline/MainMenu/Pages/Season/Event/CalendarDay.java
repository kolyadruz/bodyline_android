package com.dmitr.bodyline.MainMenu.Pages.Season.Event;

public class CalendarDay {

    public int today;
    public int day;
    public int diets;
    public int lessons;
    public int measures;
    public int tasks;
    public int weight;
    public String s_token;

    public CalendarDay(){

    }

    public CalendarDay (int today, int day, int diets, int lessons, int measures, int tasks, int weight, String s_token){

        this.today = today;
        this.day = day;
        this.diets = diets;
        this.lessons = lessons;
        this.measures = measures;
        this.tasks = tasks;
        this.weight = weight;

        this.s_token = s_token;

    }

}
