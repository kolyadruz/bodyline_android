package com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.OFFLINE.AppModeChooser;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import de.hdodenhof.circleimageview.CircleImageView;
import info.hoang8f.android.segmented.SegmentedGroup;

public class ProfileActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 1, CAMERA = 2;

    ConstraintLayout fragProfLoad;

    String token, imagePath;

    CircleImageView circleView;

    EditText fam, imya, otch, address, work;
    Integer selectedGender, selectedKids, fam_status, lifestyle;

    Button loadButton, birthday, fam_status_btn, lifestyle_btn, saveButton;
    SegmentedGroup gender;

    SegmentedGroup childs;

    Calendar date = Calendar.getInstance();

    //Boolean isFirstTimeSaving = false;

    Activity activity;
    Context appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        activity = this;
        appContext = getApplicationContext();

        saveStandartAvatar();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);

        fragProfLoad = findViewById(R.id.fragProfLoad);

        fragProfLoad.setVisibility(View.INVISIBLE);

        circleView = findViewById(R.id.circleView);
        if (imagePath != "") {
            circleView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        } else {
            circleView.setImageResource(R.drawable.profile);
        }

        loadButton = findViewById(R.id.loadButton);
        loadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Смена изображения профиля")
                        .setMessage("Сделать новую фотографию с помощью камеры или выбрать существующее изображение из галереи")
                        .setCancelable(false)
                        .setPositiveButton("Выбрать из галереи", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                startActivityForResult(intent, RESULT_LOAD_IMAGE);

                            }
                        })
                        .setNegativeButton("Сделать фотографию", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                                /*SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
                                SharedPreferences.Editor editor = mSettings.edit();

                                editor.putBoolean("isFromCamera", true);
                                editor.commit();*/

                                startActivityForResult(intent, CAMERA);
                            }
                        });


                AlertDialog alert = builder.create();
                if (!activity.isFinishing()) {
                    alert.show();
                }

            }
        });

        fam = findViewById(R.id.famText);
        fam.setText(mSettings.getString("fam", ""));

        imya = findViewById(R.id.imyaText);
        imya.setText(mSettings.getString("imya", ""));


        otch = findViewById(R.id.otchestvoText);
        otch.setText(mSettings.getString("otch", ""));


        birthday = findViewById(R.id.datebtn);
        birthday.setText(mSettings.getString("birthday", ""));
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bd = birthday.getText().toString();

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

                Calendar dateBD = Calendar.getInstance();

                try {
                    dateBD.setTime(dateFormat.parse(bd));
                }catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!activity.isFinishing()) {
                    new DatePickerDialog(activity, d,
                            dateBD.get(dateBD.YEAR),
                            dateBD.get(dateBD.MONTH),
                            dateBD.get(dateBD.DAY_OF_MONTH))
                            .show();
                }
            }
        });

        gender = findViewById(R.id.segment);
        selectedGender = mSettings.getInt("gender", -1);
        if (selectedGender == 0) {
            gender.check(R.id.manbtn);
        } else if (selectedGender == 1) {
            gender.check(R.id.womanbtn);
        }

        gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.manbtn: selectedGender = 0;
                        switch (fam_status) {
                            case 2:
                                fam_status_btn.setText("Холост");
                                break;
                            case 3:
                                fam_status_btn.setText("Женат");
                                break;
                            case 4:
                                fam_status_btn.setText("В отношениях");
                                break;
                            case 5:
                                fam_status_btn.setText("Другое");
                                break;
                        }

                        break;
                    case R.id.womanbtn: selectedGender = 1;
                        switch (fam_status) {
                            case 2:
                                fam_status_btn.setText("Холоста");
                                break;
                            case 3:
                                fam_status_btn.setText("Замужем");
                                break;
                            case 4:
                                fam_status_btn.setText("В отношениях");
                                break;
                            case 5:
                                fam_status_btn.setText("Другое");
                                break;
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        fam_status_btn = findViewById(R.id.familybtn);
        fam_status = mSettings.getInt("fam_status", -1);

        if (selectedGender == 0) {
            if (fam_status == 2) {
                fam_status_btn.setText("Холост");
            } else if (fam_status == 3) {
                fam_status_btn.setText("Женат");
            } else if (fam_status == 4) {
                fam_status_btn.setText("В отношениях");
            } else if (fam_status == 5) {
                fam_status_btn.setText("Другое");
            }

        } else if (selectedGender == 1) {
            if (fam_status == 2) {
                fam_status_btn.setText("Холоста");
            } else if (fam_status == 3) {
                fam_status_btn.setText("Замужем");
            } else if (fam_status == 4) {
                fam_status_btn.setText("В отношениях");
            } else if (fam_status == 5) {
                fam_status_btn.setText("Другое");
            }
        }

        fam_status_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Выберите семейное положение");

                if (selectedGender == 1) {
                    String[] familyStatus = {"Холоста", "Замужем", "В отношениях", "Другое"};
                    builder.setItems(familyStatus, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    fam_status_btn.setText("Холоста");
                                    fam_status = 2;
                                    break;
                                case 1:
                                    fam_status_btn.setText("Замужем");
                                    fam_status = 3;
                                    break;
                                case 2:
                                    fam_status_btn.setText("В отношениях");
                                    fam_status = 4;
                                    break;
                                case 3:
                                    fam_status_btn.setText("Другое");
                                    fam_status = 5;
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                } else if (selectedGender == 0) {
                    String[] familyStatus = {"Холост", "Женат", "В отношениях", "Другое"};
                    builder.setItems(familyStatus, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    fam_status_btn.setText("Холост");
                                    fam_status = 2;
                                    break;
                                case 1:
                                    fam_status_btn.setText("Женат");
                                    fam_status = 3;
                                    break;
                                case 2:
                                    fam_status_btn.setText("В отношениях");
                                    fam_status = 4;
                                    break;
                                case 3:
                                    fam_status_btn.setText("Другое");
                                    fam_status = 5;
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                }

                AlertDialog dialog = builder.create();
                if (!activity.isFinishing()) {
                    dialog.show();
                }
            }
        });

        childs = findViewById(R.id.segment2);
        selectedKids = mSettings.getInt("childs", -1);
        if (selectedKids == 1) {
            childs.check(R.id.havechld);
        } else if (selectedKids == 0) {
            childs.check(R.id.notchld);
        }
        childs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.havechld: selectedKids = 1;
                    case R.id.notchld: selectedKids = 0;
                }
            }
        });

        address = findViewById(R.id.addresstext);
        address.setText(mSettings.getString("address", ""));

        work = findViewById(R.id.worktext);
        work.setText(mSettings.getString("work", ""));

        lifestyle_btn = findViewById(R.id.lifestylebtn);
        lifestyle = mSettings.getInt("lifestyle", -1);
        if (lifestyle == 1) {
            lifestyle_btn.setText("Сидячий");
        } else if (lifestyle == 2) {
            lifestyle_btn.setText("Малоактивный");
        } else if (lifestyle == 3) {
            lifestyle_btn.setText("Активный");
        } else if (lifestyle == 4) {
            lifestyle_btn.setText("Гиперактивный");
        }

        lifestyle_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Выберите семейное положение");

                String[] familyStatus = {"Сидячий", "Малоактивный", "Активный", "Гиперактивный"};
                builder.setItems(familyStatus, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0: lifestyle_btn.setText("Сидячий"); lifestyle = 1;
                                break;
                            case 1: lifestyle_btn.setText("Малоактивный"); lifestyle = 2;
                                break;
                            case 2: lifestyle_btn.setText("Активный"); lifestyle = 3;
                                break;
                            case 3: lifestyle_btn.setText("Гиперактивный"); lifestyle = 4;
                                break;
                            default:
                                break;
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                if (!activity.isFinishing()) {
                    dialog.show();
                }
            }
        });



        saveButton = findViewById(R.id.saveButton);
        saveButton.bringToFront();
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = 0;
                if (fam.length() != 0) {
                    ++i;
                }
                if (imya.length() != 0) {
                    ++i;
                }
                if (otch.length() != 0) {
                    ++i;
                }
                if (birthday.length() != 0) {
                    ++i;
                }
                if (selectedGender < 2) {
                    ++i;
                }
                if (fam_status_btn.length() != 0) {
                    ++i;
                }
                if (selectedKids < 2) {
                    ++i;
                }
                if (address.length() != 0) {
                    ++i;
                }
                if (work.length() != 0) {
                    ++i;
                }
                if (lifestyle_btn.length() != 0) {
                    ++i;
                }

                if (i == 10) {


                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
                    final SharedPreferences.Editor editor = mSettings.edit();
                    token = mSettings.getString("token", "");

                    Log.d("Profile", "token:" + token);

                    final String famStr = fam.getText().toString();
                    final String imyaStr = imya.getText().toString();
                    final String otchStr = otch.getText().toString();
                    String genderStr = "" + selectedGender;
                    final String birthdayStr = birthday.getText().toString();
                    String fam_statusStr = String.valueOf(fam_status);
                    final String workStr = work.getText().toString();
                    String childsStr = String.valueOf(selectedKids);
                    final String addressStr = address.getText().toString();
                    String lifestyleStr = "" + lifestyle;

                    Log.d("birthday", "" + birthdayStr);

                    AsyncHttpClient client = new AsyncHttpClient();

                    JSONObject data = new JSONObject();
                    JSONObject dataparams = new JSONObject();
                    StringEntity entity = new StringEntity("", "");
                    try {
                        dataparams.put("fam", famStr);
                        dataparams.put("imya", imyaStr);
                        dataparams.put("otch", otchStr);
                        dataparams.put("gender", genderStr);
                        dataparams.put("birthday", birthdayStr);
                        dataparams.put("fam_status", fam_statusStr);
                        dataparams.put("work", workStr);
                        dataparams.put("childs", childsStr);
                        dataparams.put("address", addressStr);
                        dataparams.put("lifestyle", lifestyleStr);
                        data.put("data", dataparams);

                        entity = new StringEntity(data.toString(), "UTF-8");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                    client.addHeader("Authorization", "Bearer " +token);
                    Log.d("data", "" + data);
                    client.post(appContext, "http://bodyline14.ru/mobile/addpersonal", entity, "application/json; charset=utf-8", new AsyncHttpResponseHandler() {

                        @Override
                        public void onStart() {
                            // called before request is started
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            // called when response HTTP status is "200 OK"
                            if (statusCode == 200) {
                                String str = new String(responseBody);
                                JSONObject json;
                                try {
                                    json = new JSONObject(str);
                                    Integer err_id = json.getInt("err_id");
                                    if (err_id == 0) {

                                        try {
                                            fragProfLoad.setVisibility(View.VISIBLE);
                                            fragProfLoad.bringToFront();
                                            AsyncHttpClient clientUplImg = new AsyncHttpClient();
                                            RequestParams params = new RequestParams();
                                            final File image = new File(imagePath);

                                            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
                                            SharedPreferences.Editor editor = mSettings.edit();
                                            editor.putString("imagePath", imagePath);
                                            editor.commit();

                                            params.setForceMultipartEntityContentType(true);
                                            params.put("file", image, "images/jpeg");
                                            clientUplImg.addHeader("Authorization", "Bearer " +token);
                                            clientUplImg.post(appContext, "http://bodyline14.ru/mobile/uploadavatar", params, new AsyncHttpResponseHandler() {

                                                @Override
                                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                    if (statusCode == 200) {

                                                        fragProfLoad.setVisibility(View.INVISIBLE);

                                                        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
                                                        SharedPreferences.Editor editor = mSettings.edit();
                                                        editor.putString("imagePath", imagePath);
                                                        editor.commit();

                                                                        /*if (isFirstTimeSaving) {
                                                                            editor.putBoolean("isFirstTimeSaving", false);
                                                                        }*/

                                                        editor.putString("fam", famStr);
                                                        editor.putString("imya", imyaStr);
                                                        editor.putString("otch", otchStr);
                                                        editor.putInt("gender", selectedGender);
                                                        editor.putString("birthday", birthdayStr);
                                                        editor.putInt("fam_status", fam_status);
                                                        editor.putString("work", workStr);
                                                        editor.putInt("childs", selectedKids);
                                                        editor.putString("address", addressStr);
                                                        editor.putInt("lifestyle", lifestyle);
                                                        editor.commit();

                                                                        /*if (isFirstTimeSaving) {
                                                                            ((MainWithBottom)getActivity()).openMain();
                                                                        }*/

                                                        //Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);

                                                        Intent intent = new Intent(getApplicationContext(), AppModeChooser.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                                        startActivity(intent);


                                                        String Message = "Личные данные сохранены";
                                                        builder.setMessage(Message)
                                                                .setCancelable(false)
                                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                    public void onClick(DialogInterface dialog, int id) {


                                                                    }
                                                                });
                                                        AlertDialog alert = builder.create();
                                                        if (!activity.isFinishing()) {
                                                            alert.show();
                                                        }
                                                    }
                                                    Log.d("uploadavatar", "" +image);
                                                }

                                                @Override
                                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                    String Message = "Интернет соединение отсутствует";
                                                    builder.setMessage(Message)
                                                            .setCancelable(false)
                                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                }
                                                            });

                                                    AlertDialog alert = builder.create();
                                                    if (!activity.isFinishing()) {
                                                        alert.show();
                                                    }
                                                }
                                            });

                                            Integer age = json.getInt("age");
                                            try {
                                                editor.putInt("age", age);
                                                editor.commit();
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }

                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                        Log.d("AddPersonal:", "" + json);
                                    } else {
                                        showErrID(err_id, saveButton);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                            // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                            String Message = "Интернет соединение отсутствует";
                            builder.setMessage(Message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    });
                        }

                        @Override
                        public void onRetry(int retryNo) {
                            // called when request is retried
                        }
                    });
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    String Message = "Заполните все поля";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

        });

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("Login:","Ошибка работы токена. Переход в Register.java");
                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    private void saveStandartAvatar() {

        Bitmap bitMap = BitmapFactory.decodeResource(getResources(),R.drawable.profile);

        File mFile1 = appContext.getFilesDir();

        String fileName ="avatar.jpg";

        File mFile2 = new File(mFile1,fileName);
        try {
            FileOutputStream outStream;

            outStream = new FileOutputStream(mFile2);

            bitMap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

            outStream.flush();

            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sdPath = mFile1.getAbsolutePath().toString()+"/"+fileName;

        imagePath = sdPath;

        Log.i("hiya", "Your IMAGE ABSOLUTE PATH:-"+sdPath);

        File temp=new File(sdPath);

        if(!temp.exists()){
            Log.e("file","no image file at location :"+sdPath);
        }

    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            date.set(Calendar.YEAR, year);
            date.set(Calendar.MONTH, month);
            date.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDate();
        }
    };

    private void setInitialDate() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String dateTime = dateFormat.format(date.getTime());
        birthday.setText(dateTime);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = new String[]{MediaStore.Images.Media.DATA};

            Cursor cursor = activity.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            //imagePath = picturePath;

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //4
            File file = new File(activity.getFilesDir() + "/avatar.jpg");
            try {
                file.createNewFile();
                FileOutputStream fo = new FileOutputStream(file);
                //5
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            circleView.setImageBitmap(BitmapFactory.decodeFile(imagePath));


        } else if (requestCode == CAMERA && resultCode == RESULT_OK && null !=data) {

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            circleView.setImageBitmap(thumbnail);
            //3
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //4
            File file = new File(activity.getFilesDir() + "/avatar.jpg");
            try {
                file.createNewFile();
                FileOutputStream fo = new FileOutputStream(file);
                //5
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            circleView.setImageBitmap(BitmapFactory.decodeFile(imagePath));

        }

    }

}
