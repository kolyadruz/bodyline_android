package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.halfbit.pinnedsection.PinnedSectionListView;

public class Diet extends AppCompatActivity {

    DietBaseAdapter dietAdapter;
    FastScrollAdapter fastScrollAdapter;

    private boolean hasHeaderAndFooter;
    private boolean isFastScroll;
    private boolean addPadding;
    private boolean isShadowVisible = true;

    List<JSONObject> dietsArray = new ArrayList<JSONObject>();

    List<Food> diet1Array = new ArrayList<>();
    List<Food> diet2Array = new ArrayList<>();
    List<Food> diet3Array = new ArrayList<>();
    List<Food> diet4Array = new ArrayList<>();
    List<Food> diet5Array = new ArrayList<>();

    List<Dish> diet1dishArray = new ArrayList<>();
    List<Dish> diet2dishArray = new ArrayList<>();
    List<Dish> diet3dishArray = new ArrayList<>();
    List<Dish> diet4dishArray = new ArrayList<>();
    List<Dish> diet5dishArray = new ArrayList<>();

    List<DietItem> dietItemsArray = new ArrayList<>();

    String s_token;
    int day;

    PinnedSectionListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_diet);

        listView = findViewById(R.id.pinnedlistview);

        s_token = getIntent().getStringExtra("s_token");
        day = getIntent().getIntExtra("day", 0);

        if (savedInstanceState != null) {
            isFastScroll = savedInstanceState.getBoolean("isFastScroll");
            addPadding = savedInstanceState.getBoolean("addPadding");
            isShadowVisible = savedInstanceState.getBoolean("isShadowVisible");
            hasHeaderAndFooter = savedInstanceState.getBoolean("hasHeaderAndFooter");
        }
        initializeHeaderAndFooter();
        initializeAdapter();
        initializePadding();
    }

    @Override
    protected void onResume() {
        super.onResume();

        clearAllArrays();
        notifyAdaptersDataSetChanged();
        runSeasonDayData();

    }

    private void clearAllArrays() {

        dietsArray.clear();

        diet1Array.clear();
        diet2Array.clear();
        diet3Array.clear();
        diet4Array.clear();
        diet5Array.clear();

        diet1dishArray.clear();
        diet2dishArray.clear();
        diet3dishArray.clear();
        diet4dishArray.clear();
        diet5dishArray.clear();

        dietItemsArray.clear();

    }

    private void runSeasonDayData() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String token = mSettings.getString("token", "");

        seasonDayData(token);

    }

    private void seasonDayData(String token) {

        final WeakReference<Diet> dietActivityWeakRef;

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/seasondaydata", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200){
                    String str = new String(responseBody);
                    try {
                        final JSONObject json = new JSONObject(str);

                        Log.d("Diet", ""+json);

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){

                            clearAllArrays();

                            JSONObject diets = json.getJSONObject("diets");

                            JSONObject diet1 = diets.getJSONObject("diet1");
                            dietsArray.add(diet1);
                            JSONObject diet2 = diets.getJSONObject("diet2");
                            dietsArray.add(diet2);
                            JSONObject diet3 = diets.getJSONObject("diet3");
                            dietsArray.add(diet3);
                            JSONObject diet4 = diets.getJSONObject("diet4");
                            dietsArray.add(diet4);
                            JSONObject diet5 = diets.getJSONObject("diet5");
                            dietsArray.add(diet5);

                            Log.d("Diet", "dietsArray: "+ dietsArray.toString());

                            String[] sectionNames = {"Завтрак", "Первый перекус", "Обед", "Второй перекус", "Ужин"};

                            for (int i = 0; i < dietsArray.size(); i++) {

                                JSONObject diet = dietsArray.get(i);

                                if (diet != null) {

                                    DietItem section = new DietItem(DietItem.SECTION, sectionNames[i], 0, "", "", 0, "", "");
                                    section.sectionPosition = i;
                                    dietItemsArray.add(section);

                                    try {
                                        JSONObject food = diet.getJSONObject("food");
                                        JSONArray foodArray = food.getJSONArray("food");

                                        for (int j = 0; j < foodArray.length(); j++) {

                                            JSONObject data = foodArray.getJSONObject(j);

                                            String count = data.getString("mcount");
                                            Integer measure = data.getInt("measure");
                                            String calories = data.getString("calories");
                                            Integer id = data.getInt("id");
                                            String name = data.getString("name");

                                            DietItem foodObj = new DietItem(DietItem.FOOD, name, id, calories, count, measure, "", "");
                                            dietItemsArray.add(foodObj);

                                            /*Food foodObj = new Food(name, id, calories, count, measure);

                                            Log.d("Diet", "added food: "+foodObj.name);

                                            switch (i) {
                                                case 0:
                                                    diet1Array.add(foodObj);
                                                    break;
                                                case 1:
                                                    diet2Array.add(foodObj);
                                                    break;
                                                case 2:
                                                    diet3Array.add(foodObj);
                                                    break;
                                                case 3:
                                                    diet4Array.add(foodObj);
                                                    break;
                                                case 4:
                                                    diet5Array.add(foodObj);
                                                    break;
                                                default:
                                                    break;

                                            }*/

                                        }
                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }


                                    try {
                                        JSONArray dishesArray = diet.getJSONArray("dishes");

                                        for (int k = 0; k < dishesArray.length(); k++) {

                                            JSONObject data = dishesArray.getJSONObject(k);

                                            JSONArray composition = data.getJSONArray("composition");

                                            if (composition.length() > 0) {
                                                //Продукты блюда
                                            }

                                            String kcal = data.getString("dcal");
                                            String description = data.getString("description");
                                            String name = data.getString("name");
                                            String portion = data.getString("portion");
                                            Integer id = data.getInt("id");

                                            DietItem dishObj = new DietItem(DietItem.DISH, name, id, kcal, "", 0, portion, description);
                                            dietItemsArray.add(dishObj);

                                            /*Dish dishObj = new Dish(name, id, kcal, portion, description);

                                            Log.d("Diet", "added dish: "+dishObj.name);

                                            switch (i) {
                                                case 0:
                                                    diet1dishArray.add(dishObj);
                                                    break;
                                                case 1:
                                                    diet2dishArray.add(dishObj);
                                                    break;
                                                case 2:
                                                    diet3dishArray.add(dishObj);
                                                    break;
                                                case 3:
                                                    diet4dishArray.add(dishObj);
                                                    break;
                                                case 4:
                                                    diet5dishArray.add(dishObj);
                                                    break;
                                                default:
                                                    break;
                                            }*/

                                        }
                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }

                                }

                            }


                            /*Log.d("Diet",
                                " diet1: " + diet1Array.size()
                                    +" diet1dish: " + diet1dishArray.size()

                                    +" diet2: " + diet2Array.size()
                                    +" diet2dish: " + diet2dishArray.size()

                                    +" diet3: " + diet3Array.size()
                                    +" diet3dish: " + diet3dishArray.size()

                                    +" diet4: " + diet4Array.size()
                                    +" diet4dish: " + diet4dishArray.size()

                                    +" diet5: " + diet5Array.size()
                                    +" diet5dish: " + diet5dishArray.size()
                            );*/

                            Log.d("Diet",
                                    " dietItems count: " + dietItemsArray.size()
                            );

                            notifyAdaptersDataSetChanged();

                        } else {
                            //showErrID(err_id);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)

                if (!isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!isFinishing()) {
                        alert.show();
                    }
                }


            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isFastScroll", isFastScroll);
        outState.putBoolean("addPadding", addPadding);
        outState.putBoolean("isShadowVisible", isShadowVisible);
        outState.putBoolean("hasHeaderAndFooter", hasHeaderAndFooter);
    }

    private void initializeHeaderAndFooter() {
        //setListAdapter(null);
        listView.setAdapter(null);
        if (hasHeaderAndFooter) {
            ListView list = listView; //getListView();

            LayoutInflater inflater = LayoutInflater.from(this);
            //TextView header1 = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, list, false);
            TextView header1 = (TextView) inflater.inflate(R.layout.cell_diet_section, list, false);
            header1.setText("First header");
            list.addHeaderView(header1);

            //TextView header2 = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, list, false);
            TextView header2 = (TextView) inflater.inflate(R.layout.cell_diet_section, list, false);
            header2.setText("Second header");
            list.addHeaderView(header2);

            //TextView footer = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, list, false);
            TextView footer = (TextView) inflater.inflate(R.layout.cell_diet_section, list, false);
            footer.setText("Single footer");
            list.addFooterView(footer);
        }
        initializeAdapter();
    }

    @SuppressLint("NewApi")
    private void initializeAdapter() {
        //getListView().setFastScrollEnabled(isFastScroll);
        listView.setFastScrollEnabled(isFastScroll);
        if (isFastScroll) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                //getListView().setFastScrollAlwaysVisible(true);
                listView.setFastScrollAlwaysVisible(true);
            }
            //fastScrollAdapter = new FastScrollAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1);

            fastScrollAdapter = new FastScrollAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1,
                    dietItemsArray
            );

            //setListAdapter(fastScrollAdapter);
            listView.setAdapter(fastScrollAdapter);
        } else {
            //dietAdapter = new DietAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1);
            dietAdapter = new DietBaseAdapter(this, android.R.layout.simple_list_item_1, R.layout.cell_diet_food, R.layout.cell_diet_dish, dietItemsArray);

            //setListAdapter(dietAdapter);
            listView.setAdapter(dietAdapter);
        }
    }

    @SuppressLint("NewApi")
    private void notifyAdaptersDataSetChanged() {

        //getListView().setFastScrollEnabled(isFastScroll);
        listView.setFastScrollEnabled(isFastScroll);
        if (isFastScroll) {
            fastScrollAdapter.notifyDataSetChanged();
        } else {
            dietAdapter.notifyDataSetChanged();
        }

    }

    private void initializePadding() {
        float density = getResources().getDisplayMetrics().density;
        int padding = addPadding ? (int) (16 * density) : 0;
        //getListView().setPadding(padding, padding, padding, padding);
        listView.setPadding(padding, padding, padding, padding);
    }
}
