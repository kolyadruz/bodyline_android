package com.dmitr.bodyline.MainMenu.Pages.Chat;

public class Message {

    public String name;
    public String message;
    public String token;

    public String date;

    public int isAdmin;


    public boolean self;

    public Message(String name, String message, String token, Boolean self, int isAdmin, String date) {

        this.name = name;
        this.message = message;
        this.token = token;

        this.isAdmin = isAdmin;

        this.date = date;

        this.self = self;

    }

}
