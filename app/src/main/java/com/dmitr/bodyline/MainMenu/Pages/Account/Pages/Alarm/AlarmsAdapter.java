package com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;

public class AlarmsAdapter extends BaseAdapter {

    private Activity activity;

    private AlarmReceiver alarmReceiver;

    Context appContext;

    List<Alarm> alarms = new ArrayList<Alarm>();

    private int layoutID;
    private LayoutInflater inflater=null;

    //public event_id;

    int selectedPosition = 0;

    public AlarmsAdapter(Activity activity, List<Alarm> alarms, int layoutID) {
        this.activity = activity;

        this.appContext = activity.getApplicationContext();

        this.alarms = alarms;

        this.layoutID = layoutID;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        alarmReceiver = new AlarmReceiver();

    }

    public int getCount() {

        return alarms.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        AlarmCell alarmCell;
        if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            alarmCell = new AlarmCell(cellView);
            cellView.setTag(alarmCell);
        } else {
            alarmCell = (AlarmCell) cellView.getTag();
        }

        final Alarm alarm = this.alarms.get(position);

        alarmCell.descriptionTxt.setText(alarm.description);
        alarmCell.timeTxt.setText(alarm.time);

        alarmCell.alarmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
                SharedPreferences.Editor editor = mSettings.edit();

                int checked = 0;
                if (isChecked) {
                    checked = 1;
                }

                String title = "";
                String msg = "";

                switch (position) {
                    case 0:
                        editor.putInt("alarm_wake_checked", checked);
                        editor.commit();
                        title = "Время подъема";
                        msg = "Пора вставать";
                        break;
                    case 1:
                        editor.putInt("alarm_breakfast_checked", checked);
                        editor.commit();
                        title = "Напомнить о завтраке";
                        msg = "Вам пора позавтракать";
                        break;
                    case 2:
                        editor.putInt("alarm_first_snack_checked", checked);
                        editor.commit();
                        title = "Напомнить о первом перекусе";
                        msg = "Пришло время для первого перекуса";
                        break;
                    case 3:
                        editor.putInt("alarm_lunch_checked", checked);
                        editor.commit();
                        title = "Напомнить об обеде";
                        msg = "Вам пора пообедать";
                        break;
                    case 4:
                        editor.putInt("alarm_second_snack_checked", checked);
                        editor.commit();
                        title = "Напомнить о втором перекусе";
                        msg = "Пришло время перекуса после обеда";
                        break;
                    case 5:
                        editor.putInt("alarm_dinner_checked", checked);
                        editor.commit();
                        title = "Напомнить об ужине";
                        msg = "Вам пора поужинать";
                        break;
                    case 6:
                        editor.putInt("alarm_train_checked", checked);
                        editor.commit();
                        title = "Время тренировки";
                        msg = "Приступите к тренировкам прямо сейчас";
                        break;
                    case 7:
                        editor.putInt("alarm_sleep_checked", checked);
                        editor.commit();
                        title = "Время отбоя";
                        msg = "Вам пора ложиться спать";
                        break;
                    default:
                        break;
                }

                if (isChecked) {

                    if(alarmReceiver != null) {
                        alarmReceiver.SetAlarm(activity, title, msg, position);
                    } else {
                        Toast.makeText(appContext,"Alarm is null", Toast.LENGTH_SHORT).show();
                    }

                    /*String hoursStr = alarm.time;
                    String hours = hoursStr.substring(2,4);
                    Log.d("ADAPTER", "hours: "+hours);
                    String minutesStr = alarm.time;
                    String minutes = minutesStr.substring(5,7);
                    Log.d("ADAPTER", "minutes: "+minutes);

                    NotificationHelper.scheduleRepeatingRTCNotification(appContext, hours, minutes, position);
                    NotificationHelper.enableBootReceiver(appContext);*/

                } else {

                    if(alarmReceiver != null) {
                        alarmReceiver.CancelAlarm(activity, position);
                    } else {
                        Toast.makeText(appContext,"Alarm is null", Toast.LENGTH_SHORT).show();
                    }

                    /*NotificationHelper.cancelAlarmRTC(position);
                    NotificationHelper.disableBootReceiver(appContext);*/

                }

            }
        });

        alarmCell.alarmSwitch.setChecked(alarm.checked == 1);

        alarmCell.textsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("ADAPTER", "CLICKED position: "+position);


                String time = alarm.time;
                Integer hours = Integer.getInteger(time.substring(2,4), 0);
                Integer minutes = Integer.getInteger(time.substring(5,6), 0);

                selectedPosition = position;

                showDialog(hours, minutes);

            }
        });


        return cellView;
    }

    class AlarmCell {

        public TextView descriptionTxt;
        public TextView timeTxt;

        public Switch alarmSwitch;

        public LinearLayout textsLayout;

        public AlarmCell(View base) {

            descriptionTxt = (TextView) base.findViewById(R.id.descriptionTxt);
            timeTxt = (TextView) base.findViewById(R.id.timeTxt);

            alarmSwitch = (Switch) base.findViewById(R.id.switch1);

            textsLayout = (LinearLayout) base.findViewById(R.id.textsLayout);
        }
    }

    private void showDialog(int hours, int minutes) {
        TimePickerDialog tpd = new TimePickerDialog(activity, myCallBack, hours, minutes, true);
        if (!activity.isFinishing()) {
            tpd.show();
        }
    }

    TimePickerDialog.OnTimeSetListener myCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            Alarm alarm = alarms.get(selectedPosition);

            String hr = "";
            if (hourOfDay < 10) {
                hr = "0"+hourOfDay;
            } else {
                hr = ""+hourOfDay;
            }

            String mn = "";
            if (minute < 10) {
                mn = "0"+minute;
            } else {
                mn = ""+minute;
            }

            //myHour = hourOfDay;
            //myMinute = minute;

            alarm.time = "В " + hr + ":" + minute;

            alarms.set(selectedPosition, alarm);
            notifyDataSetChanged();

            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appContext);
            SharedPreferences.Editor editor = mSettings.edit();

            switch (selectedPosition) {
                case 0:
                    editor.putString("alarm_wake", alarm.time);
                    editor.commit();
                    break;
                case 1:
                    editor.putString("alarm_breakfast", alarm.time);
                    editor.commit();
                    break;
                case 2:
                    editor.putString("alarm_first_snack", alarm.time);
                    editor.commit();
                    break;
                case 3:
                    editor.putString("alarm_lunch", alarm.time);
                    editor.commit();
                    break;
                case 4:
                    editor.putString("alarm_second_snack", alarm.time);
                    editor.commit();
                    break;
                case 5:
                    editor.putString("alarm_dinner", alarm.time);
                    editor.commit();
                    break;
                case 6:
                    editor.putString("alarm_train", alarm.time);
                    editor.commit();
                    break;
                case 7:
                    editor.putString("alarm_sleep", alarm.time);
                    editor.commit();
                    break;
                default:
                    break;
            }

            //tvTime.setText("Time is " + myHour + " hours " + myMinute + " minutes");
        }
    };

}
