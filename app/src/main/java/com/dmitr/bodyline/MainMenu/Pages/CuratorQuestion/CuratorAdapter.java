package com.dmitr.bodyline.MainMenu.Pages.CuratorQuestion;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.List;

public class CuratorAdapter extends BaseAdapter {

    private Activity activity;

    private List<Curator> curators;

    private int layoutID;
    private LayoutInflater inflater = null;

    public CuratorAdapter(Activity activity, List<Curator> curators, int layoutID) {
        this.activity = activity;
        this.curators = curators;
        this.layoutID = layoutID;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() { return curators.size(); }

    public Object getItem(int position) { return position; }

    public long getItemId(int position) { return position; }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        CuratorsListCell curatorsListCell;
        if (editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            curatorsListCell = new CuratorsListCell(cellView);
            cellView.setTag(curatorsListCell);
        } else {
            curatorsListCell = (CuratorsListCell) cellView.getTag();
        }

        final Curator curator = curators.get(position);

        if (!curator.isAnswered) {
            curatorsListCell.quesTopCell.setVisibility(View.VISIBLE);
            curatorsListCell.quesDate.setVisibility(View.VISIBLE);
            curatorsListCell.quesBottomCell.setVisibility(View.VISIBLE);
            curatorsListCell.quesAnsLine.setVisibility(View.INVISIBLE);
            curatorsListCell.ansTopCell.setVisibility(View.INVISIBLE);
            curatorsListCell.answerBottomCell.setVisibility(View.INVISIBLE);
            curatorsListCell.ansDate.setVisibility(View.INVISIBLE);
        } else {
            curatorsListCell.quesTopCell.setVisibility(View.VISIBLE);
            curatorsListCell.quesDate.setVisibility(View.VISIBLE);
            curatorsListCell.quesBottomCell.setVisibility(View.VISIBLE);
            curatorsListCell.quesAnsLine.setVisibility(View.VISIBLE);
            curatorsListCell.ansTopCell.setVisibility(View.VISIBLE);
            curatorsListCell.answerBottomCell.setVisibility(View.VISIBLE);
            curatorsListCell.ansDate.setVisibility(View.VISIBLE);
        }

        curatorsListCell.quesDate.setText(curator.questionDate);
        curatorsListCell.quesBottomCell.setText(curator.question);
        curatorsListCell.ansDate.setText(curator.answerDate);
        curatorsListCell.answerBottomCell.setText(curator.answer);

        return cellView;
    }

    class CuratorsListCell {
        public TextView quesTopCell;
        public TextView quesDate;
        public TextView quesBottomCell;
        public TextView ansTopCell;
        public TextView quesAnsLine;
        public TextView answerBottomCell;
        public TextView ansDate;

        public CuratorsListCell(View base) {
            quesTopCell = (TextView) base.findViewById(R.id.quesTopCell);
            quesDate = (TextView) base.findViewById(R.id.quesDate);
            quesBottomCell = (TextView) base.findViewById(R.id.quesBottomCell);
            ansTopCell = (TextView) base.findViewById(R.id.ansTopCell);
            quesAnsLine = (TextView) base.findViewById(R.id.quesAnsLine);
            answerBottomCell = (TextView) base.findViewById(R.id.answerBottomCell);
            ansDate = (TextView) base.findViewById(R.id.ansDate);
        }

    }

}


