package com.dmitr.bodyline.MainMenu.Pages.Season.Measure;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.dmitr.bodyline.App.DApplication;
import com.dmitr.bodyline.MainMenu.Pages.Season.Data;
import com.dmitr.bodyline.R;
import com.dmitr.bodyline.trainandtask.Video;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.FetchObserver;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.Reason;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import im.ene.toro.exoplayer.ExoCreator;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.exoplayer.ToroExo;
import im.ene.toro.helper.ToroPlayerHelper;
import timber.log.Timber;

public class Measure extends AppCompatActivity implements FetchObserver<Download> {

    private static final int RESULT_LOAD_IMAGE = 1, CAMERA = 2;

    int accessLevel = 0, day = 0, completed = 0;
    String url = "", descriptionText = "", s_token = "";

    ScrollView paramsScrollView;
    Video video = new Video();

    //Описание замеров
    TextView descriptionLabel;

    //ImageViews
    ImageView imgView, frontImgView, sideImgView, backImgView;
    ImageButton frontImgBtn, sideImgBtn, backImgBtn;

    //Pickers

    //editTexts
    Button heightField, weightField, wantWeightField, breastField, waistField, stomachField, hipsField, footField;
    File frontImgData, sideImgData, backImgData;
    Button saveBtn;


    int tappedImgIndex = 0;

    //Player
    RelativeLayout playerFrame;
    PlayerView playerView;
    ExoCreator creator;
    MediaSource mediaSource;
    SimpleExoPlayer exoPlayer;

    ConstraintLayout savingView;

    //ДИАЛОГИ
    ArrayList<String> kgValues = new ArrayList<>();
    ArrayList<String> grValues = new ArrayList<>();

    ArrayList<String> cmValues = new ArrayList<>();
    ArrayList<String> mmValues = new ArrayList<>();

    final Playable.EventListener listener = new Playable.DefaultEventListener() {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            super.onPlayerStateChanged(playWhenReady, playbackState);

            boolean active = playbackState > Player.STATE_IDLE && playbackState < Player.STATE_ENDED;
            playerView.setKeepScreenOn(active);

        }
    };

    //ExoPlayer playerView;
    ProgressBar progressBar;
    TextView progressView;
    Button downloadButton;
    ImageView thumbs;

    //DownloadManager mDownloadManager;

    @Nullable
    ToroPlayerHelper helper;
    @Nullable private Uri mediaUri;

    private static final int STORAGE_PERMISSION_CODE = 100;
    private View mainView;
    private Request request;
    private Fetch fetch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure);

        mainView = findViewById(R.id.activity_single_download);

        fetch = Fetch.Impl.getDefaultInstance();
        //checkStoragePermission();

        s_token = getIntent().getStringExtra("s_token");
        day = getIntent().getIntExtra("day", 0);
        descriptionText = getIntent().getStringExtra("descriptionText");
        accessLevel = getIntent().getIntExtra("accessLevel", 0);

        paramsScrollView = findViewById(R.id.paramsScrollView);

        setupPlayer();
        setupViews();

    }

    private void setupPlayer() {

        playerFrame = findViewById(R.id.playerFrame);
        progressBar = findViewById(R.id.player_progress_bar);
        progressView = findViewById(R.id.progress_text);
        descriptionLabel = findViewById(R.id.description);
        thumbs = findViewById(R.id.thumbs);
        downloadButton = findViewById(R.id.player_state_button);

        playerView = findViewById(R.id.player);
        creator = DApplication.getExoCreator();

        exoPlayer = ToroExo.with(this).requestPlayer(creator);

        playerView.setPlayer(null);
        exoPlayer.removeListener(listener);

    }

    private void setupViews() {

        imgView = findViewById(R.id.imgView);

        savingView = findViewById(R.id.savingView);

        heightField = findViewById(R.id.height_txt);
        weightField = findViewById(R.id.weight_txt);
        wantWeightField = findViewById(R.id.wantweight_txt);
        breastField = findViewById(R.id.breast_txt);
        waistField = findViewById(R.id.waist_txt);
        stomachField = findViewById(R.id.stomach_txt);
        hipsField = findViewById(R.id.hips_txt);
        footField = findViewById(R.id.feet_txt);

        setupDialogBtns();

        frontImgView = findViewById(R.id.frontImgView);
        sideImgView = findViewById(R.id.sideImgView);
        backImgView = findViewById(R.id.backImgView);

        frontImgBtn = findViewById(R.id.frontImgBtn);
        sideImgBtn = findViewById(R.id.sideImgBtn);
        backImgBtn = findViewById(R.id.backImgBtn);

        saveBtn = findViewById(R.id.saveBtn);
        if (accessLevel < 2) {
            saveBtn.setVisibility(View.INVISIBLE);
        } else {
            saveBtn.setVisibility(View.VISIBLE);
        }

        descriptionLabel.setText(descriptionText);

    }

    private void setupDialogBtns() {

        for (int i = 35; i < 301; i++) {
            kgValues.add(""+i);
        }

        for (int i = 0; i < 10; i++) {
            grValues.add(""+i);
        }

        for (int i = 5; i < 201; i++) {
            cmValues.add(""+i);
        }

        for (int i = 0; i < 9; i++) {
            mmValues.add(""+i);
        }

        heightField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(heightField);
            }
        });

        weightField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeightDialog(weightField);
            }
        });


        wantWeightField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeightDialog(wantWeightField);
            }
        });

        breastField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(breastField);
            }
        });

        waistField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(waistField);
            }
        });

        stomachField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(stomachField);
            }
        });

        hipsField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(hipsField);
            }
        });

        footField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLengthDialog(footField);
            }
        });

    }

    private void preparePlayer (Uri videoUri) {
        mediaSource = creator.createMediaSource(videoUri, null);
        exoPlayer.addListener(listener);
        exoPlayer.prepare(mediaSource);
        playerView.setPlayer(exoPlayer);
    }

    @Override
    protected void onStart() {
        super.onStart();
        exoPlayer.setPlayWhenReady(false);
        runSeasonDayData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        exoPlayer.setPlayWhenReady(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("Measure", "RESUMED");
        hideSaveBtn();

        if (request != null) {
            fetch.attachFetchObserversForDownload(request.getId(), this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (request != null) {
            fetch.removeFetchObserversForDownload(request.getId(), this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playerView.setPlayer(null);
        exoPlayer.removeListener(listener);
        ToroExo.with(this).releasePlayer(creator, exoPlayer);
        fetch.close();
    }

    public void runSeasonDayData() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String token = mSettings.getString("token", "");

        seasonDayData(token, s_token, day, 2, 2);
    }

    public void seasonDayData(final String token, final String s_token, final int day, final int eventType, final int accessLevel) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/seasondaydata", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                savingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {

                    savingView.setVisibility(View.INVISIBLE);

                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("Measure", "seasondaydata: " + json.toString());



                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                //savingView.setVisibility(View.INVISIBLE);


                                JSONObject measure = json.getJSONObject("measure");

                                completed = measure.getInt("completed");
                                descriptionText = measure.getString("description");

                                String url_str = "";
                                boolean isImage;
                                String filename = "";
                                String thumbs = "";

                                try {

                                  String image_url = measure.getString("image_url");
                                    url_str = image_url;

                                    isImage = true;

                                } catch (JSONException ex) {
                                    ex.printStackTrace();

                                    url_str = measure.getString("video_url");
                                    filename = measure.getString("filename");
                                    thumbs = measure.getString("thumbs");

                                    isImage = false;
                                }

                                if (completed == 1) {
                                    saveBtn.setVisibility(View.INVISIBLE);
                                } else {
                                    saveBtn.setVisibility(View.VISIBLE);
                                }

                                if (accessLevel < 2) {
                                    saveBtn.setVisibility(View.INVISIBLE);
                                }

                                descriptionLabel.setText(descriptionText);

                                video.setFilename(filename);
                                video.setUrl(url_str);
                                video.setIsImage(isImage);
                                video.setThumbs(thumbs);

                                hideSaveBtn();

                                runGetDayMeasure();

                                updateViewData(video, completed);

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                savingView.setVisibility(View.INVISIBLE);
            }
        });

    }

    public void runGetDayMeasure() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String token = mSettings.getString("token", "");

        getDayMeasure(token, s_token, day);
    }

    private void getDayMeasure(String token, String s_token, Integer day) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getdaymeasure", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                savingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();

                    savingView.setVisibility(View.INVISIBLE);

                    try {
                        json = new JSONObject(str);

                        Log.d("Measure", "getdaymeasure: " + json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                //savingView.setVisibility(View.INVISIBLE);
                                JSONObject data = json.getJSONObject("data");

                                try {

                                    String height = data.getString("m_height");
                                    String vbreast = data.getString("m_vbreast");
                                    String vfoots = data.getString("m_vfoots");
                                    String vhip = data.getString("m_vhip");
                                    String vstomach = data.getString("m_vstomach");
                                    String vwaist = data.getString("m_vwaist");
                                    String want_weight = data.getString("m_want_weight");
                                    String weight = data.getString("m_weight");

                                    String photo1 = data.getString("m_photo1");
                                    String photo2 = data.getString("m_photo2");
                                    String photo3 = data.getString("m_photo3");


                                    Log.d("Measure: getdaymeasure", " height" + height
                                            + " vbreast" + vbreast
                                            + " vfoots" + vfoots
                                            + " vhip" + vhip
                                            + " vstomach" + vstomach
                                            + " vwaist" + vwaist
                                            + " want_weight" + want_weight
                                            + " weight" + weight);

                                    Picasso.get()
                                            .load(photo1)
                                            .fit()
                                            .centerCrop()
                                            .into(frontImgView);

                                    Picasso.get()
                                            .load(photo2)
                                            .fit()
                                            .centerCrop()
                                            .into(sideImgView);

                                    Picasso.get()
                                            .load(photo3)
                                            .fit()
                                            .centerCrop()
                                            .into(backImgView);

                                    if (!height.equals("")) {
                                        heightField.setText(height);
                                    }

                                    if (!weight.equals("")) {
                                        weightField.setText(weight);
                                    }

                                    if (!want_weight.equals("")) {
                                        wantWeightField.setText(want_weight);
                                    }

                                    if (!vbreast.equals("")) {
                                        breastField.setText(vbreast);
                                    }

                                    if (!vwaist.equals("")) {
                                        waistField.setText(vwaist);
                                    }

                                    if (!vstomach.equals("")) {
                                        stomachField.setText(vstomach);
                                    }

                                    if (!vhip.equals("")) {
                                        hipsField.setText(vhip);
                                    }

                                    if (!vfoots.equals("")) {
                                        footField.setText(vfoots);
                                    }

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                savingView.setVisibility(View.INVISIBLE);
            }
        });

    }

    //PlayerRefresh
    private void updateViewData(final Video video, int completed) {

        Log.d("Measure", "Refreshing view");

        if (completed == 1) {

            showCompletedSnackBar("Вы уже отправили замеры на этот день");

            /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String message = "Вы уже отправили замеры на этот день";

            builder.setTitle("Внимание").setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog alert = builder.create();
            if (!isFinishing()) {
                alert.show();
            }*/
        }

        if (video.isImage) {
            Picasso.get().load(video.getUrl()).into(imgView);
            imgView.setVisibility(View.VISIBLE);
            playerFrame.setVisibility(View.INVISIBLE);

        } else {

            url = video.getUrl();
            checkStoragePermission();

            imgView.setVisibility(View.INVISIBLE);
            playerFrame.setVisibility(View.VISIBLE);

            Picasso.get().load(video.getThumbs()).into(thumbs);
            Log.d("Measure", "thumbs url:" + video.getThumbs());

            if (TextUtils.isEmpty(video.getUrl())) {
                video.setUrl("is empty url...");
            }

            thumbs.setVisibility(View.VISIBLE);

            downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    url = video.getUrl();
                    checkStoragePermission();
                }
            });

        }

    }

    @Override
    public void onChanged(Download data, @NotNull Reason reason) {
        updateViews(data, reason);
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {
            enqueueDownload();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enqueueDownload();
        } else {
            Snackbar.make(mainView, R.string.permission_not_enabled, Snackbar.LENGTH_LONG).show();
        }
    }

    private void enqueueDownload() {
        final String url = this.url;//Data.sampleUrls[0];
        final String filePath = Data.getSaveDir() + "/movies/" + Data.getNameFromUrl(url);
        request = new Request(url, filePath);
        //request.setExtras(getExtrasForRequest(request));

        fetch.attachFetchObserversForDownload(request.getId(), this)
                .enqueue(request, new Func<Request>() {
                    @Override
                    public void call(@NotNull Request result) {
                        request = result;
                    }
                }, new Func<Error>() {
                    @Override
                    public void call(@NotNull Error result) {
                        Timber.d("SingleDownloadActivity Error: %1$s", result.toString());
                    }
                });
    }

    private void updateViews(@NotNull Download download, Reason reason) {
        if (request.getId() == download.getId()) {
            if (reason == Reason.DOWNLOAD_QUEUED || reason == Reason.DOWNLOAD_COMPLETED) {
                setTitleView(download.getFile());
            }
            setProgressView(download.getStatus(), download.getProgress());
            //etaTextView.setText(Utils.getETAString(this, download.getEtaInMilliSeconds()));
            //downloadSpeedTextView.setText(Utils.getDownloadSpeedString(this, download.getDownloadedBytesPerSecond()));
            if (download.getError() != Error.NONE) {
                showDownloadErrorSnackBar(download.getError());
            }
        }
    }

    private void setTitleView(@NonNull final String fileName) {
        final Uri uri = Uri.parse(fileName);
        //titleTextView.setText(uri.getLastPathSegment());
    }

    private void setProgressView(@NonNull final Status status, final int progress) {

        final String progressString = getResources().getString(R.string.percent_progress, progress);

        switch (status) {
            case QUEUED: {

                downloadButton.setBackgroundResource(R.drawable.undone);
                thumbs.setVisibility(View.VISIBLE);
                playerView.setVisibility(View.INVISIBLE);

                break;
            }
            case ADDED: {
                downloadButton.setBackgroundResource(R.drawable.download);

                thumbs.setVisibility(View.VISIBLE);
                progressView.setText(R.string.added);
                downloadButton.setVisibility(View.VISIBLE);
                break;
            }
            case DOWNLOADING:
                thumbs.setVisibility(View.VISIBLE);
                downloadButton.setVisibility(View.INVISIBLE);

                downloadButton.setBackgroundResource(R.drawable.undone);

                progressView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                progressBar.setProgress(progress);
                progressView.setText(progressString);

                thumbs.setVisibility(View.VISIBLE);
                playerView.setVisibility(View.INVISIBLE);

                break;
            case COMPLETED: {
                downloadButton.setVisibility(View.INVISIBLE);
                //holder.downloadButton.setText(R.string.delete);

                thumbs.setVisibility(View.INVISIBLE);
                playerView.setVisibility(View.VISIBLE);

                progressView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);

                //File file = new File(filePath, fileName);
                //preparePlayer(Uri.parse("file:///" + filePath + fileName));
                preparePlayer(Uri.parse("file:///" + Data.getSaveDir() + "/movies/" + Data.getNameFromUrl(url)));

                break;
            }
            default: {
                progressView.setText(R.string.status_unknown);
                break;
            }
        }
    }

    private void showDownloadErrorSnackBar(@NotNull Error error) {
        final Snackbar snackbar = Snackbar.make(mainView, "Download Failed: ErrorCode: " + error, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, v -> {
            fetch.retry(request.getId());
            snackbar.dismiss();
        });
        snackbar.show();
    }

    private void showCompletedSnackBar(@NotNull String text) {
        final Snackbar snackbar = Snackbar.make(mainView, text, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
        });
        snackbar.show();
    }

    private void hideSaveBtn() {
        if (completed == 1 || accessLevel < 2) {
            saveBtn.setVisibility(View.GONE);

            heightField.setEnabled(false);
            weightField.setEnabled(false);
            wantWeightField.setEnabled(false);
            breastField.setEnabled(false);
            waistField.setEnabled(false);
            stomachField.setEnabled(false);
            hipsField.setEnabled(false);
            footField.setEnabled(false);

            frontImgBtn.setVisibility(View.GONE);
            sideImgBtn.setVisibility(View.GONE);
            backImgBtn.setVisibility(View.GONE);

        } else {
            saveBtn.setVisibility(View.VISIBLE);
        }
    }

    public void firstPhotoTapped(View v) {
        tappedImgIndex = 0;
        showImagePickerDialog((ImageButton)v, 0);
    }

    public void secondPhotoTapped(View v) {
        tappedImgIndex = 1;
        showImagePickerDialog((ImageButton)v, 1);
    }

    public void thirdPhotoTapped(View v) {
        tappedImgIndex = 2;
        showImagePickerDialog((ImageButton)v, 2);
    }

    private void showImagePickerDialog(ImageButton sender, int tappedIndex) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String msg = "";

        switch (tappedIndex) {
            case 0:
                msg = "Фотография спереди";
                break;
            case 1:
                msg = "Фотография сбоку";
                break;
            case 2:
                msg = "Фотография сзади";
                break;
            default:
                break;
        }

        builder.setTitle(msg).setMessage("Сделать новую фотографию с помощью камеры или выбрать существующее изображение из гелереи? Фотография может быть без отображения лица (ниже уровня подбородка)")
                .setCancelable(true)
                .setPositiveButton("Выбрать из галереи", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, RESULT_LOAD_IMAGE);

                    }
                })
                .setNegativeButton("Сделать фотографию", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                        startActivityForResult(intent, CAMERA);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void saveTapped(View v) {
        Button sender = (Button)v;
        sender.setEnabled(false);

        if (!checkForErrors()) {
            runAddMeasure();
        } else {
            sender.setEnabled(true);
        }
    }

    private boolean checkForErrors() {

        boolean errors = false;
        String title = "Ошибка";
        String message = "Укажите Ваш ";

        if (heightField.length() == 0) {
            errors = true;
            message += "рост";

            alertWithTitle(title, message, this, true);

        } else if (weightField.length() == 0) {
            errors = true;
            message += "вес";

            alertWithTitle(title, message, this, true);

        } else if (wantWeightField.length() == 0) {
            errors = true;
            message += "желаемый вес";

            alertWithTitle(title, message, this, true);

        } else if (breastField.length() == 0) {
            errors = true;
            message += "обхват груди";

            alertWithTitle(title, message, this, true);

        } else if (waistField.length() == 0) {
            errors = true;
            message += "обхват пояса";

            alertWithTitle(title, message, this, true);

        } else if (stomachField.length() == 0) {
            errors = true;
            message += "обхват живота";

            alertWithTitle(title, message, this, true);

        } else if (hipsField.length() == 0) {
            errors = true;
            message += "обхват бедер";

            alertWithTitle(title, message, this, true);

        } else if (footField.length() == 0) {
            errors = true;
            message += "обхват ног";

            alertWithTitle(title, message, this, true);

        }

        if (frontImgData == null) {

            errors = true;
            message += "Добавьте фото спереди. Фотографироваться можно без лица, охватив зону ниже подбородка";

            alertWithTitle(title, message, this, false);

        } else if (sideImgData == null) {

            errors = true;
            message += "Добавьте фото сбоку. Фотографироваться можно без лица, охватив зону ниже подбородка";

            alertWithTitle(title, message, this, false);

        } else if (backImgData == null) {

            errors = true;
            message += "Добавьте фото сзади. Фотографироваться можно без лица, охватив зону ниже подбородка";

            alertWithTitle(title, message, this, false);

        }

        return errors;
    }

    private void alertWithTitle(String title, String message, Context ctx, final boolean needToFocus) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(title).setMessage(message)
                .setCancelable(true)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //if (needToFocus) {
                            //toFocus.setFocusableInTouchMode(true);
                            //toFocus.requestFocus();
                        //} else {
                            paramsScrollView.fullScroll(ScrollView.FOCUS_UP);
                        //}

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void runAddMeasure() {

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String token = mSettings.getString("token", "");
        String height = heightField.getText().toString();
        String weight = weightField.getText().toString();
        String want_weight = wantWeightField.getText().toString();
        String breast = breastField.getText().toString();
        String waist = waistField.getText().toString();
        String stomach = stomachField.getText().toString();
        String hip = hipsField.getText().toString();
        String foots = footField.getText().toString();

        //savingView.setVisibility(View.VISIBLE);

        addMeasure(token, height, weight, want_weight, breast, waist, stomach, hip, foots);
    }

    private void addMeasure(final String token, final String height, final String weight, final String want_weight, final String breast, final String waist, final String stomach, final String hip, final String foots) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.setForceMultipartEntityContentType(true);
        try {
            params.put("photo1", frontImgData, "images/jpeg");
            params.put("photo2", sideImgData, "images/jpeg");
            params.put("photo3", backImgData, "images/jpeg");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/addmeasurephotos", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                savingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {

                    savingView.setVisibility(View.INVISIBLE);

                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            Log.d("Measure", "photos saved");

                            addMeasureData(token, height, weight, want_weight, breast, waist, stomach, hip, foots);

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                savingView.setVisibility(View.INVISIBLE);
                addMeasureData(token, height, weight, want_weight, breast, waist, stomach, hip, foots);
            }
        });

    }

    private void addMeasureData(final String token, String height, String weight, String want_weight, String breast, String waist, String stomach, String hip, String foots) {

        AsyncHttpClient client = new AsyncHttpClient();

        JSONObject data = new JSONObject();
        JSONObject dataparams = new JSONObject();
        StringEntity entity = new StringEntity("", "");
        try {
            dataparams.put("height", height);
            dataparams.put("weight", weight);
            dataparams.put("want_weight", want_weight);
            dataparams.put("breast", breast);
            dataparams.put("waist", waist);
            dataparams.put("stomach", stomach);
            dataparams.put("hip", hip);
            dataparams.put("foots", foots);

            data.put("data", dataparams);

            entity = new StringEntity(data.toString(), "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        client.addHeader("Authorization","Bearer "+token);
        client.post(getApplicationContext(),"http://bodyline14.ru/mobile/addmeasure", entity, "application/json; charset=utf-8", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            Log.d("Measure", "data saved");
                            complete(token);

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void complete(String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("event_type", "4");
        params.put("event_reason", "0");
        params.put("day", day);
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/complete", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            //savingView.setVisibility(View.INVISIBLE);

                            AlertDialog.Builder builder = new AlertDialog.Builder(Measure.this);
                            String message = "Ваши замеры успешно сохранены! Ваша суточная норма  калорий будет пересчитана";

                            builder.setTitle("Успешно").setMessage(message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Measure.this.finish();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = new String[]{MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap d = BitmapFactory.decodeFile(picturePath);

            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
            //imagePath = picturePath;

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //4
            File file = new File(getFilesDir() + "/zamer"+tappedImgIndex+".jpg");
            try {
                file.createNewFile();
                FileOutputStream fo = new FileOutputStream(file);
                //5
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            switch (tappedImgIndex) {
                case 0:
                    frontImgView.setImageBitmap(scaled);
                    frontImgData = file;//new File(picturePath);
                    break;
                case 1:
                    sideImgView.setImageBitmap(scaled);
                    sideImgData = file;//new File(picturePath);
                    break;
                case 2:
                    backImgView.setImageBitmap(scaled);
                    backImgData = file;//new File(picturePath);
                    break;
                default:
                    break;
            }

        } else if (requestCode == CAMERA && resultCode == RESULT_OK && null !=data) {

            Bitmap d = (Bitmap) data.getExtras().get("data");

            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //4
            File file = new File(getFilesDir() + "/zamer"+tappedImgIndex+".jpg");
            try {
                file.createNewFile();
                FileOutputStream fo = new FileOutputStream(file);
                //5
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            switch (tappedImgIndex) {
                case 0:
                    frontImgView.setImageBitmap(scaled);
                    frontImgData = file;
                    break;
                case 1:
                    sideImgView.setImageBitmap(scaled);
                    sideImgData = file;
                    break;
                case 2:
                    backImgView.setImageBitmap(scaled);
                    backImgData = file;
                    break;
                default:
                    break;
            }

        }

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    private void showWeightDialog(Button btn) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_weight, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView kgPicker = dialogView.findViewById(R.id.kg);
        final NumberPickerView grPicker = dialogView.findViewById(R.id.gr);

        setData(kgPicker, kgValues);
        setData(grPicker, grValues);

        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHING

                String kg = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                btn.setText(kg);

                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void showLengthDialog(Button btn) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_lenght, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView cmPicker = dialogView.findViewById(R.id.cm);
        final NumberPickerView mmPicker = dialogView.findViewById(R.id.mm);

        setData(cmPicker, cmValues);
        setData(mmPicker, mmValues);

        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS

                String cm = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                btn.setText(cm);

                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void setData(NumberPickerView picker, ArrayList<String> valuesList){
        String[] values = new String[valuesList.size()];

        for (int i = 0; i < values.length; i++) {
            String str = valuesList.get(i);
            values[i] = str;
        }

        picker.setDisplayedValues(values);
        picker.setMinValue(0);
        picker.setMaxValue(values.length - 1);
        picker.setValue(0);
    }

}
