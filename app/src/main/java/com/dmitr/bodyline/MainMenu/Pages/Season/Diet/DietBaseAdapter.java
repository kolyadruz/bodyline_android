package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;

import de.halfbit.pinnedsection.PinnedSectionListView;

public class DietBaseAdapter extends BaseAdapter implements PinnedSectionListView.PinnedSectionListAdapter {

    private static final int[] COLORS = new int[] {
            R.color.green_light, R.color.orange_light,
            R.color.blue_light, R.color.red_light };

    List<DietItem> dietItemsArray = new ArrayList<>();

    Context context;

    int layoutSection;
    int layoutFood;
    int layoutDish;

    boolean isSection = false;
    boolean isFirstElement = false;
    boolean isLastElement = false;
    boolean isSingleElement = false;

    private LayoutInflater inflater;

    public DietBaseAdapter(Context context, int layoutSection, int layoutFood, int layoutDish, List<DietItem> dietItemsArray) {

        this.context = context;

        this.dietItemsArray = dietItemsArray;

        this.layoutSection = layoutSection;
        this.layoutFood = layoutFood;
        this.layoutDish = layoutDish;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dietItemsArray.size();
    }

    @Override
    public DietItem getItem(int position) {
        return dietItemsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return dietItemsArray.get(position).id;
    }

    @Override
    public View getView(int position, View editingCell, ViewGroup parent) {

        DietItem currentItem = dietItemsArray.get(position);

        View cellView = editingCell;

        RelativeLayout divider;

        switch (currentItem.type) {
            case DietItem.SECTION:

                isSection = true;

                //cellView = inflater.inflate(layoutSection, null);

                //TextView sectionNameTxt = (TextView)cellView.findViewById(android.R.id.text1);

                cellView = inflater.inflate(R.layout.cell_diet_section, null);
                TextView sectionNameTxt = (TextView)cellView.findViewById(R.id.section_txt);

                sectionNameTxt.setText(currentItem.name);

                //cellView.setBackgroundColor(parent.getResources().getColor(COLORS[currentItem.sectionPosition % COLORS.length]));

                divider = null;

                //cellView.setBackgroundResource(R.drawable.cell_diet_bg);

                break;

            case DietItem.FOOD:

                if (isSection) {
                    isSection = false;
                    isFirstElement = true;
                }

                cellView = inflater.inflate(layoutFood, null);
                divider = (RelativeLayout) cellView.findViewById(R.id.divider);
                divider.setVisibility(View.VISIBLE);

                TextView foodNameTxt = (TextView)cellView.findViewById(R.id.nameLabel);
                TextView foodCountTxt = (TextView)cellView.findViewById(R.id.countLabel);
                TextView foodMeasureTxt = (TextView)cellView.findViewById(R.id.measureLabel);
                TextView foodCaloriesTxt = (TextView)cellView.findViewById(R.id.kcalLabel);

                foodNameTxt.setText(currentItem.name);
                foodCountTxt.setText(currentItem.count);

                switch (currentItem.measure) {
                    case 1:
                        foodMeasureTxt.setText("гр");
                        break;
                    case 2:
                        foodMeasureTxt.setText("шт");
                        break;
                    default:
                        break;
                }

                foodCaloriesTxt.setText(currentItem.calories + " ккал");

                break;
            case DietItem.DISH:

                if (isSection) {
                    isSection = false;
                    isFirstElement = true;
                }

                cellView = inflater.inflate(layoutDish, null);
                divider = (RelativeLayout) cellView.findViewById(R.id.divider);
                divider.setVisibility(View.VISIBLE);

                TextView dishNameTxt = (TextView)cellView.findViewById(R.id.nameLabel);
                TextView dishDescriptionTxt = (TextView)cellView.findViewById(R.id.descriptionLabel);
                TextView dishPortionTxt = (TextView)cellView.findViewById(R.id.portionLabel);
                TextView dishCaloriesTxt = (TextView)cellView.findViewById(R.id.kcalLabel);

                dishNameTxt.setText(currentItem.name);
                dishDescriptionTxt.setText(currentItem.description);
                dishPortionTxt.setText(currentItem.portion + "гр");
                dishCaloriesTxt.setText(currentItem.calories + " ккал");

                break;
            default:
                divider = null;
                break;
        }

        if (position != (dietItemsArray.size() - 1)) {

            DietItem nextItem = dietItemsArray.get(position + 1);
            if (nextItem.type == DietItem.SECTION) {
                if (!isFirstElement) {
                    isLastElement = true;
                } else {
                    isSingleElement = true;
                }
            }

        } else {

            if (!isFirstElement) {
                isLastElement = true;
            } else {
                isSingleElement = true;
            }

        }

        if (isFirstElement) {

            isFirstElement = false;
            cellView.setBackgroundResource(R.drawable.cell_diet_bg_top_rounded);

        }

        if (isLastElement) {

            isLastElement = false;
            cellView.setBackgroundResource(R.drawable.cell_diet_bg_bottom_rounded);
            divider.setVisibility(View.INVISIBLE);

        }

        if (isSingleElement) {

            isSingleElement = false;
            cellView.setBackgroundResource(R.drawable.cell_diet_bg_rounded);
            divider.setVisibility(View.INVISIBLE);

        }

        return cellView;
    }

    protected void prepareSections(int sectionsNumber) { }

    protected void onSectionAdded(DietItem section, int sectionPosition) { }

    @Override public int getViewTypeCount() {
        return 3;
    }

    @Override public int getItemViewType(int position) {
        return getItem(position).type;
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == DietItem.SECTION;
    }
}
