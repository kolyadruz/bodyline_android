package com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm;

public class Alarm {

    public String description;
    public String time;
    public int checked;

    public Alarm(String description, String time, Integer checked) {

        this.description = description;
        this.time = time;
        this.checked = checked;

    }


}
