package com.dmitr.bodyline.MainMenu.Pages.Season.Event;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends BaseAdapter {

    private Activity activity;

    List<Event> events = new ArrayList<Event>();

    int accessLevel = 0;

    private int layoutID;
    private LayoutInflater inflater=null;

    //public event_id;

    public EventAdapter(Activity activity, List<Event> events, int accessLevel, int layoutID) {
        this.activity = activity;

        this.events = events;
        this.accessLevel = accessLevel;

        this.layoutID = layoutID;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {

        return events.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        EventsListCell eventsListCell;
        if(editingCell == null) {

            if (events.size() < 3) {
                cellView = inflater.inflate(R.layout.cell_event_big, null);
            } else {
                cellView = inflater.inflate(layoutID, null);
            }
            eventsListCell = new EventsListCell(cellView);
            cellView.setTag(eventsListCell);
        } else {
            eventsListCell = (EventsListCell) cellView.getTag();
        }


        Event event = this.events.get(position);

        switch (event.type) {
            case 1:
                //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_task_icon_bg);
                eventsListCell.eventIcon.setImageResource(R.drawable.ic_event_note);
                eventsListCell.typeStr.setText("Задания");
                eventsListCell.eventDescriptionText.setText("указания, советы");
                break;
            case 2:
                //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_food_icon_bg);
                eventsListCell.eventIcon.setImageResource(R.drawable.ic_restaurant);
                eventsListCell.typeStr.setText("Питание");
                eventsListCell.eventDescriptionText.setText("индивидуальный план");
                break;
            case 3:
                //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_training_icon_bg);
                eventsListCell.eventIcon.setImageResource(R.drawable.ic_fitness_center);
                eventsListCell.typeStr.setText("Тренировка");
                eventsListCell.eventDescriptionText.setText("комплекс упражнений");
                break;
            case 4:
                //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_measure_icon_bg);
                eventsListCell.eventIcon.setImageResource(R.drawable.ic_timeline);
                eventsListCell.typeStr.setText("Замеры");
                eventsListCell.eventDescriptionText.setText("еженедельные замеры");
                break;
            case 5:
                //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_measure_icon_bg);
                eventsListCell.eventIcon.setImageResource(R.drawable.ic_timeline);
                eventsListCell.typeStr.setText("Вес");
                eventsListCell.eventDescriptionText.setText("ежедневные замеры веса");
                break;
            default:
                break;
        }

        if (accessLevel == 0 && event.type != 2) {
            //eventsListCell.eventIconBg.setBackgroundResource(R.drawable.event_disabled_icon_bg);
            //eventsListCell.typeStr.setTextColor(R.drawable.event_disabled_icon_bg);
            eventsListCell.typeStr.setTextColor(activity.getResources().getColor(R.color.numdaytxt_gray));
        }

        /*if (position == 0) {
            eventsListCell.topLine.setVisibility(View.INVISIBLE);
            eventsListCell.bottomLine.setVisibility(View.VISIBLE);
        } else if (position == events.size()-1) {
            eventsListCell.topLine.setVisibility(View.VISIBLE);
            eventsListCell.bottomLine.setVisibility(View.INVISIBLE);
        } else {
            eventsListCell.topLine.setVisibility(View.VISIBLE);
            eventsListCell.bottomLine.setVisibility(View.VISIBLE);
        }*/
        //eventsListCell.primTxt.setText(""+event.prim);

        return cellView;
    }

    class EventsListCell {
        //public RelativeLayout eventIconBg;
        public ImageView eventIcon;
        //public TextView topLine;
        //public TextView bottomLine;
        public TextView typeStr;
        public TextView eventDescriptionText;

        //public TextView primTxt;
        //public TextView timerDescritionTxt;
        //public TextView timerTxt;

        public EventsListCell(View base) {
            //eventIconBg = (RelativeLayout) base.findViewById(R.id.eventIconBg);
            eventIcon = (ImageView) base.findViewById(R.id.eventIcon);
            //topLine = (TextView) base.findViewById(R.id.topLine);
            //bottomLine = (TextView) base.findViewById(R.id.bottomLine);
            typeStr = (TextView) base.findViewById(R.id.typeStr);
            eventDescriptionText = (TextView) base.findViewById(R.id.eventDescriptionText);

            //primTxt = (TextView) base.findViewById(R.id.primTxt);
            //timerDescritionTxt = (TextView) base.findViewById(R.id.timerDescriptionTxt);
            //timerTxt = (TextView) base.findViewById(R.id.timerTxt);
        }
    }

}