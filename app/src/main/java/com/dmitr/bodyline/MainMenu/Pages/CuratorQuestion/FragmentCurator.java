package com.dmitr.bodyline.MainMenu.Pages.CuratorQuestion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.fabactivity.CuratorQuestion;
import com.dmitr.bodyline.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentCurator.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentCurator#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCurator extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int page;
    private String title;

    private OnFragmentInteractionListener mListener;

    String token;
    String s_token;

    FloatingActionButton fab;

    CuratorAdapter curatorAdapter;

    List<Curator> curators = new ArrayList<Curator>();

    TextView noQuestionTxt;

    Activity activity;

    public FragmentCurator() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentCurator newInstance(int page, String title) {
        FragmentCurator fragment = new FragmentCurator();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, page);
        args.putString(ARG_PARAM2, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            page = getArguments().getInt(ARG_PARAM1);
            title = getArguments().getString(ARG_PARAM2);
        }

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        token = mSettings.getString("token", "");
        s_token = mSettings.getString("s_token", "");

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_curator, container, false);

        activity = getActivity();

        fab = (FloatingActionButton) rootView.findViewById(R.id.fabques);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), CuratorQuestion.class);
                intent.putExtra("s_token", s_token);
                startActivity(intent);
            }
        });

        ListView cellQuesAnsListView =(ListView) rootView.findViewById(R.id.curatorListView);
        curatorAdapter = new CuratorAdapter(getActivity(),curators,R.layout.cell_curator);
        cellQuesAnsListView.setAdapter(curatorAdapter);

        noQuestionTxt = rootView.findViewById(R.id.noQuestionTxt);

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        curators.clear();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/questionsteacher", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("json", "" + json);

                        Integer err_id = json.getInt("err_id");
                        String rowData = json.getString("data");
                        JSONArray data = new JSONArray(rowData);
                        if (err_id == 0) {
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject dataObject = data.getJSONObject(i);
                                try {

                                    String question = dataObject.getString("text_question");
                                    String date_question = dataObject.getString("date_question");

                                    String answer = dataObject.getString("text_answer");
                                    String answerDate = dataObject.getString("date_answer");

                                    Log.d("curatorques", "question: " + question + " date_question: " + date_question + "answer: " + answer + " answerDate: " + answerDate);


                                    //с ответом
                                    //isAnswered = true

                                    Curator cur = new Curator(question, date_question, answer, answerDate, true);

                                    curators.add(cur);

                                } catch (JSONException ex) {

                                    String question = dataObject.getString("text_question");
                                    String date_question = dataObject.getString("date_question");

                                    Log.d("curatorques", "question: " + question + " date_question: " + date_question);


                                    // без ответа
                                    //isAnswered = false
                                    Curator cur = new Curator(question, date_question, "", "", false);
                                    curators.add(cur);

                                    ex.printStackTrace();

                                }

                                if (curators.size() > 0) {
                                    noQuestionTxt.setVisibility(View.INVISIBLE);
                                } else {
                                    noQuestionTxt.setVisibility(View.VISIBLE);
                                }

                                curatorAdapter.notifyDataSetChanged();

                            /*String question = dataObject.getString("text_question");
                            String date_question = dataObject.getString("date_question");
                            Log.d("jsonObj #"+i, "question: "+question +" date_question: "+date_question);*/

                            }
                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("CuratorQuestion:", "ошибка "+err_id);
                    }
                });
        AlertDialog alert = builder.create();
        if(!activity.isFinishing()) {
            alert.show();
        }
    }
}
