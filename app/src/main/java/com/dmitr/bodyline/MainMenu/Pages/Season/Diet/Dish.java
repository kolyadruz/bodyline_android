package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

public class Dish {

    public String name;
    public Integer id;
    public String calories;
    public String portion;
    public String description;

    public Dish(String name, int id, String calories, String portion, String description) {

        this.name = name;
        this.id = id;
        this.calories = calories;
        this.portion = portion;
        this.description = description;

    }

}
