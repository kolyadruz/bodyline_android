package com.dmitr.bodyline.MainMenu.Pages.Store;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.MainMenu.Store.SSLTolerentWebViewClient;
import com.dmitr.bodyline.R;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.util.EncodingUtils;

public class StoreWebView extends AppCompatActivity {

    WebView webView;
    String uri;
    JSONObject data;

    String postData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storewebview);

        Activity activity = StoreWebView.this;

        uri = getIntent().getStringExtra("url");
        String datastr = getIntent().getStringExtra("data");
        try {

            data = new JSONObject(datastr);

            postData = "Mrchlogin=" + data.getString("MrchLogin")
                    +"&OutSum="+ data.getString("OutSum")
                    +"&InvId="+ data.getString("InvId")
                    +"&Desc="+ data.getString("Desc")
                    +"&SignatureValue="+ data.getString("SignatureValue")
                    +"&Shp_item="+ data.getString("Shp_item")
                    +"&IncCurrLabel="+ data.getString("IncCurrLabel")
                    +"&Culture="+data.getString("Culture");

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.parseColor("#328ec9"));
        webView.setWebViewClient(
                new SSLTolerentWebViewClient(this)
        );
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setSupportZoom(false);
        //webView.getSettings().setLoadWithOverviewMode(true);
        //webView.getSettings().setUseWideViewPort(true);
        //Uri data = getIntent().getData();
        //webView.loadUrl("http://www.ya.ru");

        Intent intent = getIntent();
        uri = intent.getStringExtra("url");
        Log.d("URL is", uri);


        //webView.postUrl(uri, postData.getBytes());

        Log.d("StoreWebView", postData);

        webView.postUrl(uri, EncodingUtils.getBytes(postData,"BASE64"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        //webView.loadUrl(uri);

        //webView.postUrl(uri, postData.getBytes());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return true;
    }

}