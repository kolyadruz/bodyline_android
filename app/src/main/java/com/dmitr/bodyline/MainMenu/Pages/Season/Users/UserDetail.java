package com.dmitr.bodyline.MainMenu.Pages.Season.Users;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserDetail extends AppCompatActivity {

    int id;

    CircleImageView ava;
    TextView nameTxt;
    TextView info;

    LineChart graphView;

    List<JSONObject> userData = new ArrayList<JSONObject>();
    Double userStartWeight = 0.0;
    List<JSONObject> myData = new ArrayList<JSONObject>();
    Double meStartWeight = 0.0;

    String token;
    String s_token;

    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        ava = (CircleImageView) findViewById(R.id.account_ava);
        nameTxt = (TextView) findViewById(R.id.account_name);
        info = (TextView) findViewById(R.id.account_info);

        id = getIntent().getIntExtra("id", 0);

        String avaurl = getIntent().getStringExtra("avatar");
        Picasso.get() //
                .load(avaurl) //
                .placeholder(R.drawable.logowhsmall) //
                .error(R.drawable.ic_action_alarm) //
                .fit().centerCrop()//
                .tag(this) //
                .into(ava);

        name = getIntent().getStringExtra("name");
        nameTxt.setText(name);
        info.setText(getIntent().getStringExtra("info"));

        s_token = getIntent().getStringExtra("s_token");
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        graphView = (LineChart)findViewById(R.id.linechart);

        graphView.getLegend().setTextColor(ColorTemplate.rgb("#FFFFFF"));
        graphView.getDescription().setTextColor(ColorTemplate.rgb("#FFFFFF"));

        graphView.setTouchEnabled(true);
        graphView.setDragEnabled(true);
        graphView.setScaleEnabled(true);
        graphView.setPinchZoom(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        runGetSeasonUserData();
    }

    private void runGetSeasonUserData() {

        getSeasonUserData(token, s_token, id);

    }


    private void getSeasonUserData(String token, String s_token, int id) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("user_id", id);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getseasonuserdata", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("UserDetail", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                userData.clear();

                                JSONArray weightArray = json.getJSONArray("weight");

                                for (int i = 0; i < weightArray.length(); i++) {

                                    JSONObject weightData = weightArray.getJSONObject(i);

                                    userData.add(weightData);

                                }

                                runGetSeasonWeight();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void runGetSeasonWeight() {
        getSeasonWeight(token, s_token);
    }

    private void getSeasonWeight(String token, String s_token) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getseasonweight", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("UserDetail", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                myData.clear();

                                JSONArray weightArray = json.getJSONArray("data");

                                for (int i = 0; i < weightArray.length(); i++) {

                                    JSONObject weightData = weightArray.getJSONObject(i);

                                    myData.add(weightData);

                                }

                                lineChartUpdate();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void lineChartUpdate() {

        ArrayList<Entry> userValues = new ArrayList<Entry>();
        ArrayList<Entry> myValues = new ArrayList<Entry>();

        for (int i = 0; i < userData.size(); i++) {

            try {

                if (userStartWeight == 0.0) {
                    userStartWeight = userData.get(i).getDouble("weight");
                }

                int day = userData.get(i).getInt("day");
                double weight = userData.get(i).getDouble("weight");

                Entry charData;

                if (userStartWeight != 0.0) {
                    Double percent = 0.0 - (userStartWeight - weight) / userStartWeight * 100;
                    charData = new Entry(day, new Float(percent));
                } else {
                    charData = new Entry(day, new Float(weight));
                }

                userValues.add(charData);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        for (int i = 0; i < myData.size(); i++) {

            try {

                if (meStartWeight == 0.0) {
                    meStartWeight = myData.get(i).getDouble("weight");
                }

                int day = myData.get(i).getInt("day");
                double weight = myData.get(i).getDouble("weight");

                Entry charData;

                if (meStartWeight != 0.0) {
                    Double percent = 0.0 - (meStartWeight - weight) / meStartWeight * 100;
                    charData = new Entry(day, new Float(percent));
                } else {
                    charData = new Entry(day, new Float(weight));
                }
                myValues.add(charData);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        LineDataSet userDataSet = new LineDataSet(userValues, name);
        userDataSet.setColors(ColorTemplate.rgb("#9382E8"));
        userDataSet.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
        userDataSet.setDrawCircles(false);
        userDataSet.setDrawValues(false);

        LineDataSet myDataSet = new LineDataSet(myValues, "Мой вес");
        myDataSet.setColors(ColorTemplate.rgb("#61B0FF"));
        myDataSet.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
        myDataSet.setDrawCircles(false);
        myDataSet.setDrawValues(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(userDataSet);
        dataSets.add(myDataSet);

        LineData data = new LineData(dataSets);

        graphView.setData(data);
        graphView.getDescription().setText("График потерь веса в процентах");

        //graphView.getData().notifyDataChanged();
        graphView.notifyDataSetChanged();
        graphView.invalidate();
        //graphView.notifyAll();

    }

    private void showErrID(Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

}
