package com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAlarm.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAlarm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAlarm extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    ListView alarmsList;

    AlarmsAdapter adapter;

    List<Alarm> alarms = new ArrayList<Alarm>();

    public FragmentAlarm() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAlarm.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAlarm newInstance(String param1, String param2) {
        FragmentAlarm fragment = new FragmentAlarm();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root = inflater.inflate(R.layout.fragment_alarm, container, false);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        alarms.clear();

        Alarm alarm1 = new Alarm("Время подъема",
                mSettings.getString("alarm_wake", "В 00:00"),
                        mSettings.getInt("alarm_wake_checked", 0));
        alarms.add(alarm1);
        Alarm alarm2 = new Alarm("Напомнить о завтраке",
                mSettings.getString("alarm_breakfast", "В 00:00"),
                mSettings.getInt("alarm_breakfast_checked", 0));
        alarms.add(alarm2);
        Alarm alarm3 = new Alarm("Напомнить о первом перекусе",
                mSettings.getString("alarm_first_snack", "В 00:00"),
                mSettings.getInt("alarm_first_snack_checked", 0));
        alarms.add(alarm3);
        Alarm alarm4 = new Alarm("Напомнить об обеде",
                mSettings.getString("alarm_lunch", "В 00:00"),
                mSettings.getInt("alarm_lunch_checked", 0));
        alarms.add(alarm4);
        Alarm alarm5 = new Alarm("Напомнить о втором перекусе",
                mSettings.getString("alarm_second_snack", "В 00:00"),
                mSettings.getInt("alarm_second_snack_checked", 0));
        alarms.add(alarm5);
        Alarm alarm6 = new Alarm("Напомнить об ужине",
                mSettings.getString("alarm_dinner", "В 00:00"),
                mSettings.getInt("alarm_dinner_checked", 0));
        alarms.add(alarm6);
        Alarm alarm7 = new Alarm("Время тренировки",
                mSettings.getString("alarm_train", "В 00:00"),
                mSettings.getInt("alarm_train_checked", 0));
        alarms.add(alarm7);
        Alarm alarm8 = new Alarm("Время отбоя",
                mSettings.getString("alarm_sleep", "В 00:00"),
                mSettings.getInt("alarm_sleep_checked", 0));
        alarms.add(alarm8);

        alarmsList = (ListView) root.findViewById(R.id.alarmsList);
        adapter = new AlarmsAdapter(getActivity(), alarms, R.layout.cell_alarm);

        alarmsList.setAdapter(adapter);

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
