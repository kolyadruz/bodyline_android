package com.dmitr.bodyline.MainMenu.Pages.Chat;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapter extends BaseAdapter {

    Activity activity;

    int otherLayoutID;
    int selfLayoutID;
    int admLayoutID;

    List<Message> messages = new ArrayList<Message>();

    private LayoutInflater inflater=null;

    public ChatAdapter(Activity activity, List<Message> messages, int otherLayoutID, int selfLayoutID, int admLayoutID) {
        this.activity = activity;

        this.messages = messages;

        this.otherLayoutID = otherLayoutID;
        this.selfLayoutID = selfLayoutID;

        this.admLayoutID = admLayoutID;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View editingCell, ViewGroup parent) {

        Message msg = messages.get(position);

        View cellView = editingCell;
        ChatCell chatCell;

        /*if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            chatCell = new ChatCell(editingCell);
            cellView.setTag(chatCell);
        } else {
            chatCell = (ChatCell) cellView.getTag();
        }*/

        if (msg.isAdmin == 1) {
            cellView = inflater.inflate(admLayoutID, null);
        } else {
            if (msg.self) {
                cellView = inflater.inflate(selfLayoutID, null);
            } else {
                cellView = inflater.inflate(otherLayoutID, null);
            }
        }
        //chatCell = new ChatCell(editingCell);
        //cellView.setTag(chatCell);

        TextView nameLabel = (TextView) cellView.findViewById(R.id.nameLabel);
        TextView messageLabel = (TextView) cellView.findViewById(R.id.messageLabel);
        TextView dateLabel = (TextView) cellView.findViewById(R.id.dateLabel);

        nameLabel.setText(msg.name);
        messageLabel.setText(msg.message);
        dateLabel.setText(msg.date);

        //chatCell.nameLabel.setText(msg.name);
        //chatCell.messageLabel.setText(msg.message);

        return cellView;
    }

    class ChatCell {

        public TextView nameLabel;
        public TextView messageLabel;

        public TextView dateLabel;

        public ChatCell(View base) {

            nameLabel = (TextView) base.findViewById(R.id.nameLabel);
            messageLabel = (TextView) base.findViewById(R.id.messageLabel);

            dateLabel = (TextView) base.findViewById(R.id.dateLabel);

        }
    }
}
