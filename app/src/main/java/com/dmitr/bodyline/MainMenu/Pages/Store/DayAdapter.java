package com.dmitr.bodyline.MainMenu.Pages.Store;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DayAdapter extends BaseAdapter {

    private Activity activity;

    private List<Day> days;

    private int layoutID;
    private LayoutInflater inflater=null;

    private Context appCtx;

    boolean isRedirecting = false;

    public DayAdapter(Activity activity, List<Day> days, int layoutID) {
        this.activity = activity;
        this.days = days;
        this.layoutID = layoutID;

        this.appCtx = activity.getApplicationContext();

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return days.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        DaysListCell daysListCell;
        if(editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            daysListCell = new DaysListCell(cellView);
            cellView.setTag(daysListCell);
        } else {
            daysListCell = (DaysListCell) cellView.getTag();
        }

        final Day day = days.get(position);

        daysListCell.name.setText(day.name);
        daysListCell.description.setText(day.description);
        daysListCell.price.setText(day.price+" руб.");
        daysListCell.count_days.setText(day.count_days+" дн.");
        daysListCell.leftDaysTxt.setText("начало "+day.date);

        daysListCell.buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Day day = days.get(position);

                Log.d("DayAdapter", "id: "+ day.id + " st_id: " + day.st_id);

                if (!isRedirecting) {
                    isRedirecting = true;
                    purchase(day.st_id, day.id);
                }
            }
        });

        String image = day.image;

        if (image.isEmpty()) {
            Picasso.get() //
                    .load(R.drawable.logowhsmall) //
                    .error(R.drawable.logowhsmall) //
                    .fit()
                    .centerCrop()//
                    .tag(daysListCell) //
                    .into(daysListCell.productImage);
        } else {
            Picasso.get() //
                    .load(image) //
                    .placeholder(R.drawable.logowhsmall) //
                    .error(R.drawable.logowhsmall) //
                    .fit()
                    .centerCrop()//
                    .tag(daysListCell) //
                    .into(daysListCell.productImage);
        }
        return cellView;
    }

    class DaysListCell {

        public TextView name;
        public TextView count_days;
        public TextView price;
        public TextView description;
        public TextView leftDaysTxt;

        public Button buyBtn;

        public ImageView productImage;

        public DaysListCell(View base) {
            name = (TextView) base.findViewById(R.id.seasonName);
            count_days = (TextView) base.findViewById(R.id.daysTxt);
            price = (TextView) base.findViewById(R.id.cost);
            description = (TextView) base.findViewById(R.id.seasonDesc);
            leftDaysTxt = (TextView) base.findViewById(R.id.leftDaysTxt);

            buyBtn = (Button) base.findViewById(R.id.buyBtn);

            productImage = (ImageView) base.findViewById(R.id.productImage);
        }
    }

    private void purchase(int st_id, int product_id) {

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(appCtx);
        String token = mSettings.getString("token", "");

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        RequestParams params = new RequestParams();
        params.put("st_id", st_id);
        params.put("season_id", product_id);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/purchase", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200){
                    String str = new String(responseBody);
                    try {
                        final JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0){

                            Intent intent = new Intent(appCtx, StoreWebView.class);
                            intent.putExtra("url", json.getString("url"));
                            intent.putExtra("data", json.getString("params"));

                            //Log.d("DayAdapter", "data: " + json.getString("params"));

                            isRedirecting = false;

                            activity.startActivity(intent);

                        } else {
                           // showErrID(err_id);
                        }
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                String Message = "Интернет соединение отсутствует";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                AlertDialog alert = builder.create();
                if (!activity.isFinishing()) {
                    alert.show();
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }


}
