package com.dmitr.bodyline.MainMenu.Pages.Season.Diet;

public class DietItem {

    public static final int SECTION = 0;
    public static final int FOOD = 1;
    public static final int DISH = 2;

    public final int type;

    public final String name;
    public final int id;
    public final String calories;
    public final String count;
    public final int measure;
    public final String portion;
    public final String description;


    public int sectionPosition;
    public int listPosition;

    public DietItem(int type, String name) {
        this.type = type;
        this.name = name;

        this.id = 0;
        this.calories = "";
        this.count = "";
        this.measure = 0;
        this.portion = "";
        this.description = "";
    }

    public DietItem(int type, String name, int id, String calories, String count, int measure, String portion, String description) {
        this.type = type;
        this.name = name;
        this.id = id;
        this.calories = calories;
        this.count = count;
        this.measure = measure;
        this.portion = portion;
        this.description = description;
    }

    @Override public String toString() {
        return name;
    }

}
