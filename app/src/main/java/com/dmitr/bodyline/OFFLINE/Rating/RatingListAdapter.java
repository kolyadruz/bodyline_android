package com.dmitr.bodyline.OFFLINE.Rating;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dmitr.bodyline.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class RatingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;

    JSONArray users;

    public RatingListAdapter(Activity activity, JSONArray users) {

        this.activity = activity;
        this.users = users;

    }

    public static class RatingViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public RatingViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemRating = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating_user, parent, false);
        return new RatingViewHolder(itemRating);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CircleImageView avatar = holder.itemView.findViewById(R.id.circleImageView);
        TextView nameTxt =  holder.itemView.findViewById(R.id.nameTxt);

        ImageView arrow = holder.itemView.findViewById(R.id.arrowImg);
        TextView weightLossTxt = holder.itemView.findViewById(R.id.weightLossTxt);

        TextView placeTxt = holder.itemView.findViewById(R.id.placeTxt);

        try {
            JSONObject obj = users.getJSONObject(position);

            String imgUrl = obj.getString("avatar");

            placeTxt.setText(obj.getString("place"));

            Picasso.get() //
                    .load(imgUrl) //
                    .placeholder(R.drawable.logowhsmall) //
                    .error(R.drawable.ic_action_alarm) //
                    .fit().centerCrop() //
                    .tag(activity) //
                    .into(avatar);

            nameTxt.setText(obj.getString("name"));

            Integer weight = obj.getInt("weight");

            weightLossTxt.setText(weight + " КГ");

            if (weight < 0) {
                arrow.setImageResource(R.drawable.arrow_down);
                weightLossTxt.setTextColor(activity.getResources().getColor(R.color.red_light));
            } else {
                arrow.setImageResource(R.drawable.arrow_up);
                weightLossTxt.setTextColor(activity.getResources().getColor(R.color.time_green));
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (users != null) {
            return users.length();
        } else {
            return 0;
        }
    }

    public void addData(JSONArray users) {
        this.users = users;
        notifyDataSetChanged();
    }

}
