package com.dmitr.bodyline.OFFLINE.Fragments.Profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;
import info.hoang8f.android.segmented.SegmentedGroup;
import com.dmitr.bodyline.OFFLINE.Polling.PollingPage;
import com.dmitr.bodyline.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ProfileOfflineFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileOfflineFragment extends Fragment implements NumberPickerView.OnValueChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private static final int RESULT_LOAD_IMAGE = 1, CAMERA = 2;

    private Activity activity;
    private ConstraintLayout loadingView;

    private String token;

    //Гланое инфо
    private String imagePath;
    private CircleImageView circleView;

    private String fam;
    private String name;

    private TextView nameField;

    private ImageButton startWeight, targetWeight, kcal, currentWeight;

    private TextView startWeightTxt, targetWeightTxt, kcalTxt, currentWeightTxt;

    //ФОТО ЗАМЕРОВ
    private ImageButton frontImgBtn;
    private ImageButton sideImgBtn;
    private ImageButton backImgBtn;

    ImageView frontImgView;
    ImageView sideImgView;
    ImageView backImgView;

    ProgressBar frontLoadingView;
    ProgressBar sideLoadingView;
    ProgressBar backLoadingView;

    //ЗАМЕРЫ
    SegmentedGroup picker;
    RecyclerView recyclerView;
    MeasuresListAdapter measureAdapter;

    Integer canMeasure;
    Integer today;
    String groupTitle;
    JSONArray measures;

    JSONArray weightGraphData;

    LineChart weightGraph;

    List<Entry> entries2 = new ArrayList();

    //РЕЙТИНГ
    ImageButton placeCupBtn;

    Integer tappedIndex;

    //ДИАЛОГИ
    ArrayList<String> kgValues = new ArrayList<>();
    ArrayList<String> grValues = new ArrayList<>();

    ArrayList<String> cmValues = new ArrayList<>();
    ArrayList<String> mmValues = new ArrayList<>();

    public ProfileOfflineFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileOfflineFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileOfflineFragment newInstance(String param1, String param2) {
        ProfileOfflineFragment fragment = new ProfileOfflineFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        for (int i = 35; i < 301; i++) {
            kgValues.add(""+i);
        }

        for (int i = 0; i < 10; i++) {
            grValues.add(""+i);
        }

        for (int i = 5; i < 201; i++) {
            cmValues.add(""+i);
        }

        for (int i = 0; i < 9; i++) {
            mmValues.add(""+i);
        }

        activity = getActivity();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        token = mSettings.getString("token", "");

        fam = mSettings.getString("imya", "");
        name =  mSettings.getString("fam", "");

        imagePath = getActivity().getApplicationContext().getFilesDir() + "/avatar.jpg";

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_offline, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadingView = view.findViewById(R.id.loading_view);

        circleView = view.findViewById(R.id.circleImageView);
        if (imagePath != "") {
            circleView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }

        nameField = view.findViewById(R.id.textView5);
        nameField.setText(fam + " " + name);

        startWeightTxt = view.findViewById(R.id.startWeightTxt);
        targetWeightTxt = view.findViewById(R.id.targetWeightTxt);
        kcalTxt = view.findViewById(R.id.kcalTxt);
        currentWeightTxt = view.findViewById(R.id.currentWeightTxt);

        setupTopButtons(view);

        weightGraph = view.findViewById(R.id.linechart);
        weightGraph.setNoDataText("Нет данных замеров веса для отображения графика.");
        weightGraph.setNoDataTextColor(getResources().getColor(R.color.time_white));
        weightGraph.invalidate();

        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(llm);
        measureAdapter = new MeasuresListAdapter(activity, measures, canMeasure, token);
        recyclerView.setAdapter(measureAdapter);

        picker = view.findViewById(R.id.segment2);

        picker.check(R.id.hand);

        picker.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int resource_id) {

                Integer position = 0;

                switch (resource_id) {
                    case R.id.hand:
                        position = 0;
                        break;
                    case R.id.stomach:
                        position = 1;
                        break;
                    case R.id.hips:
                        position = 2;
                        break;
                    case R.id.foots:
                        position = 3;
                        break;
                    case R.id.weight:
                        position = 4;
                        break;
                }

                measureAdapter.segmentChanged(position);

            }
        });

        setupMeasureButtons(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        runGetProfile();
    }

    private void setupMeasureButtons(View view) {

        frontImgView = view.findViewById(R.id.frontImgView);
        sideImgView = view.findViewById(R.id.sideImgView);
        backImgView = view.findViewById(R.id.backImgView);

        frontLoadingView = view.findViewById(R.id.frontLoadingView);
        sideLoadingView = view.findViewById(R.id.sideLoadingView);
        backLoadingView = view.findViewById(R.id.backLoadingView);

        frontImgBtn = view.findViewById(R.id.frontImgBtn);
        frontImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tappedIndex = 0;
                showImagePickerDialog((ImageButton) v, 0);
            }
        });

        sideImgBtn = view.findViewById(R.id.sideImgBtn);
        sideImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tappedIndex = 1;
                showImagePickerDialog((ImageButton) v, 1);
            }
        });

        backImgBtn = view.findViewById(R.id.backImgBtn);
        backImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tappedIndex = 2;
                showImagePickerDialog((ImageButton) v, 2);
            }
        });
    }


    private void showImagePickerDialog(ImageButton sender, int tappedIndex) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        String msg = "";

        switch (tappedIndex) {
            case 0:
                msg = "Фотография спереди";
                break;
            case 1:
                msg = "Фотография сбоку";
                break;
            case 2:
                msg = "Фотография сзади";
                break;
            default:
                break;
        }

        builder.setTitle(msg).setMessage("Сделать новую фотографию с помощью камеры или выбрать существующее изображение из гелереи? Фотография может быть без отображения лица (ниже уровня подбородка)")
                .setCancelable(true)
                .setPositiveButton("Выбрать из галереи", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, RESULT_LOAD_IMAGE);

                    }
                })
                .setNegativeButton("Сделать фотографию", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                        startActivityForResult(intent, CAMERA);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap d = BitmapFactory.decodeResource(getResources(), R.drawable.logowhsmall);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == activity.RESULT_OK && null != data) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = new String[]{MediaStore.Images.Media.DATA};

            Cursor cursor = activity.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            d = BitmapFactory.decodeFile(picturePath);

        } else if (requestCode == CAMERA && resultCode == activity.RESULT_OK && null !=data) {

            d = (Bitmap) data.getExtras().get("data");

        }

        int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        //4
        File file = new File(activity.getFilesDir() + "/zamer"+tappedIndex+".jpg");
        try {
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            //5
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        uploadPhoto(tappedIndex, file);

    }

    private void uploadPhoto(Integer tappedIndex, File imgData) {

        String url = "";

        switch (tappedIndex) {
            case 0:
                url = "http://bodyline14.ru/api/1.0/offline/addmeasurephotofront";

                frontLoadingView.setVisibility(View.VISIBLE);
                frontImgBtn.setVisibility(View.INVISIBLE);

                break;
            case 1:
                url = "http://bodyline14.ru/api/1.0/offline/addmeasurephotoside";

                sideLoadingView.setVisibility(View.VISIBLE);
                sideImgBtn.setVisibility(View.INVISIBLE);

                break;
            case 2:
                url = "http://bodyline14.ru/api/1.0/offline/addmeasurephotoback";

                backLoadingView.setVisibility(View.VISIBLE);
                backImgBtn.setVisibility(View.INVISIBLE);

                break;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.setForceMultipartEntityContentType(true);
        try {
            params.put("image", imgData, "images/jpeg");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        client.addHeader("Authorization","Bearer "+token);
        client.post(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {

                    switch (tappedIndex) {
                        case 0:
                            frontLoadingView.setVisibility(View.INVISIBLE);
                            frontImgBtn.setVisibility(View.VISIBLE);
                            break;
                        case 1:
                            sideLoadingView.setVisibility(View.INVISIBLE);
                            sideImgBtn.setVisibility(View.VISIBLE);
                            break;
                        case 2:
                            backLoadingView.setVisibility(View.INVISIBLE);
                            backImgBtn.setVisibility(View.VISIBLE);
                            break;
                    }

                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            Log.d("Measure", "photos saved");

                            Bitmap d = BitmapFactory.decodeFile(imgData.getPath());
                            int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
                            Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                            switch (tappedIndex) {
                                case 0:
                                    frontImgView.setImageBitmap(scaled);
                                    break;
                                case 1:
                                    sideImgView.setImageBitmap(scaled);
                                    break;
                                case 2:
                                    backImgView.setImageBitmap(scaled);
                                    break;
                                default:
                                    break;
                            }

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                switch (tappedIndex) {
                    case 0:
                        frontLoadingView.setVisibility(View.INVISIBLE);
                        frontImgBtn.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        sideLoadingView.setVisibility(View.INVISIBLE);
                        sideImgBtn.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        backLoadingView.setVisibility(View.INVISIBLE);
                        backImgBtn.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

    }

    private void setupTopButtons(View view) {

        startWeight = view.findViewById(R.id.startWeight);
        startWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        targetWeight = view.findViewById(R.id.targetWeight);
        targetWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWeightDialog(targetWeightTxt);
            }
        });

        kcal = view.findViewById(R.id.kcal);
        kcal.setOnClickListener((View v) -> {

        });

        currentWeight = view.findViewById(R.id.currentWeight);
        currentWeight.setOnClickListener((View v) -> {
            showWeightDialog(currentWeightTxt);
        });

    }

    private void showWeightDialog(TextView textView) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(activity).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_weight, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView kgPicker = dialogView.findViewById(R.id.kg);
        final NumberPickerView grPicker = dialogView.findViewById(R.id.gr);

        setData(kgPicker, kgValues);
        setData(grPicker, grValues);

        Button button1 = dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener((View v) -> {
                dialogBuilder.dismiss();
        });
        button1.setOnClickListener((View v) -> {
                // DO SOMETHINGS
                sendWeight(token, kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue()), textView);

                dialogBuilder.dismiss();
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void sendWeight(String token, String weight, TextView textView) {

        String url = "";

        if (textView == targetWeightTxt) {
            url = "addweightgoal";
        } else {
            url = "addweight";
        }

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        params.add("weight", weight);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/api/1.0/offline/" + url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", "addGoalWeight: " + json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:

                                textView.setText(weight + " КГ");
                                runGetProfile();

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void runGetProfile() {

        getProfile(token);

    }

    private void getProfile(final String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/api/1.0/offline/getprofile", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:
                                //SET INFO

                                canMeasure = json.getInt("canMeasure");
                                today = json.getInt("today");
                                groupTitle = json.getString("groupTitle");
                                measures = json.getJSONArray("measures");

                                startWeightTxt.setText(json.getDouble("startWeight") + " КГ");
                                targetWeightTxt.setText(json.getDouble("goalWeight") + " КГ");
                                kcalTxt.setText(json.getString("dayCalories"));

                                measureAdapter.addData(canMeasure, measures);

                                getWeightGraph(token);

                                //loadingView.setVisibility(View.INVISIBLE);

                                break;
                            case 16:

                                //GET QUESTIONS

                                Integer lastDay = json.getInt("lastDay");

                                getQuestions(token, lastDay);

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void getWeightGraph(String token) {

        Log.d("ProfileOfflineFragment", "token: " + token);

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/api/1.0/offline/getweightgraph", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:
                                //SET INFO

                                weightGraphData = json.getJSONArray("data");

                                getCurrentWeight();

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void getCurrentWeight() {

        Log.d("ProfileOfflineFragment", "token: " + token);

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/api/1.0/offline/getcurrentweight", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:
                                //SET INFO

                                String weight = json.getString("data");
                                currentWeightTxt.setText(weight);

                                showMainInfo();

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showMainInfo() {

        setWeightGraph();

    }

    private void setWeightGraph() {

        //MARK: Block 3

        entries2.clear();

        for (int i = 0; i < weightGraphData.length(); i++) {

            try {
                JSONObject weightData = weightGraphData.getJSONObject(i);

                Float day = (float)weightData.getInt("day");
                Float weight = (float)weightData.getInt("weight");

                Entry newEntry = new Entry(day, weight);

                entries2.add(newEntry);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        LineDataSet lineDataSet = new LineDataSet(entries2, "Мой вес");
        lineDataSet.setColor(getResources().getColor(R.color.blue_light));
        lineDataSet.setCircleColor(getResources().getColor(R.color.time_white));
        lineDataSet.setDrawCircles(false);


        LineData lineData = new LineData(lineDataSet);

        weightGraph.setData(lineData);
        weightGraph.getDescription().setText("График замеров веса в килограммах");

        weightGraph.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        weightGraph.getDescription().setTextColor(getResources().getColor(R.color.time_white));
        weightGraph.getLegend().setTextColor(getResources().getColor(R.color.time_white));
        weightGraph.getXAxis().setTextColor(getResources().getColor(R.color.time_white));
        weightGraph.getAxisLeft().setTextColor(getResources().getColor(R.color.time_white));
        weightGraph.getAxisRight().setTextColor(getResources().getColor(R.color.time_white));

        weightGraph.getLegend();

        weightGraph.notifyDataSetChanged();
        weightGraph.invalidate();

        loadingView.setVisibility(View.INVISIBLE);

    }


    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

    private void getQuestions(String token, Integer lastDay) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/api/1.0/offline/getquestionnaire", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:

                                JSONArray questions = json.getJSONArray("data");

                                Intent intent = new Intent(activity, PollingPage.class);

                                intent.putExtra("questions", questions.toString());
                                intent.putExtra("lastDay", lastDay);

                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                startActivity(intent);

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {



                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    @Override
    public void onValueChange(NumberPickerView picker, int oldVal, int newVal) {

    }

    private void setData(NumberPickerView picker, ArrayList<String> valuesList){
        String[] values = new String[valuesList.size()];

        for (int i = 0; i < values.length; i++) {
            String str = valuesList.get(i);
            values[i] = str;
        }

        picker.setDisplayedValues(values);
        picker.setMinValue(0);
        picker.setMaxValue(values.length - 1);
        picker.setValue(0);
    }

}
