package com.dmitr.bodyline.OFFLINE.Fragments.Profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.dmitr.bodyline.OFFLINE.MainOffline;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class MeasuresListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;

    JSONArray measures;
    String token;
    Integer canMeasure;

    Integer segmentSelectedPosition = 0;

    String arms;
    String stomach;
    String hips;
    String foots;
    String weight;

    //ДИАЛОГИ
    ArrayList<String> kgValues = new ArrayList<>();
    ArrayList<String> grValues = new ArrayList<>();

    ArrayList<String> cmValues = new ArrayList<>();
    ArrayList<String> mmValues = new ArrayList<>();

    public MeasuresListAdapter(Activity activity, JSONArray measures, Integer canMeasure, String token) {

        this.activity = activity;
        this.token = token;
        this.canMeasure = canMeasure;

        for (int i = 35; i < 301; i++) {
            kgValues.add(""+i);
        }

        for (int i = 0; i < 10; i++) {
            grValues.add(""+i);
        }

        for (int i = 5; i < 201; i++) {
            cmValues.add(""+i);
        }

        for (int i = 0; i < 9; i++) {
            mmValues.add(""+i);
        }
    }

    public static class EditTextViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public EditTextViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    public static class TextViewViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public TextViewViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    public static class SendBtnViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public SendBtnViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        switch (viewType) {
            case 1:
                View itemDropDownView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_measure_edittext, parent, false);
                return new MeasuresListAdapter.EditTextViewHolder(itemDropDownView);
            case 2:
                View itemEditTextView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_measure_textview, parent, false);
                return new MeasuresListAdapter.TextViewViewHolder(itemEditTextView);
            case 3:
                View itemSendBtnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_send_btn, parent, false);
                return new MeasuresListAdapter.SendBtnViewHolder(itemSendBtnView);

            default:
                View itemSendBtnView2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_send_btn, parent, false);
                return new MeasuresListAdapter.SendBtnViewHolder(itemSendBtnView2);
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 1:

                Button btn = holder.itemView.findViewById(R.id.measureBtn);
                TextView textView = holder.itemView.findViewById(R.id.dateTxt);

                textView.setText("Сегодня");

                switch (segmentSelectedPosition) {
                    case 0:
                        btn.setHint("Руки");
                        btn.setText(arms);
                        break;
                    case 1:
                        btn.setHint("Живот");
                        btn.setText(stomach);
                        break;
                    case 2:
                        btn.setHint("Бедра");
                        btn.setText(hips);
                        break;
                    case 3:
                        btn.setHint("Ноги");
                        btn.setText(foots);
                        break;
                    case 4:
                        btn.setHint("Вес");
                        btn.setText(weight);
                        break;
                }

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (segmentSelectedPosition == 4) {

                            showWeightDialog(segmentSelectedPosition, (Button)v);

                        } else {

                            showLengthDialog(segmentSelectedPosition, (Button)v);

                        }

                    }
                });

                break;
            case 2:

                TextView measureTxt = holder.itemView.findViewById(R.id.measureTxt);
                TextView dateTxt = holder.itemView.findViewById(R.id.dateTxt);

                try {

                    Integer pos;

                    if (canMeasure == 1) {
                        pos = position - 1;
                    } else {
                        pos = position;
                    }

                    dateTxt.setText(measures.getJSONObject(pos).getString("date"));

                    switch (segmentSelectedPosition) {
                        case 0:
                            measureTxt.setText(""+measures.getJSONObject(pos).getDouble("m_hand"));
                            break;
                        case 1:
                            measureTxt.setText(""+measures.getJSONObject(pos).getDouble("m_vstomach"));
                            break;
                        case 2:
                            measureTxt.setText(""+measures.getJSONObject(pos).getDouble("m_vhip"));
                            break;
                        case 3:
                            measureTxt.setText(""+measures.getJSONObject(pos).getDouble("m_vfoots"));
                            break;
                        case 4:
                            measureTxt.setText(""+measures.getJSONObject(pos).getDouble("m_weight"));
                            break;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                break;
            case 3:

                Button sendBtn = holder.itemView.findViewById(R.id.sendBtn);

                sendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Boolean isAllAdded = true;

                        if (arms == null || arms.isEmpty()) {
                            isAllAdded = false;
                        } else if (stomach == null || stomach.isEmpty()) {
                            isAllAdded = false;
                        } else if (hips == null || hips.isEmpty()) {
                            isAllAdded = false;
                        } else if (foots == null || foots.isEmpty()) {
                            isAllAdded = false;
                        } else if (weight == null || weight.isEmpty()) {
                            isAllAdded = false;
                        }


                        if (isAllAdded) {

                            sendMeasures(token);

                        } else {

                            showErrID(4);

                        }

                    }
                });

                break;
        }

    }

    @Override
    public int getItemCount() {
        if (measures != null) {
            if (canMeasure == 1) {
                return measures.length() + 2;
            } else {
                return measures.length();
            }
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if(canMeasure == 1) {
                return 1;
            } else {
                return 2;
            }
        } else if (position < measures.length() + 1) {
            return 2;
        } else {
            return 3;
        }
    }

    private void sendMeasures(String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);

        JSONObject data = new JSONObject();
        JSONObject dataparams = new JSONObject();
        StringEntity entity = new StringEntity("", "");
        try {

            dataparams.put("height", "1");
            dataparams.put("weight", weight);
            dataparams.put("want_weight", "1");
            dataparams.put("breast", "1");
            dataparams.put("waist", "1");
            dataparams.put("stomach", stomach);
            dataparams.put("hip", hips);
            dataparams.put("foots", foots);
            dataparams.put("hand", arms);

            data.put("data", dataparams);

            entity = new StringEntity(data.toString(), "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.d("PollingListAdapter", "entity: " + data.toString());

        client.post(activity.getApplicationContext(), "http://bodyline14.ru/api/1.0/offline/addmeasure", entity, "application/json; charset=utf-8", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                //loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:

                                Intent intent = new Intent(activity, MainOffline.class);

                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                activity.getApplicationContext().startActivity(intent);

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {



                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                //loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

    public void segmentChanged(Integer position) {
        this.segmentSelectedPosition = position;
        notifyDataSetChanged();
    }

    public void addData(Integer canMeasure, JSONArray measures) {
        this.canMeasure = canMeasure;
        this.measures = measures;
        notifyDataSetChanged();
    }

    private void showWeightDialog(int selectedSegmentIndex, Button btn) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(activity).create();
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_weight, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView kgPicker = dialogView.findViewById(R.id.kg);
        final NumberPickerView grPicker = dialogView.findViewById(R.id.gr);

        setData(kgPicker, kgValues);
        setData(grPicker, grValues);

        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHING

                switch (selectedSegmentIndex) {
                    case 0:
                        arms = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                        btn.setText(arms);
                        break;
                    case 1:
                        stomach = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                        btn.setText(stomach);
                        break;
                    case 2:
                        hips = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                        btn.setText(hips);
                        break;
                    case 3:
                        foots = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                        btn.setText(foots);
                        break;
                    case 4:
                        weight = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                        btn.setText(weight);
                        break;
                }

                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void showLengthDialog(int selectedSegmentIndex, Button btn) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(activity).create();
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_lenght, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView cmPicker = dialogView.findViewById(R.id.cm);
        final NumberPickerView mmPicker = dialogView.findViewById(R.id.mm);

        setData(cmPicker, cmValues);
        setData(mmPicker, mmValues);

        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS

                switch (selectedSegmentIndex) {
                    case 0:
                        arms = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                        btn.setText(arms);
                        break;
                    case 1:
                        stomach = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                        btn.setText(stomach);
                        break;
                    case 2:
                        hips = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                        btn.setText(hips);
                        break;
                    case 3:
                        foots = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                        btn.setText(foots);
                        break;
                    case 4:
                        weight = cmValues.get(cmPicker.getValue()) + "." + mmValues.get(mmPicker.getValue());
                        btn.setText(weight);
                        break;
                }

                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void setData(NumberPickerView picker, ArrayList<String> valuesList){
        String[] values = new String[valuesList.size()];

        for (int i = 0; i < values.length; i++) {
            String str = valuesList.get(i);
            values[i] = str;
        }

        picker.setDisplayedValues(values);
        picker.setMinValue(0);
        picker.setMaxValue(values.length - 1);
        picker.setValue(0);
    }

}
