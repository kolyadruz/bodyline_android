package com.dmitr.bodyline.OFFLINE.Polling;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dmitr.bodyline.R;

import org.json.JSONArray;

public class PollingPage extends AppCompatActivity {

    RecyclerView recyclerView;
    PollingListAdapter pollingListAdapter;
    JSONArray questions;
    Toolbar toolbar;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_polling_page);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("Анкета");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        String strQuestions = getIntent().getStringExtra("questions");
        Integer lastDay = getIntent().getIntExtra("lastDay", 0);

        try {

            questions = new JSONArray(strQuestions);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.d("PollingPage", questions.toString());
        Log.d("PollingPage", "questionsArray size: " + questions.length());
        Log.d("PollingPage", "lastDAy: " + lastDay);

        pollingListAdapter = new PollingListAdapter(this, questions, token, lastDay);

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(llm);

        recyclerView.setAdapter(pollingListAdapter);

    }
}
