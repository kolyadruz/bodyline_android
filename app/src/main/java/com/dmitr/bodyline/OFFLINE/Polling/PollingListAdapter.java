package com.dmitr.bodyline.OFFLINE.Polling;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import com.dmitr.bodyline.OFFLINE.MainOffline;
import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class PollingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;

    JSONArray questions;

    Integer[] questionsIDs;
    Integer[] answersIDs;

    String token;

    Integer lastDay;

    String pluses;
    String  minuses;

    public PollingListAdapter(Activity activity, JSONArray questions, String token, Integer lastDay) {

        this.activity = activity;
        this.questions = questions;

        this.questionsIDs = new Integer[questions.length()];
        this.answersIDs = new Integer[questions.length()];

        this.token = token;

        this.lastDay = lastDay;
    }

    public static class DropDownViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public DropDownViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    public static class EditTextViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public EditTextViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    public static class SendBtnViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View itemView;

        public SendBtnViewHolder(View v) {
            super(v);
            itemView = v;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        switch (viewType) {
            case 1:
                View itemDropDownView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_dropdown, parent, false);
                return new DropDownViewHolder(itemDropDownView);
            case 2:
                View itemEditTextView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_edit_text, parent, false);
                return new EditTextViewHolder(itemEditTextView);
            case 3:
                View itemSendBtnView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_send_btn, parent, false);
                return new SendBtnViewHolder(itemSendBtnView);

            default:
                View itemSendBtnView2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polling_page_send_btn, parent, false);
                return new SendBtnViewHolder(itemSendBtnView2);
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 1:

                Button dropDown =  holder.itemView.findViewById(R.id.dropdownbtn);

                try {
                    JSONObject obj = questions.getJSONObject(position);

                    JSONArray answers = obj.getJSONArray("answers");

                    String question = obj.getJSONObject("question").getString("questionText");
                    Integer questionID = obj.getJSONObject("question").getInt("questionId");

                    questionsIDs[position] = questionID;

                    dropDown.setHint(question);

                    dropDown.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle(question);

                            String[] answersArray = new String[answers.length()];

                            for (int i = 0; i < answers.length(); i++) {

                                try {
                                    String answer = answers.getJSONObject(i).getString("answerText");
                                    answersArray[i] = answer;

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }

                            builder.setItems(answersArray, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int pos) {
                                    try {

                                        dropDown.setText(answers.getJSONObject(pos).getString("answerText"));
                                        answersIDs[position] = answers.getJSONObject(pos).getInt("answerId");

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });

                            AlertDialog dialog = builder.create();
                            if (!activity.isFinishing()) {
                                dialog.show();
                            }
                        }
                    });

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case 2:

                EditText editText = holder.itemView.findViewById(R.id.edit_text);

                if (position == questions.length()) {
                    editText.setHint("Наши плюсы");
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            pluses = charSequence.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                } else {
                    editText.setHint("Наши минусы");
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            minuses = charSequence.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                }

                break;
            case 3:

                Button sendBtn = holder.itemView.findViewById(R.id.sendBtn);

                sendBtn.setText("СОХРАНИТЬ");

                sendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Boolean isAllSelected = true;

                        for (Integer id: answersIDs) {
                            if (id == null) {
                                isAllSelected = false;
                            }
                        }

                        if(lastDay == 1) {

                            if (pluses == null || pluses.isEmpty()) {
                                isAllSelected = false;
                            }

                            if (minuses == null || minuses.isEmpty()) {
                                isAllSelected = false;
                            }

                        }

                        if (isAllSelected) {

                            sendAnswers(token);

                        } else {

                            showErrID(4);

                        }

                    }
                });

                break;
        }

    }

    @Override
    public int getItemCount() {
        if (questions != null) {

            if (lastDay == 1) {
                return questions.length() + 3;
            } else {
                return questions.length() + 1;
            }
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position < questions.length()) {
            return 1;
        } else if (position < questions.length() + 2) {

            if(lastDay == 1) {
                return 2;
            } else {
                return 3;
            }

        } else {
            return 3;
        }

    }

    private void sendAnswers(String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        client.addHeader("Authorization","Bearer "+token);

        JSONObject data = new JSONObject();
        JSONArray dataArray = new JSONArray();
        StringEntity entity = new StringEntity("", "");
        try {

            for (int i = 0; i < questionsIDs.length; i ++) {

                JSONObject dataparams = new JSONObject();

                dataparams.put("questionId", questionsIDs[i]);
                dataparams.put("answerId", answersIDs[i]);

                dataArray.put(dataparams);
            }

            data.put("answers", dataArray);

            entity = new StringEntity(data.toString(), "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.d("PollingListAdapter", "entity: " + data.toString());

        client.post(activity.getApplicationContext(), "http://bodyline14.ru/api/1.0/offline/answerquestionnaire", entity, "application/json; charset=utf-8", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                //loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:

                                if (lastDay == 0) {

                                    Intent intent = new Intent(activity, MainOffline.class);

                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                    activity.getApplicationContext().startActivity(intent);

                                } else {
                                    sendAnswersEnd(token);
                                }

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {



                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                //loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void sendAnswersEnd(String token) {

        AsyncHttpClient client = new AsyncHttpClient();
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final RequestParams params = new RequestParams();
        params.add("pluses", pluses);
        params.add("minuses", minuses);

        client.addHeader("Authorization","Bearer "+token);


        Log.d("PollingListAdapter", "params: " + params.toString());

        client.post("http://bodyline14.ru/api/1.0/offline/answerendquestionnaire", params,  new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                //loadingView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"

                if (statusCode == 200){
                    String str = new String(responseBody);

                    try {
                        JSONObject json = new JSONObject(str);

                        Log.d("ProfileOfflineFragment", json.toString());

                        Integer err_id = json.getInt("err_id");

                        switch (err_id) {
                            case 0:

                                Intent intent = new Intent(activity, MainOffline.class);

                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                                activity.getApplicationContext().startActivity(intent);

                                break;
                            default:
                                showErrID(err_id);
                                break;
                        }

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {



                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                //loadingView.setVisibility(View.INVISIBLE);
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                if (!activity.isFinishing()) {
                    String Message = "Интернет соединение отсутствует, приложение будет запущено заново";
                    builder.setMessage(Message)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                    AlertDialog alert = builder.create();
                    if (!activity.isFinishing()) {
                        alert.show();
                    }
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

}
