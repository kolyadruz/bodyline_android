package com.dmitr.bodyline.OFFLINE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.MainMenu.MainWithBottom;
import com.dmitr.bodyline.OFFLINE.Polling.PollingPage;
import com.dmitr.bodyline.R;

public class AppModeChooser extends AppCompatActivity {

    Integer opt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_mode_chooser);

        opt = getIntent().getIntExtra("opt", 0);

    }

    public void openOnline(View v) {
        v.setEnabled(false);

        Intent intent = new Intent(getApplicationContext(), MainWithBottom.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("opt",opt);

        startActivity(intent);

        v.setEnabled(true);
    }

    public void openOffline(View v) {
        v.setEnabled(false);

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer isOffline = mSettings.getInt("isOffline", 0);

        //if (isOffline == 1) {

            Intent intent = new Intent(getApplicationContext(), MainOffline.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            startActivity(intent);

        /*} else {

            Intent intent = new Intent(getApplicationContext(), PollingPage.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

            startActivity(intent);

        }*/

        v.setEnabled(false);
    }

}
