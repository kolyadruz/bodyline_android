package com.dmitr.bodyline.OFFLINE;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.dmitr.bodyline.MainMenu.BottomNavigationViewHelper;
import com.dmitr.bodyline.MainMenu.Pages.Account.Pages.Alarm.FragmentAlarm;
import com.dmitr.bodyline.OFFLINE.Fragments.Profile.ProfileOfflineFragment;
import com.dmitr.bodyline.OFFLINE.Rating.FragmentRating;
import com.dmitr.bodyline.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;

public class MainOffline extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 1, CAMERA = 2;

    FragmentManager fragmentManager;

    Toolbar toolbar;
    BottomNavigationView navigation;

    ImageView takePhotoBtn;

    String token;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.account:
                    toolbar.setTitle("Аккаунт");
                    setFragment(new ProfileOfflineFragment());
                    takePhotoBtn.setVisibility(View.VISIBLE);
                    break;
                case R.id.rating:
                    toolbar.setTitle("Рейтинг");
                    setFragment(new FragmentRating());
                    takePhotoBtn.setVisibility(View.GONE);
                    break;
                case R.id.alarm:
                    toolbar.setTitle("Будильник");
                    setFragment(new FragmentAlarm());
                    takePhotoBtn.setVisibility(View.GONE);
                    break;
            }

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_offline);

        fragmentManager = getSupportFragmentManager();

        takePhotoBtn = findViewById(R.id.takePhotoBtn);

        navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.fitness);

        DisplayMetrics disp = getResources().getDisplayMetrics();
        BottomNavigationViewHelper.removeShiftMode(navigation, disp);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("Аккаунт");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        setFragment(new ProfileOfflineFragment());

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();
    }

    public void takePhoto(View v) {

        v.setEnabled(false);

        showImagePickerDialog();

        v.setEnabled(true);

    }

    private void showImagePickerDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String msg = "Фотография ДО";

        builder.setTitle(msg).setMessage("Сделать новую фотографию с помощью камеры или выбрать существующее изображение из гелереи? Фотография может быть без отображения лица (ниже уровня подбородка)")
                .setCancelable(true)
                .setPositiveButton("Выбрать из галереи", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, RESULT_LOAD_IMAGE);

                    }
                })
                .setNegativeButton("Сделать фотографию", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                        startActivityForResult(intent, CAMERA);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap d = BitmapFactory.decodeResource(getResources(), R.drawable.logowhsmall);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = new String[]{MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            d = BitmapFactory.decodeFile(picturePath);

        } else if (requestCode == CAMERA && resultCode == RESULT_OK && null !=data) {

            d = (Bitmap) data.getExtras().get("data");

        }

        int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        scaled.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        //4
        File file = new File(getFilesDir() + "/zamerDO.jpg");
        try {
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            //5
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        uploadPhoto(file);

    }

    private void uploadPhoto(File imgData) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.setForceMultipartEntityContentType(true);
        try {
            params.put("image", imgData, "images/jpeg");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        client.addHeader("Authorization","Bearer " + token);
        client.post("http://bodyline14.ru/api/1.0/offline/uploadphotostart", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {

                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
        });

    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!this.isFinishing()) {
            alert.show();
        }
    }

}
