package com.dmitr.bodyline.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAbonement.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAbonement#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAbonement extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TextView title;
    TextView description;

    String token;
    String s_token;

    Activity activity;

    public FragmentAbonement() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentAbonement.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAbonement newInstance(String param1, String param2) {
        FragmentAbonement fragment = new FragmentAbonement();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_abonement, container, false);

        activity = getActivity();

        //title = (TextView) root.findViewById(R.id.title);
        //description = (TextView) root.findViewById(R.id.description);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        runGetActiveUserSeason();
    }

    private void runGetActiveUserSeason() {

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        token = mSettings.getString("token", "");
        s_token = mSettings.getString("s_token", "");

        //getActiveUserSeason(token, s_token);

    }

    private void getActiveUserSeason(String token, String s_token){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getactiveuserseason", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    try {
                        JSONObject json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            JSONObject season_info = json.getJSONObject("season_info");
                            String name = season_info.getString("name");
                            String descr = season_info.getString("description");

                            title.setText(name);
                            description.setText(descr);

                        }
                        Log.d("Login: s_token", ""+json);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String Message = "Интернет соединение отсутствует";
                builder.setMessage(Message)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                AlertDialog alert = builder.create();
                if (!activity.isFinishing()) {
                    alert.show();
                }
            }

        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
