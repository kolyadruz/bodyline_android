package com.dmitr.bodyline.pager;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

/**
 * Created by Programmist_kts on 19.10.2015.
 */
public class MyPagerAdapter extends PagerAdapter {

    List<View> pages = null;

    public MyPagerAdapter(List<View> pages){
        this.pages = pages;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position){
        View v = pages.get(position);
        ((ViewPager) collection).addView(v, 0);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view){
        ((ViewPager) collection).removeView((View) view);
    }

    @Override
    public int getCount(){
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view.equals(object);
    }

    @Override
    public void finishUpdate(View arg0){
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1){
    }

    @Override
    public Parcelable saveState(){
        return null;
    }

    @Override
    public void startUpdate(View arg0){
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}