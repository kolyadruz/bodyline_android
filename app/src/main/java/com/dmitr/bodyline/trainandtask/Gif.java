package com.dmitr.bodyline.trainandtask;

import android.os.Parcel;
import android.os.Parcelable;

public class Gif  implements Parcelable {

    private String title;
    private String description;
    private String url;
    private String filename;

    private Integer isBothPlace;

    public Gif() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(url);
        parcel.writeString(filename);

        parcel.writeInt(isBothPlace);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Gif createFromParcel(Parcel in) {
            return new Gif(in);
        }

        @Override
        public Gif[] newArray(int i) {
            return new Gif[i];
        }
    };

    public Gif(Parcel in){
        title=in.readString();
        description=in.readString();
        url=in.readString();
        filename=in.readString();

        isBothPlace=in.readInt();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getIsBothPLace() {
        return isBothPlace;
    }

    public void setIsBothPlace(Integer isBothPLace) {
        this.isBothPlace = isBothPLace;
    }
}
