package com.dmitr.bodyline.trainandtask;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.dmitr.bodyline.ActionListener;
import com.dmitr.bodyline.R;
import com.google.android.exoplayer2.ui.PlayerView;
import com.squareup.picasso.Picasso;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.ene.toro.ToroPlayer;
import im.ene.toro.helper.ToroPlayerHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

public final class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull
    private final List<DownloadData> downloads = new ArrayList<>();
    @NonNull
    private final ActionListener actionListener;

    private List<TrainContain> contains = new ArrayList<>();

    private String description;

    public VideoAdapter(@NonNull final ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder implements ToroPlayer {

        @BindView(R.id.list_item_title)
        TextView titleView;

        @BindView(R.id.list_item_progress_bar)
        ProgressBar progressBar;

        @BindView(R.id.list_item_progress_text)
        TextView progressView;

        @BindView(R.id.list_item_state_button)
        Button downloadButton;

        @BindView(R.id.list_item_description)
        TextView descriptionView;

        @BindView(R.id.playerFrame)
        RelativeLayout playerFrame;

        @BindView(R.id.imageFrame)
        ImageView imageFrame;

        @BindView(R.id.thumbs)
        ImageView thumbs;

        //SimpleExoPlayerView playerView;
        //SimpleExoPlayerViewHelper helper;
        @Nullable
        ToroPlayerHelper helper;
        @Nullable private Uri mediaUri;

        @BindView(R.id.player)
        PlayerView playerView;

        String fileName;

        VideoViewHolder(View itemView) {
            super(itemView);
            //playerView = (SimpleExoPlayerView) itemView.findViewById(R.id.player);
            ButterKnife.bind(this, itemView);
        }

        @NonNull
        @Override
        public View getPlayerView() {
            return playerView;
        }

        @NonNull @Override
        public PlaybackInfo getCurrentPlaybackInfo() {
            return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
        }

        @Override
        public void initialize(@NonNull Container container, @Nullable PlaybackInfo playbackInfo) {
            if (helper == null && mediaUri != null) {
                helper = new MyViewHelper(container, this, mediaUri);
            }
            try {
                helper.initialize(container, playbackInfo);
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void play() {
            if (helper != null) {
                try {
                    helper.play();
                    //playerView.getPlayer().setPlayWhenReady(false);
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public void pause() {
            if (helper != null) helper.pause();
        }

        @Override
        public boolean isPlaying() {
            return helper != null && helper.isPlaying();
        }

        @Override
        public void release() {
            if (helper != null) {
                helper.release();
                helper = null;
            }
        }

        @Override
        public boolean wantsToPlay() {
            if (mediaUri != null) {
                try {
                    File file = new File(mediaUri.getPath());
                    return file.exists();//((ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85) && file.exists());
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                    return false;
                }
            } else {
                return false;
            }
        }

        @Override
        public int getPlayerOrder() {
            return getAdapterPosition();
        }

        @Override public String toString() {
            return "ExoPlayer{" + hashCode() + " " + getAdapterPosition() + "}";
        }

        void bind(Uri media) {
            this.mediaUri = media;
        }
    }

    public class DescritpionHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView descriptionTxt;

        public DescritpionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ContainDescriptionHolder extends RecyclerView.ViewHolder {

        public ContainDescriptionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ContainHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView descriptionTxt;
        @BindView(R.id.description)
        TextView containTxt;

        public ContainHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

        switch (viewType) {
            case 0:
                final View containsTitleItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_training_title, parent, false);
                return new DescritpionHolder(containsTitleItemView);
            case 1:
                final View videoItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_training_video, parent, false);
                return new VideoViewHolder(videoItemView);
            case 2:
                final View containsDescriptionItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_training_description_title, parent, false);
                return new ContainDescriptionHolder(containsDescriptionItemView);
            case 3:
                final View containsItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_training_description, parent, false);
                return new ContainHolder(containsItemView);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case 0:
                DescritpionHolder descritpionHolder = (DescritpionHolder)holder;

                descritpionHolder.descriptionTxt.setText(description);
                break;
            case 1:
                Log.d("VideoAdapter","downloadsCount: "+ downloads.size());

                VideoViewHolder videoHolder = (VideoViewHolder) holder;

                Integer correctedPosition = 0;

                if (description != null) {
                    correctedPosition = holder.getAdapterPosition() - 1;
                } else {
                    correctedPosition = holder.getAdapterPosition();
                }
                Extras dowloadExtras = downloads.get(correctedPosition).download.getExtras();

                Boolean isImage = dowloadExtras.getBoolean("isImage", false);

                String title = dowloadExtras.getString("title", "");
                videoHolder.titleView.setText(title);

                String description = dowloadExtras.getString("description", "");
                videoHolder.descriptionView.setText(description);

                if (isImage) {

                    videoHolder.playerFrame.setVisibility(View.INVISIBLE);
                    String url = dowloadExtras.getString("url", "");
                    Picasso.get().load(url).into(videoHolder.imageFrame);

                    Log.d("VideoAdapter", title + " - IS IMAGE");

                } else {

                    String thumbs = dowloadExtras.getString("thumbs", "");
                    if (!thumbs.equals("")) {
                        Picasso.get().load(thumbs).into(videoHolder.thumbs);
                    }

                    videoHolder.imageFrame.setVisibility(View.INVISIBLE);

                    //holder.playerView.getPlayer().setPlayWhenReady(false);
                    String videoUrl = dowloadExtras.getString("url", "");
                    videoHolder.itemView.setTag(videoUrl);
                    videoHolder.playerView.setVisibility(View.INVISIBLE);

                    final DownloadData downloadData = downloads.get(correctedPosition);
                    String url = "";
                    if (downloadData.download != null) {
                        url = downloadData.download.getUrl();
                    }

                    File file = new File(downloadData.download.getFile());
                    videoHolder.bind(Uri.fromFile(file));

                    final Uri uri = Uri.parse(url);
                    final Status status = downloadData.download.getStatus();
                    final Context context = holder.itemView.getContext();


                    int progress = downloadData.download.getProgress();
                    if (progress == -1) { // Download progress is undermined at the moment.
                        progress = 0;
                    }

            /*if (downloadData.eta == -1) {
                holder.timeRemainingTextView.setText("");
            } else {
                holder.timeRemainingTextView.setText(Utils.getETAString(context, downloadData.eta));
            }

            if (downloadData.downloadedBytesPerSecond == 0) {
                holder.downloadedBytesPerSecondTextView.setText("");
            } else {
                holder.downloadedBytesPerSecondTextView.setText(Utils.getDownloadSpeedString(context, downloadData.downloadedBytesPerSecond));
            }*/

                    Log.d("VideoAdapter", "position: " + correctedPosition + " status: " + status);

                    videoHolder.downloadButton.setBackgroundResource(R.drawable.download);
                    videoHolder.downloadButton.setVisibility(View.VISIBLE);

                    videoHolder.progressBar.setProgress(progress);
                    videoHolder.progressView.setText("" + progress);

                    switch (status) {

                        case COMPLETED: {

                            videoHolder.downloadButton.setVisibility(View.INVISIBLE);

                            videoHolder.progressBar.setVisibility(View.INVISIBLE);
                            videoHolder.progressView.setVisibility(View.INVISIBLE);

                            videoHolder.playerView.setVisibility(View.VISIBLE);
                            videoHolder.thumbs.setVisibility(View.VISIBLE);

                            //File file = new File(holder.fileName);
                            if (file.exists()) {
                                //holder.bind(Uri.fromFile(file));
//                        notifyItemChanged(position);
                                videoHolder.thumbs.setVisibility(View.INVISIBLE);
                            }
                            break;
                        }
                        case FAILED: {

                            //holder.downloadButton.setText(R.string.retry);
                            videoHolder.downloadButton.setBackgroundResource(R.drawable.download);

                            videoHolder.progressView.setText("Ошибка при загрузке");

                            videoHolder.progressBar.setVisibility(View.VISIBLE);
                            videoHolder.progressView.setVisibility(View.VISIBLE);
                            videoHolder.thumbs.setVisibility(View.VISIBLE);

                            videoHolder.downloadButton.setOnClickListener(view -> {
                                //holder.downloadButton.setEnabled(false);
                                actionListener.onResumeDownload(downloadData.download.getId());
                            });

                            break;
                        }
                        case PAUSED: {
                            videoHolder.downloadButton.setBackgroundResource(R.drawable.download);

                            videoHolder.progressBar.setVisibility(View.INVISIBLE);
                            videoHolder.progressView.setVisibility(View.INVISIBLE);
                            videoHolder.thumbs.setVisibility(View.VISIBLE);
                            videoHolder.downloadButton.setOnClickListener(view -> {
                                //holder.downloadButton.setEnabled(false);
                                actionListener.onResumeDownload(downloadData.download.getId());
                            });

                            break;
                        }
                        case DOWNLOADING: {
                            videoHolder.downloadButton.setBackgroundResource(R.drawable.undone);

                            videoHolder.progressBar.setVisibility(View.VISIBLE);
                            videoHolder.progressView.setVisibility(View.VISIBLE);
                            videoHolder.thumbs.setVisibility(View.VISIBLE);

                            videoHolder.downloadButton.setOnClickListener(view -> {
                                //holder.downloadButton.setEnabled(false);
                                actionListener.onPauseDownload(downloadData.download.getId());
                            });

                            break;
                        }
                        case QUEUED: {

                            videoHolder.downloadButton.setBackgroundResource(R.drawable.undone);

                            videoHolder.progressBar.setVisibility(View.VISIBLE);
                            videoHolder.progressView.setVisibility(View.VISIBLE);

                            videoHolder.thumbs.setVisibility(View.VISIBLE);

                            videoHolder.downloadButton.setOnClickListener(view -> {
                                //holder.downloadButton.setEnabled(false);
                                actionListener.onPauseDownload(downloadData.download.getId());
                            });

                            break;
                        }
                        case ADDED: {

                            videoHolder.downloadButton.setBackgroundResource(R.drawable.download);

                            videoHolder.progressBar.setVisibility(View.INVISIBLE);
                            videoHolder.progressView.setVisibility(View.INVISIBLE);

                            videoHolder.thumbs.setVisibility(View.VISIBLE);

                            videoHolder.downloadButton.setOnClickListener(view -> {
                                //holder.downloadButton.setEnabled(false);
                                actionListener.onResumeDownload(downloadData.download.getId());
                            });

                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }
                break;
            case 2:

                ContainDescriptionHolder containDescriptionHolder = (ContainDescriptionHolder)holder;

                break;
            case 3:

                Integer correctedPos = holder.getAdapterPosition() - (downloads.size() + 2);

                ContainHolder containHolder = (ContainHolder)holder;

                TrainContain contain = contains.get(correctedPos);


                containHolder.descriptionTxt.setText(contain.getTitle());
                containHolder.containTxt.setText(contain.getContain());

                break;
            default:
                break;
        }
    }

    public void updateCell(int position) {
        notifyItemChanged(position);
    }


    public void addDownload(@NonNull final Download download) {

        boolean found = false;
        DownloadData data = null;
        int dataPosition = -1;
        for (int i = 0; i < downloads.size(); i++) {
            final DownloadData downloadData = downloads.get(i);
            if (downloadData.id == download.getId()) {
                data = downloadData;
                dataPosition = i;
                found = true;
                break;
            }
        }
        if (!found) {
            final DownloadData downloadData = new DownloadData();
            downloadData.id = download.getId();
            downloadData.download = download;
            downloads.add(downloadData);
        } else {
            data.download = download;
            notifyItemChanged(dataPosition);
        }
    }

    public void setContains(@NonNull final List<TrainContain> contains) {

        this.contains = contains;
        notifyDataSetChanged();

    }

    public void setDescription(@NonNull final String description) {

        this.description = description;
        notifyDataSetChanged();

    }

    @Override
    public int getItemViewType(int position) {
        if (description != null) {
            if (position == 0) {
                return 0;
            }
            if (position < downloads.size() + 1) {
                return 1;
            } else {
                if (position == downloads.size() + 1) {
                    return 2;
                } else {
                    return 3;
                }
            }
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        if (description != null) {
            return downloads.size() + contains.size() + 2;
        } else {
            return downloads.size();
        }
    }

    public void update(@NonNull final Download download, long eta, long downloadedBytesPerSecond) {

        for (int position = 0; position < downloads.size(); position++) {
            final DownloadData downloadData = downloads.get(position);
            if (downloadData.id == download.getId()) {
                switch (download.getStatus()) {
                    case REMOVED:
                    case DELETED: {
                        downloads.remove(position);
                        notifyItemRemoved(position);
                        break;
                    }
                    default: {
                        downloadData.download = download;
                        downloadData.eta = eta;
                        downloadData.downloadedBytesPerSecond = downloadedBytesPerSecond;
                        notifyItemChanged(position);
                    }
                }
                return;
            }
        }
    }

    public static class DownloadData {
        public int id;
        @Nullable
        public Download download;
        long eta = -1, downloadedBytesPerSecond = 0;

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public String toString() {
            if (download == null) {
                return "";
            }
            return download.toString();
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof DownloadData && ((DownloadData) obj).id == id;
        }
    }

}