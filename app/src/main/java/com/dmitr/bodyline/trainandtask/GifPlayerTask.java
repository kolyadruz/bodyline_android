package com.dmitr.bodyline.trainandtask;

public class GifPlayerTask {

    public String title;
    public String fileName;
    public Long time;
    public String url;
    public Boolean isAnimation;
    public Boolean isSecondFromBoth;

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileName() {
        return fileName;
    }

    public void setTime(Long time) {
        this.time = time;
    }
    public Long getTime() {
        return time;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }


    public void setIsAnimation(Boolean isAnimation) {
        this.isAnimation = isAnimation;
    }

    public Boolean getIsAnimation() {
        return isAnimation;
    }

    public void setIsSecondFromBoth(Boolean isSecondFromBoth) {
        this.isSecondFromBoth = isSecondFromBoth;
    }

    public Boolean getIsSecondFromBoth() {
        return isSecondFromBoth;
    }
}
