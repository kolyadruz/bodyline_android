package com.dmitr.bodyline.trainandtask;

public class TrainContain {

    private String title;
    private String contain;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContain() {
        return contain;
    }

    public void setContain(String contain) {
        this.contain = contain;
    }

}
