package com.dmitr.bodyline.trainandtask;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.exoplayer2.ui.PlayerView;

import im.ene.toro.ToroPlayer;
import im.ene.toro.annotations.RemoveIn;
import im.ene.toro.exoplayer.Config;
import im.ene.toro.exoplayer.ExoCreator;
import im.ene.toro.exoplayer.ExoPlayable;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.helper.ToroPlayerHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.media.VolumeInfo;
import im.ene.toro.widget.Container;

import static im.ene.toro.ToroUtil.checkNotNull;
import static im.ene.toro.exoplayer.ToroExo.with;

public class MyViewHelper extends ToroPlayerHelper {

    @NonNull
    private final ExoPlayable playable;
    @NonNull private final MyEventListeners listeners;

    // Container is no longer required for constructing new instance.
    @SuppressWarnings("unused") @RemoveIn(version = "3.6.0") @Deprecated  //
    public MyViewHelper(Container container, @NonNull ToroPlayer player, @NonNull Uri uri) {
        this(player, uri);
    }

    public MyViewHelper(@NonNull ToroPlayer player, @NonNull Uri uri) {
        this(player, uri, null);
    }

    public MyViewHelper(@NonNull ToroPlayer player, @NonNull Uri uri, String fileExt) {
        this(player, uri, fileExt, with(player.getPlayerView().getContext()).getDefaultCreator());
    }

    /** Config instance should be kept as global instance. */
    public MyViewHelper(@NonNull ToroPlayer player, @NonNull Uri uri, String fileExt,
                        @NonNull Config config) {
        this(player, uri, fileExt,
                with(player.getPlayerView().getContext()).getCreator(checkNotNull(config)));
    }

    public MyViewHelper(@NonNull ToroPlayer player, @NonNull Uri uri, String fileExt,
                        @NonNull ExoCreator creator) {
        this(player, new ExoPlayable(creator, uri, fileExt));
    }

    public MyViewHelper(@NonNull ToroPlayer player, @NonNull ExoPlayable playable) {
        super(player);
        //noinspection ConstantConditions
        if (player.getPlayerView() == null || !(player.getPlayerView() instanceof PlayerView)) {
            throw new IllegalArgumentException("Require non-null SimpleExoPlayerView");
        }

        listeners = new MyEventListeners();
        this.playable = playable;
    }

    @Override protected void initialize(@NonNull PlaybackInfo playbackInfo) {
        playable.setPlaybackInfo(playbackInfo);
        playable.addEventListener(listeners);
        playable.prepare(true);
        playable.setPlayerView((PlayerView) player.getPlayerView());
    }

    @Override public void release() {
        super.release();
        playable.setPlayerView(null);
        playable.removeEventListener(listeners);
        playable.release();
    }

    @Override public void play() {
        playable.play();
    }

    @Override public void pause() {
        playable.pause();
    }

    @Override public boolean isPlaying() {
        return playable.isPlaying();
    }

    @Override public void setVolume(float volume) {
        playable.setVolume(volume);
    }

    @Override public float getVolume() {
        return playable.getVolume();
    }

    @Override public void setVolumeInfo(@NonNull VolumeInfo volumeInfo) {
        playable.setVolumeInfo(volumeInfo);
    }

    @Override @NonNull public VolumeInfo getVolumeInfo() {
        return playable.getVolumeInfo();
    }

    @NonNull @Override public PlaybackInfo getLatestPlaybackInfo() {
        return playable.getPlaybackInfo();
    }

    @SuppressWarnings("WeakerAccess") //
    public void addEventListener(@NonNull Playable.EventListener listener) {
        //noinspection ConstantConditions
        if (listener != null) this.listeners.add(listener);
    }

    @SuppressWarnings("WeakerAccess") //
    public void removeEventListener(Playable.EventListener listener) {
        this.listeners.remove(listener);
    }

    @Override
    public void addOnVolumeChangeListener(@NonNull ToroPlayer.OnVolumeChangeListener listener) {
        this.playable.addOnVolumeChangeListener(checkNotNull(listener));
    }

    @Override public void removeOnVolumeChangeListener(ToroPlayer.OnVolumeChangeListener listener) {
        this.playable.removeOnVolumeChangeListener(listener);
    }

    // A proxy, to also hook into ToroPlayerHelper's state change event.
    private class MyEventListeners extends MyPlayable.EventListeners {

        MyEventListeners() {
        }

        @Override public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            MyViewHelper.super.onPlayerStateUpdated(playWhenReady, playbackState); // important
            super.onPlayerStateChanged(playWhenReady, playbackState);
        }
    }
}