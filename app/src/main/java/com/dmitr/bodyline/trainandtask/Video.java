package com.dmitr.bodyline.trainandtask;

public class Video {

    private String title;
    private String description;
    private String url;
    private String filename;
    private Integer type; //0 - video, 1 - gif, 2 - image

    public boolean isImage = false;

    private String thumbs;

    public void init(String title, String description, String url, String filename, Integer type, Boolean isBothPlace, boolean isImage, String thumbs) {
        this.title = title;
        this.description = description;
        this.url = url;
        this.filename = filename;
        this.type = type;
        this.isImage = isImage;

        this.thumbs = thumbs;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public boolean getIsImage() {
        return isImage;
    }

    public void setIsImage(boolean isImage) {
        this.isImage = isImage;
    }

    public void setThumbs(String thumbs) {
        this.thumbs = thumbs;
    }

    public String getThumbs() {
        return thumbs;
    }

}