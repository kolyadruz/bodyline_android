package com.dmitr.bodyline;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.collection.ArrayMap;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.dmitr.bodyline.MainMenu.Pages.Season.Data;
import com.dmitr.bodyline.trainandtask.Gif;
import com.dmitr.bodyline.trainandtask.TrainContain;
import com.dmitr.bodyline.trainandtask.Video;
import com.dmitr.bodyline.trainandtask.VideoAdapter;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchErrorUtils;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;
import com.tonyodev.fetch2rx.RxFetch;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import im.ene.toro.PlayerSelector;
import im.ene.toro.widget.Container;
import io.reactivex.disposables.Disposable;
import kotlin.Pair;
import timber.log.Timber;

public class TrainAndTask extends AppCompatActivity implements ActionListener {

    int accessLevel = 0;
    JSONArray events =  new JSONArray();

    String token, s_token, descriptionText = "";
    int day, type, completed = 0;

    //Boolean isFromTraining = false;
    @BindView(R.id.doneBtn) Button doneBtn;
    @BindView(R.id.loadingView) RelativeLayout loadingView;

    //GIFs loading process
    @BindView(R.id.gifsLoadingAlert) RelativeLayout gifsLoadingAlert;
    @BindView(R.id.gifsDownloadBtn) Button gifsDownloadBtn;
    @BindView(R.id.laterGifsDownloadBtn) Button laterGifsDownloadBtn;
    @BindView(R.id.gifsLoadingProcessView) ProgressBar gifsLoadingProcessView;
    @BindView(R.id.gifsLoadingProcessViewLabel) TextView gifsLoadingProcessViewLabel;

    List<Gif> gifsList = new ArrayList<Gif>(), gifsToBeDownloaded = new ArrayList<Gif>();
    List<Integer> gifsCount = new ArrayList<Integer>(), gifsMeasures = new ArrayList<Integer>(), gifsPodh = new ArrayList<Integer>(), gifsRest = new ArrayList<Integer>();
    List<TrainContain> trainContains = new ArrayList<TrainContain>();

    //VIDEOplayer
    @BindView(R.id.player_container)
    Container container;
    LinearLayoutManager layoutManager;
    List<Video> videosList = new ArrayList<Video>();

    //DOWNLOAD "LIST" (VIDEOS)
    private View mainView;
    private VideoAdapter adapter;
    private Fetch fetch;
    int currentDayVideoListDownloadGroup_ID = 0;
    private static final int TRAINGROUP_ID = "trainGroup".hashCode(), TASKGROUP_ID = "taskGroup".hashCode();
    static final String FETCH_NAMESPACE = "DownloadListActivity";

    //DOWNLOAD "GROUP" (GIFs)
    private RxFetch rxFetch;
    @Nullable private Disposable enqueueDisposable, resumeDisposable;
    private final ArrayMap<Integer, Integer> fileProgressMap = new ArrayMap<>();
    Boolean isGroup = false;
    private static final int groupId = 12, STORAGE_PERMISSION_CODE = 100;
    private static final long UNKNOWN_REMAINING_TIME = -1, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainandtask);

        ButterKnife.bind(this);

        setupVars();
        setupViews();

        final FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(20)
                .setHttpDownloader(new OkHttpDownloader(Downloader.FileDownloaderType.PARALLEL))
                .setNamespace(FETCH_NAMESPACE)
                .build();
        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        rxFetch = RxFetch.Impl.getDefaultRxInstance();
        reset();

        checkStoragePermissions();
    }

    private void reset() {
        rxFetch.deleteAll();
        fileProgressMap.clear();

        gifsLoadingProcessView.setProgress(0);
        gifsLoadingProcessViewLabel.setText("");
        gifsDownloadBtn.setVisibility(View.VISIBLE);
    }

    private void setupVars() {
        accessLevel = getIntent().getIntExtra("accessLevel", 0);
        descriptionText = getIntent().getStringExtra("descriptionText");
        day = getIntent().getIntExtra("day", 0);
        type = getIntent().getIntExtra("type", 0);
        s_token = getIntent().getStringExtra("s_token");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        Log.d("TrainAndTask", "accessLevel: "+ accessLevel + " day: "+day+" type: "+type+" s_token: "+s_token);
    }

    private void setupViews() {
        doneBtn.setVisibility(View.GONE);

        mainView = findViewById(R.id.activityMain);
        gifsLoadingAlert.setVisibility(View.INVISIBLE);

        container.setPlayerSelector(PlayerSelector.NONE);
        layoutManager = new LinearLayoutManager(this);
        container.setLayoutManager(layoutManager);

        adapter = new VideoAdapter(this);
        container.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadingView.setVisibility(View.VISIBLE);
        switch (type) {
            case 1:
                currentDayVideoListDownloadGroup_ID = TASKGROUP_ID + day + s_token.hashCode();
                break;
            case 3:
                currentDayVideoListDownloadGroup_ID = TRAINGROUP_ID + day + s_token.hashCode();
                break;
            default:
                break;
        }
        requestSeasonDayData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        doneBtn.setEnabled(true);
        String typeStr = "";
        switch (type) {
            case 1:
                typeStr = "День " + day + ". Задание";
                doneBtn.setText("Выполнено");
                break;
            case 3:
                typeStr = "День " + day + ". Тренировка";
                doneBtn.setText("Приступить");
                break;
            default:
                break;
        }
        //НАЗНАЧИТЬ ТЕКСТ TOOLBAR на typeStr
        fetch.getDownloadsInGroup(currentDayVideoListDownloadGroup_ID, downloads -> {
            final ArrayList<Download> list = new ArrayList<>(downloads);
            Collections.sort(list, (first, second) -> Long.compare(first.getCreated(), second.getCreated()));
            for (Download download : list) {
                adapter.addDownload(download);
            }
        }).addListener(fetchListener);

        rxFetch.addListener(fetchListenerGroup);

        resumeDisposable = rxFetch.getDownloadsInGroup(groupId).asFlowable().subscribe(downloads -> {
            for (Download download : downloads) {
                if (fileProgressMap.containsKey(download.getId())) {
                    fileProgressMap.put(download.getId(), download.getProgress());
                    updateUIWithProgress();
                }
            }
        }, throwable -> {
            final Error error = FetchErrorUtils.getErrorFromThrowable(throwable);
            Timber.d("TrainAndTask Error: %1$s", error);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!rxFetch.isClosed()) {
            rxFetch.removeListener(fetchListener);
        }
        if (!fetch.isClosed()) {
            fetch.removeListener(fetchListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rxFetch.deleteAll();
        rxFetch.close();
        if (enqueueDisposable != null && !enqueueDisposable.isDisposed()) {
            enqueueDisposable.dispose();
        }
        if (resumeDisposable != null && !resumeDisposable.isDisposed()) {
            resumeDisposable.dispose();
        }
        fetch.close();
    }

    private final FetchListener fetchListener = new AbstractFetchListener() {
        @Override
        public void onAdded(@NotNull Download download) {
            adapter.addDownload(download);
        }
        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onCompleted(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {
            super.onError(download, error, throwable);
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onProgress(@NotNull Download download, long etaInMilliseconds, long downloadedBytesPerSecond) {
            adapter.update(download, etaInMilliseconds, downloadedBytesPerSecond);
        }
        @Override
        public void onPaused(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onResumed(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onCancelled(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onRemoved(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
        @Override
        public void onDeleted(@NotNull Download download) {
            adapter.update(download, UNKNOWN_REMAINING_TIME, UNKNOWN_DOWNLOADED_BYTES_PER_SECOND);
        }
    };

    private final FetchListener fetchListenerGroup = new AbstractFetchListener() {
        @Override
        public void onCompleted(@NotNull Download download) {
            fileProgressMap.put(download.getId(), download.getProgress());
            updateUIWithProgress();
        }
        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            super.onError(download, error, throwable);
            reset();
            Snackbar.make(mainView, "Ошибка загрузки GIF файлов", Snackbar.LENGTH_INDEFINITE).show();
        }
        @Override
        public void onProgress(@NotNull Download download, long etaInMilliseconds, long downloadedBytesPerSecond) {
            super.onProgress(download, etaInMilliseconds, downloadedBytesPerSecond);
            fileProgressMap.put(download.getId(), download.getProgress());
            updateUIWithProgress();
        }
    };

    private void checkStoragePermissions() {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } else {*/
            enqueueDownloads();
        //}
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enqueueDownloads();
        } else {
            Snackbar.make(mainView, R.string.permission_not_enabled, Snackbar.LENGTH_INDEFINITE).show();
            if (isGroup) {
                isGroup = false;
                reset();
            }
        }
    }

    private void enqueueDownloads() {
        if (isGroup) {
            isGroup = false;
            final List<Request> requestList = Data.getGifUpdates(gifsToBeDownloaded);
            for (Request request : requestList) {
                request.setGroupId(groupId);
            }
            enqueueDisposable = rxFetch.enqueue(requestList).asFlowable().subscribe(updatedRequests -> {
                for (Pair<Request, Error> request : updatedRequests) {
                    fileProgressMap.put(request.getFirst().getId(), 0);
                    updateUIWithProgress();
                }
            }, throwable -> {
                final Error error = FetchErrorUtils.getErrorFromThrowable(throwable);
                Timber.d("TrainAndTask Error: %1$s", error);
            });
        } else {
            final List<Request> requests = Data.getFetchRequestWithGroupId(currentDayVideoListDownloadGroup_ID, videosList);
            fetch.enqueue(requests, updatedRequests -> {
            });
        }
    }

    private void updateUIWithProgress() {
        final int totalFiles = fileProgressMap.size();
        final int completedFiles = getCompletedFileCount();
        gifsLoadingProcessViewLabel.setText(getResources().getString(R.string.complete_over, completedFiles, totalFiles));
        final int progress = getDownloadProgress();
        gifsLoadingProcessView.setProgress(progress);
        if (completedFiles == totalFiles) {
            if (gifsToBeDownloaded.size() > 0) {
                startTraining();
            }
        }
    }

    private int getDownloadProgress() {
        int currentProgress = 0;
        final int totalProgress = fileProgressMap.size() * 100;
        final Set<Integer> ids = fileProgressMap.keySet();
        for (int id : ids) {
            currentProgress += fileProgressMap.get(id);
        }
        currentProgress = (int) (((double) currentProgress / (double) totalProgress) * 100);
        return currentProgress;
    }

    private int getCompletedFileCount() {
        int count = 0;
        final Set<Integer> ids = fileProgressMap.keySet();
        for (int id : ids) {
            int progress = fileProgressMap.get(id);
            if (progress == 100) {
                count++;
            }
        }
        return count;
    }

    private void requestSeasonDayData() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("s_token", s_token);
        params.put("day", day);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/seasondaydata", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            switch (type) {
                                case 1: //Задания
                                    try {
                                        JSONObject tasks = json.getJSONObject("tasks");
                                        events = tasks.getJSONArray("task");

                                        completed = tasks.getInt("completed");
                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                    break;
                                case 3: //Тренировки
                                    try {
                                        descriptionText = json.getString("lesson_description");
                                        adapter.setDescription(descriptionText);

                                        JSONObject lessons = json.getJSONObject("lessons");
                                        events = lessons.getJSONArray("lesson");

                                        completed = lessons.getInt("completed");
                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            Log.d("TrainAndTask Events:", "" + events);
                            sortVideosFromEventsArray();
                            loadingView.setVisibility(View.INVISIBLE);
                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });

    }

    private void sortVideosFromEventsArray() {

        //Удаление всех видео
        videosList.clear();

        //Удаление всех гифок
        gifsList.clear();
        gifsCount.clear();
        gifsMeasures.clear();
        gifsPodh.clear();
        gifsToBeDownloaded.clear();
        gifsRest.clear();

        trainContains.clear();

        for (int i = 0; i < events.length(); i++){
            try {
                JSONObject data = events.getJSONObject(i);
                String description = data.getString("description");
                String title = data.getString("name");
                String videourl = data.getString("video_url");
                String videofilename = data.getString("filename");

                String url_str = "";
                boolean isImage;
                String filename = "";
                String thumbs = "";

                try {

                    String image_url = data.getString("image_url");
                    url_str = image_url;

                    isImage = true;

                } catch (JSONException ex) {
                    ex.printStackTrace();

                    url_str = videourl;

                    filename = videofilename;

                    thumbs = data.getString("thumbs");

                    isImage = false;
                }

                Log.d("TrainAndTask","itIsHere");

                Video newVideo = new Video();

                newVideo.setTitle(title);
                newVideo.setDescription(description);
                newVideo.setUrl(url_str);
                newVideo.setFilename(filename);
                newVideo.setIsImage(isImage);
                newVideo.setThumbs(thumbs);

                boolean videoExistsInArray = false;

                if (i > 0) {
                    for (Video videoInArray: videosList) {
                        if (videoInArray.getFilename().equals(newVideo.getFilename())) {
                            videoExistsInArray = true;
                        }
                    }
                }
                if (!videoExistsInArray) {
                    videosList.add(newVideo);
                }

                checkStoragePermissions();

                if (type == 3) {
                    sortGifsToArraysFromEvents(data, title);
                } else {
                    showOrHideDoneBtn();
                }

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        }

        if (type == 3) {
            adapter.setContains(trainContains);
        }

    }

    private void sortGifsToArraysFromEvents(JSONObject data, String title) {

        try {
            //String title = data.getString("name");
            String gifUrl = data.getString("gif_url");
            String giffilename = data.getString("gifname");

            int gifrest = 0;
            if (!data.getString("lvac").equals("")) {
                gifrest = Integer.parseInt(data.getString("lvac"));
            }
            int gifcount = data.getInt("lcount");
            int gifmeasure = data.getInt("lmeasure");
            int gifpodh = data.getInt("lpodh");
            int gifboth_place = data.getInt("both_place");

            Gif newGif = new Gif();
            newGif.setFilename(giffilename);
            newGif.setUrl(gifUrl);
            newGif.setTitle(title);
            newGif.setIsBothPlace(gifboth_place);

            //Train contains
            String contain = "";
            if (gifmeasure == 0) {
                contain += "Секунды: " + gifcount + ". ";
            } else {
                contain += "Повторы: " + gifcount + ". ";
            }

            if (gifpodh > 1) {
                if (gifrest > 0) {
                    contain += "Подходы: " + gifpodh + ". Отдых между подходами: " + gifrest + " сек.";
                } else {
                    contain += "Подходы: " + gifpodh + ".";
                }
            }

            TrainContain containObj = new TrainContain();

            containObj.setTitle(title);
            containObj.setContain(contain);

            trainContains.add(containObj);

            if (gifpodh < 2) {
                if (gifrest > 0) {
                    TrainContain restContainObj = new TrainContain();

                    restContainObj.setTitle("Отдых");
                    restContainObj.setContain(gifrest + " сек.");
                }
            }

            trainContains.add(containObj);

            //String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/fetch/gifsAssets/";

            //File file = new File(filePath, newGif.getFilename());

            //if (!file.exists()) {
                boolean gifExistsInArray = false;
                for (Gif gifInArray : gifsToBeDownloaded) {
                    if (gifInArray.getFilename().equals(newGif.getFilename())) {
                        gifExistsInArray = true;
                    }
                }
                if (!gifExistsInArray) {
                    gifsToBeDownloaded.add(newGif);
                }
            //}
            gifsList.add(newGif);
            gifsCount.add(gifcount);
            gifsMeasures.add(gifmeasure);
            gifsPodh.add(gifpodh);
            gifsRest.add(gifrest);

            showOrHideDoneBtn();
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void showOrHideDoneBtn() {
        if (accessLevel < 2) {
            doneBtn.setVisibility(View.GONE);
        } else {
            if (completed == 1) {

                switch (type) {
                    case 1:
                        showCompletedSnackBar("Данное задание Вы уже выполнили");
                        break;
                    case 3:
                        showCompletedSnackBar("Данную тренировку Вы уже выполнили");
                        break;
                    default:
                        break;
                }

                doneBtn.setVisibility(View.GONE);
            } else {
                doneBtn.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showCompletedSnackBar(@NotNull String text) {
        final Snackbar snackbar = Snackbar.make(mainView, text, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("OK", v -> {
            snackbar.dismiss();
        });
        snackbar.show();
    }

    public void doneTapped(View v) {
        Log.d("TrainAndTask", "gifsToDownload: " + gifsToBeDownloaded.size());
        Toast.makeText(getApplicationContext(), "gifsToDownload: " + gifsToBeDownloaded.size(), Toast.LENGTH_LONG);
        v.setEnabled(false);
        switch (type) {
            case 1:
                loadingView.setVisibility(View.VISIBLE);
                completeTask();
                break;
            case 3:

                if (gifsToBeDownloaded.size() > 0) {

                    boolean allGifFilesExistsOnStorage = true;

                    for(Gif gif: gifsToBeDownloaded) {

                        String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/fetch/gifsAssets/";

                        File file = new File(filePath, gif.getFilename());

                        if (!file.exists()) {

                            allGifFilesExistsOnStorage = false;

                            gifsLoadingProcessView.setVisibility(View.INVISIBLE);
                            gifsLoadingProcessViewLabel.setVisibility(View.INVISIBLE);
                            gifsDownloadBtn.setVisibility(View.VISIBLE);
                            gifsLoadingAlert.setVisibility(View.VISIBLE);

                            break;
                        }

                    }

                    if (allGifFilesExistsOnStorage) {
                        startTraining();
                    }


                } else {
                    startTraining();
                }
                break;
            default:
                break;
        }
    }


    public void startTraining() {
        /*if (!isFromTraining) {
            isFromTraining = true;*/

            Intent multipleVideoLoader = new Intent(this, MultipleVideo.class);
            multipleVideoLoader.putParcelableArrayListExtra("gifsList", (ArrayList) gifsList);
            multipleVideoLoader.putIntegerArrayListExtra("gifsCount", (ArrayList<Integer>) gifsCount);
            multipleVideoLoader.putIntegerArrayListExtra("gifsMeasures", (ArrayList<Integer>) gifsMeasures);
            multipleVideoLoader.putIntegerArrayListExtra("gifsPodh", (ArrayList<Integer>) gifsPodh);
            multipleVideoLoader.putIntegerArrayListExtra("gifsRest", (ArrayList<Integer>) gifsRest);
            multipleVideoLoader.putExtra("day", day);
            multipleVideoLoader.putExtra("s_token", s_token);

            gifsToBeDownloaded.clear();
            gifsLoadingAlert.setVisibility(View.INVISIBLE);

            startActivity(multipleVideoLoader);
        //}
    }

    public void downloadGifTapped(View v) {
        v.setEnabled(false);
        laterGifsDownloadBtn.setEnabled(false);

        gifsDownloadBtn.setVisibility(View.INVISIBLE);

        gifsLoadingProcessViewLabel.setVisibility(View.VISIBLE);
        gifsLoadingProcessView.setVisibility(View.VISIBLE);

        v.setEnabled(true);
        laterGifsDownloadBtn.setEnabled(true);

        isGroup = true;
        checkStoragePermissions();
    }

    public void downloadGifLaterTapped(View v) {
        doneBtn.setEnabled(true);

        rxFetch.deleteAll();
        reset();

        gifsLoadingAlert.setVisibility(View.INVISIBLE);
    }


    public void completeTask() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        params.put("event_type", type);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/complete", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            doneBtn.setEnabled(true);
                            //Спрятать кнопку "Выполнить"
                            //Вывести алерт "Данное задание будет считаться выполненным" с одной кнопкой "ОК"
                            //После нажатия "ОК" вызвать finish()

                            loadingView.setVisibility(View.INVISIBLE);

                            AlertDialog.Builder builder = new AlertDialog.Builder(TrainAndTask.this);

                            String message = "Данное задание будет считаться выполненным.";

                            builder.setTitle("Успешно").setMessage(message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            TrainAndTask.this.finish();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }


                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    Log.d("MainActivity:", "" + json);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
            }

            @Override
            public void onRetry(int retryNo) {
            }
        });

    }


    private void showErrID(Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

    @Override
    public void onPauseDownload(int id) {
        fetch.pause(id);
    }

    @Override
    public void onResumeDownload(int id) {
        fetch.resume(id);
    }

    @Override
    public void onRemoveDownload(int id) {
        fetch.remove(id);
    }

    @Override
    public void onRetryDownload(int id) {
        fetch.retry(id);
    }
}