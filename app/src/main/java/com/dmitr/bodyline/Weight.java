package com.dmitr.bodyline;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;
import cz.msebera.android.httpclient.Header;

public class Weight extends AppCompatActivity {

    int accessLevel = 0;
    int day = 0;

    Button weightField;
    LineChart graphView;
    Button saveBtn;

    String token;
    String s_token;

    List<JSONObject> myData = new ArrayList<JSONObject>();

    //ДИАЛОГИ
    ArrayList<String> kgValues = new ArrayList<>();
    ArrayList<String> grValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);

        Typeface lato = Typeface.createFromAsset(getAssets(),
                "fonts/lato_regular.ttf");

        graphView = (LineChart) findViewById(R.id.linechart);

        try {
            myData.add(new JSONObject("{day:1,weight:2}"));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        for (int i = 35; i < 301; i++) {
            kgValues.add(""+i);
        }

        for (int i = 0; i < 10; i++) {
            grValues.add(""+i);
        }

        //lineChartUpdate();

        /*graphView.setBackgroundColor(getResources().getColor(R.color.chart_bg));
        graphView.getDescription().setTextColor(android.R.color.white);
        graphView.getDescription().setTypeface(lato);
        graphView.getDescription().setTextSize(14f);
        graphView.getLegend().setTextColor(android.R.color.white);
        graphView.getLegend().setTypeface(lato);
        graphView.getLegend().setTextSize(14f);

        graphView.getXAxis().setTextColor(android.R.color.white);*/

        graphView.getLegend().setTextColor(ColorTemplate.rgb("#FFFFFF"));
        graphView.getDescription().setTextColor(ColorTemplate.rgb("#FFFFFF"));

        graphView.setTouchEnabled(true);
        graphView.setDragEnabled(true);
        graphView.setScaleEnabled(true);
        graphView.setPinchZoom(true);

        weightField = findViewById(R.id.weight_txt);
        weightField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeightDialog(weightField);
            }
        });

        accessLevel = getIntent().getIntExtra("accessLevel", 0);
        s_token = getIntent().getStringExtra("s_token");
        day = getIntent().getIntExtra("day", 0);

        saveBtn = (Button) findViewById(R.id.saveBtn);
        if (accessLevel < 2) {
            saveBtn.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        runGetDayWeight();
    }

    private void runGetDayWeight() {

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");
        s_token = mSettings.getString("s_token", "");
        getDayWeight(token, s_token, day);
    }

    private void getDayWeight(String token, String s_token, int day) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getdayweight", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("Weight", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                weightField.setText(json.getString("weight"));

                                runGetSeasonWeight();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }


    private void runGetSeasonWeight() {
        getSeasonWeight(token, s_token);
    }

    private void getSeasonWeight(String token, String s_token) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/getseasonweight", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("Weight", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {
                            try {

                                myData.clear();

                                JSONArray weightArray = json.getJSONArray("data");

                                for (int i = 0; i < weightArray.length(); i++) {

                                    myData.add(weightArray.getJSONObject(i));

                                }

                                lineChartUpdate();

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void lineChartUpdate() {

        ArrayList<Entry> myValues = new ArrayList<Entry>();

        for (int i = 0; i < myData.size(); i++) {

            try {
                int day = myData.get(i).getInt("day");
                float weight = new Float(myData.get(i).getDouble("weight"));

                Entry charData = new Entry(day, weight);

                myValues.add(charData);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        LineDataSet myDataSet = new LineDataSet(myValues, "Мой вес");
        myDataSet.setColors(ColorTemplate.rgb("#61B0FF"));
        myDataSet.setValueTextColor(ColorTemplate.rgb("#FFFFFF"));
        myDataSet.setDrawCircles(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(myDataSet);

        LineData data = new LineData(dataSets);

        graphView.setData(data);
        graphView.getDescription().setText("График замеров веса в килограммах");

        //graphView.getData().notifyDataChanged();
        graphView.notifyDataSetChanged();
        graphView.invalidate();
        //graphView.notifyAll();

    }


    public void saveTapped(View v) {
        Button sender = (Button)v;
        sender.setEnabled(false);

        if (!checkForErrors()) {
            runInsertDayWeight();
        } else {
            sender.setEnabled(true);
        }
    }

    private void runInsertDayWeight() {

        insertDayWeight(token, s_token, day, weightField.getText().toString());

    }


    private void insertDayWeight(String token, String s_token, int day, String weight) {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("s_token", s_token);
        params.put("day", day);
        params.put("weight", weight);
        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/insertdayweight", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);

                        Log.d("Weight", json.toString());

                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(Weight.this);
                            String message = "Ваш вес успешно сохранен!";

                            builder.setTitle("Успешно").setMessage(message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Weight.this.finish();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }

                        } else {
                            showErrID(err_id, saveBtn);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private boolean checkForErrors() {

        boolean errors = false;
        String title = "Ошибка";
        String message = "Укажите Ваш ";

         if (weightField.length() == 0) {
            errors = true;
            message += "вес";

            alertWithTitle(title, message, this, true);

        }

        return errors;
    }

    private void alertWithTitle(String title, String message, Context ctx, final boolean needToFocus) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(title).setMessage(message)
                .setCancelable(true)
                .setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        /*if (needToFocus) {
                            toFocus.setFocusableInTouchMode(true);
                            toFocus.requestFocus();
                        } else {
                            //paramsScrollView.fullScroll(ScrollView.FOCUS_UP);
                        }*/

                    }
                });
        AlertDialog alert = builder.create();
        if (isFinishing()) {
            alert.show();
        }

    }

    private void showErrID(Integer err_id, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        if (isFinishing()) {
            alert.show();
        }

        button.setEnabled(true);
    }

    private void showWeightDialog(Button btn) {

        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_weight, null);

        //final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);

        final NumberPickerView kgPicker = dialogView.findViewById(R.id.kg);
        final NumberPickerView grPicker = dialogView.findViewById(R.id.gr);

        setData(kgPicker, kgValues);
        setData(grPicker, grValues);

        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHING

                String kg = kgValues.get(kgPicker.getValue()) + "." + grValues.get(grPicker.getValue());
                btn.setText(kg);

                dialogBuilder.dismiss();
            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();

    }

    private void setData(NumberPickerView picker, ArrayList<String> valuesList){
        String[] values = new String[valuesList.size()];

        for (int i = 0; i < values.length; i++) {
            String str = valuesList.get(i);
            values[i] = str;
        }

        picker.setDisplayedValues(values);
        picker.setMinValue(0);
        picker.setMaxValue(values.length - 1);
        picker.setValue(0);
    }

}

