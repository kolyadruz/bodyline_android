package com.dmitr.bodyline.support;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmitr.bodyline.R;

import java.util.List;

public class SupportAdapter extends BaseAdapter {

    private Activity activity;

    private List<Support> supports;

    private int layoutID;
    private LayoutInflater inflater = null;

    public SupportAdapter(Activity activity, List<Support> supports, int layoutID) {
        this.activity = activity;
        this.supports = supports;
        this.layoutID = layoutID;

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() { return supports.size(); }

    public Object getItem(int position) { return position; }

    public long getItemId(int position) { return position; }

    public View getView(final int position, View editingCell, ViewGroup parent) {

        View cellView = editingCell;
        SupportsListCell supportsListCell;
        if (editingCell == null) {
            cellView = inflater.inflate(layoutID, null);
            supportsListCell = new SupportsListCell(cellView);
            cellView.setTag(supportsListCell);
        } else {
            supportsListCell = (SupportsListCell) cellView.getTag();
        }

        final Support support = supports.get(position);

        if (!support.isAnswered) {
            supportsListCell.quesSuppTopCell.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppDate.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppBottomCell.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppAnsLine.setVisibility(View.INVISIBLE);
            supportsListCell.ansSuppTopCell.setVisibility(View.INVISIBLE);
            supportsListCell.answerSuppBottomCell.setVisibility(View.INVISIBLE);
            supportsListCell.ansSuppDate.setVisibility(View.INVISIBLE);
        } else {
            supportsListCell.quesSuppTopCell.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppDate.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppBottomCell.setVisibility(View.VISIBLE);
            supportsListCell.quesSuppAnsLine.setVisibility(View.VISIBLE);
            supportsListCell.ansSuppTopCell.setVisibility(View.VISIBLE);
            supportsListCell.answerSuppBottomCell.setVisibility(View.VISIBLE);
            supportsListCell.ansSuppDate.setVisibility(View.VISIBLE);
        }

        supportsListCell.quesSuppDate.setText(support.questionDate);
        supportsListCell.quesSuppBottomCell.setText(support.question);
        supportsListCell.ansSuppDate.setText(support.answerDate);
        supportsListCell.answerSuppBottomCell.setText(support.answer);

        return cellView;
    }

    class SupportsListCell {
        public TextView quesSuppTopCell;
        public TextView quesSuppDate;
        public TextView quesSuppBottomCell;
        public TextView ansSuppTopCell;
        public TextView quesSuppAnsLine;
        public TextView answerSuppBottomCell;
        public TextView ansSuppDate;

        public SupportsListCell(View base) {
            quesSuppTopCell = (TextView) base.findViewById(R.id.quesSuppTopCell);
            quesSuppDate = (TextView) base.findViewById(R.id.quesSuppDate);
            quesSuppBottomCell = (TextView) base.findViewById(R.id.quesSuppBottomCell);
            ansSuppTopCell = (TextView) base.findViewById(R.id.ansSuppTopCell);
            quesSuppAnsLine = (TextView) base.findViewById(R.id.quesSuppAnsLine);
            answerSuppBottomCell = (TextView) base.findViewById(R.id.answerSuppBottomCell);
            ansSuppDate = (TextView) base.findViewById(R.id.ansSuppDate);
        }

    }

}
