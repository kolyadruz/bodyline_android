package com.dmitr.bodyline.support;

public class Support{

    String question;
    String questionDate;
    String answer;
    String answerDate;
    boolean isAnswered;

    public Support(){

    }

    public Support(String question, String questionDate, String answer, String answerDate, boolean isAnswered){


        this.question = question;
        this.questionDate = questionDate;
        this.answer = answer;
        this.answerDate = answerDate;
        this.isAnswered = isAnswered;

    }
}
