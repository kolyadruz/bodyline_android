package com.dmitr.bodyline;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cunoraz.gifview.library.GifView;
import com.dmitr.bodyline.trainandtask.Gif;
import com.dmitr.bodyline.trainandtask.GifPlayerTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class MultipleVideo extends AppCompatActivity {

    String token = "";
    String s_token = "";
    int day = 0;

    //Timer
    Timer timer;// = new Timer();
    TimerTask mPLayWithInterval = new playWithInterval();

    @BindView(R.id.gifPlayerLayer)
    GifView gifPlayerLayer;
    @BindView(R.id.gifPlayerProgress)
    ProgressBar gifPlayerProgress;
    @BindView(R.id.gifPlayerProgressLabel)
    TextView gifPlayerProgressLabel;

    @BindView(R.id.playBtnImg)
    ImageView playBtnImg;

    @BindView(R.id.controlBtn)
    Button controlBtn;
    @BindView(R.id.nextBtn)
    ImageButton nextBtn;
    @BindView(R.id.prevBtn) ImageButton prevBtn;

    @BindView(R.id.countProgress) ProgressBar countProgress;
    @BindView(R.id.countLabel) TextView countLabel;

    @BindView(R.id.titleLabel) TextView titleLabel;

    //Dialogs
    @BindView(R.id.confirmExitView)
    RelativeLayout confirmExitView;
    @BindView(R.id.confirmCompleteView) RelativeLayout confirmCompleteView;
    @BindView(R.id.repeatBtn) Button repeatBtn;

    //Bind confirmExitView
    //Bind confirmCompleteView
    //Bind repeatBtn
    //Bind completeBtn

    //Variables
    int playingIndex = 0;
    boolean isGIF = false;
    boolean isPlaying = false;
    boolean isStart = true;
    int restTime = 0;
    int prepareTime = 3;

    Long currentTime = new Long(0);
    Long currentMax = new Long(0);
    Long secondsLeft = new Long(0);

    //Данные GIFок;
    ArrayList<Gif> gifsList = new ArrayList<Gif>();
    ArrayList<Integer> gifsCount = new ArrayList<Integer>();
    ArrayList<Integer> gifsMeasure = new ArrayList<Integer>();
    ArrayList<Integer> gifsPodh = new ArrayList<Integer>();

    ArrayList<Integer> gifsRest = new ArrayList<Integer>();

    ArrayList<GifPlayerTask> playList = new ArrayList<GifPlayerTask>();

    //String localFilePath = "/storage/sdcard0/okhttp/download/";
    //String localFilePath = Environment.getExternalStorageDirectory() + "/okhttp/download/";
    String localFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/fetch/gifsAssets/";

    @BindView(R.id.switchSideAlertLabel) TextView switchSideAlertLabel;

    @BindView(R.id.nextLabel) TextView nextLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplevideo);
        ButterKnife.bind(this);

        gifPlayerProgress.setProgress(0);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        gifPlayerLayer.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        gifsList = getIntent().getParcelableArrayListExtra("gifsList");
        gifsCount = getIntent().getIntegerArrayListExtra("gifsCount");
        gifsMeasure = getIntent().getIntegerArrayListExtra("gifsMeasures");
        gifsPodh = getIntent().getIntegerArrayListExtra("gifsPodh");
        gifsRest = getIntent().getIntegerArrayListExtra("gifsRest");

        day = getIntent().getIntExtra("day", 0);
        s_token = getIntent().getStringExtra("s_token");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = mSettings.getString("token", "");

        scheduleStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void scheduleStart() {
        //Очистка плейлиста
        playList.clear();

        //Добавление приготовьтесь
        String firstName = gifsList.get(0).getFilename();
        GifPlayerTask prepareTask = new GifPlayerTask();
        prepareTask.setTitle("Приготовьтесь");
        prepareTask.setTime(new Long(3000));
        prepareTask.setFileName(firstName);
        prepareTask.setIsAnimation(false);
        prepareTask.setIsSecondFromBoth(false);

        playList.add(prepareTask);

        for(int i = 0; i < gifsList.size(); i++) {

            Gif gif = gifsList.get(i);

            String title = gif.getTitle();
            String filename = gif.getFilename();
            Long time = new Long(gifsCount.get(i) * 1000);
            Integer measure = gifsMeasure.get(i);
            String url = gif.getUrl();
            Boolean isBoth = (gif.getIsBothPLace() == 1);

            if (measure == 1) {
                Long temp = time;
                time = (new Double(temp * 1.5)).longValue();
            }

            Integer podh = gifsPodh.get(i);

            for (int p = 1; p < podh + 1; p++) {

                if (isBoth) {
                    //Два подряд
                    String titleMod = title + ". Подход " + p + " из " + podh + " (левая сторона)";

                    GifPlayerTask gifTask1 = new GifPlayerTask();
                    gifTask1.setTitle(titleMod);
                    gifTask1.setFileName(filename);
                    gifTask1.setTime(time);
                    gifTask1.setUrl(url);
                    gifTask1.setIsAnimation(true);
                    gifTask1.setIsSecondFromBoth(false);

                    String titleMod2 = title + ". Подход " + p + " из " + podh + " (правая сторона)";

                    GifPlayerTask gifTask2 = new GifPlayerTask();
                    gifTask2.setTitle(titleMod2);
                    gifTask2.setFileName(filename);
                    gifTask2.setTime(time);
                    gifTask2.setUrl(url);
                    gifTask2.setIsAnimation(true);
                    gifTask2.setIsSecondFromBoth(true);

                    playList.add(gifTask1);
                    playList.add(gifTask2);

                } else {
                    //ОДНО УПРАЖНЕНИЕ

                    String titleMod = title + ". Подход " + p + " из " + podh;

                    GifPlayerTask gifTaskSingle = new GifPlayerTask();
                    gifTaskSingle.setTitle(titleMod);
                    gifTaskSingle.setFileName(filename);
                    gifTaskSingle.setTime(time);
                    gifTaskSingle.setUrl(url);
                    gifTaskSingle.setIsAnimation(true);
                    gifTaskSingle.setIsSecondFromBoth(false);

                    playList.add(gifTaskSingle);
                }

                //ОТДЫХ

                int restTime = gifsRest.get(i);

                if (restTime != 0) {
                    if (p < podh) {
                        String resturl = gifsList.get(i).getUrl();
                        GifPlayerTask gifTaskRest = new GifPlayerTask();
                        gifTaskRest.setTitle("Отдых");
                        gifTaskRest.setFileName(filename);
                        gifTaskRest.setTime(new Long(restTime*1000));
                        gifTaskRest.setUrl(resturl);
                        gifTaskRest.setIsAnimation(false);
                        gifTaskRest.setIsSecondFromBoth(false);
                        playList.add(gifTaskRest);
                    } else if (i < gifsList.size() - 1){
                        String resturl = gifsList.get(i + 1).getUrl();
                        GifPlayerTask gifTaskRest = new GifPlayerTask();
                        gifTaskRest.setTitle("Отдых");
                        gifTaskRest.setFileName(filename);
                        gifTaskRest.setTime(new Long(restTime*1000));
                        gifTaskRest.setUrl(resturl);
                        gifTaskRest.setIsAnimation(false);
                        gifTaskRest.setIsSecondFromBoth(false);
                        playList.add(gifTaskRest);
                    }
                }

            }

        }

        currentMax = playList.get(0).getTime();

        playingIndex = 0;
        secondsLeft = new Long(0);

        //gifPlayerProgressMax = currentMax;
        gifPlayerProgressLabel.setText("0");
        gifPlayerProgress.setProgress(0);

        nextBtn.setVisibility(View.VISIBLE);
        countLabel.setText(""+(playingIndex + 1) + " из " + playList.size());
        countProgress.setProgress(playingIndex + 1);
        countProgress.setMax(playList.size());

        setSingleImage();

        titleLabel.setText("Приготовьтесь");

        if (timer == null) {
            timer = new Timer();
            timer.schedule(mPLayWithInterval, 0, 1000);
        }

        isPlaying = true;
    }

    class playWithInterval extends TimerTask {

        @Override
        public void run() {

            MultipleVideo.this.runOnUiThread(setTimerRunnable);

        }
    }

    final Runnable setTimerRunnable = new Runnable() {
        public void run() {
            secondsLeft = currentMax - currentTime;
            gifPlayerProgressLabel.setText(""+Math.round(secondsLeft/1000));
            gifPlayerProgress.setProgress(currentTime.intValue());

            if (currentTime < currentMax) {
                currentTime += 1000;
            } else {
                stepNext();
            }
        }
    };

    private void setTimer() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        currentTime = new Long(0);
        gifPlayerProgress.setProgress(0);
        gifPlayerProgressLabel.setText("0");

        GifPlayerTask currentGifTask = playList.get(playingIndex);

        currentMax = currentGifTask.getTime();
        gifPlayerProgress.setMax(currentMax.intValue());

        if (currentGifTask.isAnimation) {

            setGif();

        } else {

            setSingleImage();

        }

        setCountProgress();

        gifPlayerProgressLabel.setText(""+Math.round(currentGifTask.getTime()/1000));

        countLabel.setText(""+ (playingIndex+1) + " из " + playList.size());

        timer = new Timer();
        mPLayWithInterval = new playWithInterval();
        timer.schedule(mPLayWithInterval, 0, 1000);

    }

    private void setCountProgress() {
        countProgress.setProgress(playingIndex + 1);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        currentTime = new Long(0);
        gifPlayerProgress.setProgress(0);
        gifPlayerProgressLabel.setText("0");

        setSingleImage();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        pause();
        confirmExitView.setVisibility(View.VISIBLE);
    }

    public void cancelExitTapped(View v) {
        confirmExitView.setVisibility(View.INVISIBLE);
    }

    public void acceptExitTapped(View v) {
        confirmExitView.setVisibility(View.INVISIBLE);
        finish();
    }

    public void completedTapped(View v) {
        v.setEnabled(false);

        repeatBtn.setEnabled(false);

        runCompleteTask();

    }

    public void repeatTapped(View v) {

        playingIndex = 0;
        stopTimer();

        nextBtn.setVisibility(View.VISIBLE);
        prevBtn.setVisibility(View.INVISIBLE);

        confirmCompleteView.setVisibility(View.INVISIBLE);

        setTimer();

    }

    private void playOrPause() {
        if (isPlaying) {
            pause();
        } else {
            play();
        }
    }

    private void play() {
        isPlaying = true;
        timer = new Timer();
        mPLayWithInterval = new playWithInterval();

        timer.schedule(mPLayWithInterval, 0, 1000);

        playBtnImg.setVisibility(View.INVISIBLE);
        gifPlayerProgressLabel.setVisibility(View.VISIBLE);

        GifPlayerTask currentGifTask = playList.get(playingIndex);
        if (currentGifTask.isAnimation) {
            setGif();
        } else {
            setSingleImage();
        }
    }

    private void pause() {
        isPlaying = false;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        playBtnImg.setVisibility(View.VISIBLE);
        gifPlayerProgressLabel.setVisibility(View.INVISIBLE);

        setSingleImage();
    }

    private void setSingleImage() {

        GifPlayerTask currentGifTask = playList.get(playingIndex);

        String fileName = currentGifTask.fileName;
        titleLabel.setText(currentGifTask.getTitle());

        if (currentGifTask.isSecondFromBoth) {
            switchSideAlertLabel.setVisibility(View.VISIBLE);
        } else {
            switchSideAlertLabel.setVisibility(View.INVISIBLE);
        }

        if (playingIndex < playList.size() - 1) {
            nextLabel.setText("Далее: "+playList.get(playingIndex + 1).getTitle());
        } else {
            nextLabel.setText("Далее: Завершение тренировки");
        }

        File file = new File(localFilePath, fileName);
        if (file.exists()) {
            Log.d("clicked filepath", localFilePath+fileName);
            final int readLimit = 16 * 1024;
            try {
                InputStream fileInputStream = new BufferedInputStream(new FileInputStream(file), readLimit);
                fileInputStream.mark(readLimit);
                gifPlayerLayer.setStream(fileInputStream);
                gifPlayerLayer.pause();
                fileInputStream.close();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setGif() {

        GifPlayerTask currentGifTask = playList.get(playingIndex);

        String fileName = currentGifTask.fileName;
        titleLabel.setText(currentGifTask.getTitle());

        if (currentGifTask.isSecondFromBoth) {
            switchSideAlertLabel.setVisibility(View.VISIBLE);
        } else {
            switchSideAlertLabel.setVisibility(View.INVISIBLE);
        }

        if (playingIndex < playList.size() - 1) {
            nextLabel.setText("Далее: "+playList.get(playingIndex + 1).getTitle());
        } else {
            nextLabel.setText("Далее: Завершение тренировки");
        }

        File file = new File(localFilePath, fileName);
        if (file.exists()) {
            Log.d("clicked filepath", localFilePath+fileName);
            final int readLimit = 16 * 1024;
            try {
                InputStream fileInputStream = new BufferedInputStream(new FileInputStream(file), readLimit);
                fileInputStream.mark(readLimit);
                gifPlayerLayer.setStream(fileInputStream);
                gifPlayerLayer.play();
                fileInputStream.close();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playTapped(View v) {
        playOrPause();
    }

    private void stepNext() {
        playBtnImg.setVisibility(View.INVISIBLE);
        gifPlayerProgressLabel.setVisibility(View.VISIBLE);

        prevBtn.setVisibility(View.VISIBLE);
        if (playingIndex < (playList.size() -1)) {
            playingIndex += 1;
            setTimer();
        } else {
            nextBtn.setVisibility(View.INVISIBLE);
            stopTimer();
            titleLabel.setText("Завершение тренировки");
            confirmCompleteView.setVisibility(View.VISIBLE);
        }
    }

    public void nextTapped(View v) {
        stepNext();
    }

    public void prevTapped(View v) {
        playBtnImg.setVisibility(View.INVISIBLE);
        gifPlayerProgressLabel.setVisibility(View.VISIBLE);

        if (playingIndex > 0) {
            playingIndex -= 1;
            setTimer();
        } else {
            v.setVisibility(View.INVISIBLE);
        }
    }

    private void runCompleteTask() {

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("event_type", "3");
        params.put("event_reason", "0");
        params.put("day", day);
        params.put("s_token", s_token);

        client.addHeader("Authorization","Bearer "+token);
        client.post("http://bodyline14.ru/mobile/complete", params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // called when response HTTP status is "200 OK"
                if (statusCode == 200) {
                    String str = new String(responseBody);
                    JSONObject json = new JSONObject();
                    try {
                        json = new JSONObject(str);
                        Integer err_id = json.getInt("err_id");
                        if (err_id == 0) {


                            AlertDialog.Builder builder = new AlertDialog.Builder(MultipleVideo.this);

                            String message = "Данное задание будет считаться выполненным.";

                            builder.setTitle("Успешно").setMessage(message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            MultipleVideo.this.finish();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }

                        } else {
                            showErrID(err_id);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    Log.d("MultipleVideoActivity:", "" + json);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
            }

            @Override
            public void onRetry(int retryNo) {
            }
        });

    }

    private void showErrID(Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

}