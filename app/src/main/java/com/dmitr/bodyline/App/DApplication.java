package com.dmitr.bodyline.App;

import android.app.Application;

import com.dmitr.bodyline.BuildConfig;
import com.dmitr.bodyline.Constants;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.HttpUrlConnectionDownloader;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;
import com.tonyodev.fetch2rx.RxFetch;

import java.io.File;
import java.net.URISyntaxException;

import im.ene.toro.exoplayer.Config;
import im.ene.toro.exoplayer.ExoCreator;
import im.ene.toro.exoplayer.MediaSourceBuilder;
import im.ene.toro.exoplayer.ToroExo;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;
import timber.log.Timber;


public class DApplication extends Application {

    Long cacheFile =  new Long(2 * 1024 * 1024);
    DApplication dApplication;
    Config config;
    static ExoCreator exoCreator;

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.CHAT_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        dApplication = this;

        SimpleCache cache = new SimpleCache( new File(getFilesDir().getPath() + "/toro_cache"), new LeastRecentlyUsedCacheEvictor(cacheFile));

        config = new Config.Builder().setMediaSourceBuilder(MediaSourceBuilder.LOOPING).setCache(cache).build();

        exoCreator = ToroExo.with(this).getCreator(config);

        final FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .enableRetryOnNetworkGain(true)
                .setDownloadConcurrentLimit(3)
                .setHttpDownloader(new HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
                // OR
                //.setHttpDownloader(getOkHttpDownloader())
                .build();
        Fetch.Impl.setDefaultInstanceConfiguration(fetchConfiguration);
        RxFetch.Impl.setDefaultRxInstanceConfiguration(fetchConfiguration);

    }

    public static ExoCreator getExoCreator() {
        return exoCreator;
    }

    private OkHttpDownloader getOkHttpDownloader() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        return new OkHttpDownloader(okHttpClient,
                Downloader.FileDownloaderType.PARALLEL);
    }
}