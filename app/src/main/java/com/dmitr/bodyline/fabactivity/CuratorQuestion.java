package com.dmitr.bodyline.fabactivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;
import cz.msebera.android.httpclient.Header;

public class CuratorQuestion extends AppCompatActivity implements NumberPickerView.OnValueChangeListener {

    String token;
    String s_token = "";

    NumberPickerView quesCatPicker;

    ArrayList<String> daysArrayTitle = new ArrayList<>(Arrays.asList("Задания", "Питание", "Тренировки", "Замеры"));
    ArrayList<Integer> daysArrayValue = new ArrayList<>(Arrays.asList(0,3));

    Integer quesCatId = 1;

    EditText qeusText;

    Button sendBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curator_question);

        s_token = getIntent().getStringExtra("s_token");
        Log.d("s_token", ""+s_token);

        qeusText = (EditText) findViewById(R.id.quesText);

        sendBtn = (Button) findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qeusText.length() == 0) {
                    showMessageIsEmpty();
                } else {
                    sendBtn.setEnabled(false);
                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    token = mSettings.getString("token", "");
                    final AlertDialog.Builder builder = new AlertDialog.Builder(CuratorQuestion.this);
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("question", qeusText.getText().toString());
                    params.put("s_token", s_token);
                    params.put("category", ""+quesCatId);
                    client.addHeader("Authorization","Bearer "+token);
                    client.post("http://bodyline14.ru/mobile/questionteacher", params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    sendBtn.setEnabled(true);
                                    if (statusCode == 200) {
                                        String str = new String(responseBody);
                                        JSONObject json = new JSONObject();
                                        try {
                                            json = new JSONObject(str);
                                            Integer err_id = json.getInt("err_id");
                                            if (err_id == 0) {

                                                String Message = "Сообщение отправлено";
                                                builder.setMessage(Message)
                                                        .setCancelable(false)
                                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                finish();
                                                            }
                                                        });

                                                AlertDialog alert = builder.create();
                                                if (!isFinishing()) {
                                                    alert.show();
                                                }
                                            } else {
                                                showErrID(err_id);
                                            }

                                        } catch (Exception ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                }
                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    String Message = "Интернет соединение отсутствует";
                                    builder.setMessage(Message)
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    sendBtn.setEnabled(true);
                                                }
                                            });

                                    AlertDialog alert = builder.create();
                                    if (!isFinishing()) {
                                        alert.show();
                                    }
                                }
                            }
                    );


                }
            }
        });

        quesCatPicker = (NumberPickerView)findViewById(R.id.textQuesCat);

        quesCatPicker.setOnValueChangedListener(this);

        constructDays();
    }



    public void showMessageIsEmpty() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String Message = "Сообщение не может быть пустым";
        builder.setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }


    @Override
    public void onValueChange(NumberPickerView picker, int oldVal, int newVal) {

            quesCatId = newVal;

        Log.d("QuesCat","after valueChanged: category_id: " + quesCatId);

    }

    private void setData(NumberPickerView picker, int minValue, int maxValue){
        String[] values = {"Задания", "Питание", "Тренировки", "Замеры"};

        Log.d("QuesCat","setData: day_id: " + quesCatId);

        if (picker == quesCatPicker) {
            values = new String[maxValue-minValue+1];
            int startIndex = daysArrayValue.indexOf(minValue);

            for (int i = 0; i < values.length; i++) {
                Integer titleIndex = i + startIndex;
                String str = daysArrayTitle.get(titleIndex);
                values[i] = str;
            }
            Log.d("QuesCat","setData: dayPicker: day_id: " + quesCatId);

        }

        picker.setMinValue(0);
        picker.setDisplayedValues(values);

        picker.setMaxValue(3);
    }

    public void constructDays() {
            quesCatId = 2;
            setData(quesCatPicker, 0, 3);
    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("CuratorQuestion:", "ошибка"+err_id);
                        sendBtn.setEnabled(true);
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }
}





