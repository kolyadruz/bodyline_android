package com.dmitr.bodyline.fabactivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.dmitr.bodyline.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SupportQuestion extends AppCompatActivity {

    String token;

    EditText suppText;

    Button sendBtnSupp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_question);

        suppText = (EditText) findViewById(R.id.suppText);


        sendBtnSupp = (Button) findViewById(R.id.sendBtnSupp);

        sendBtnSupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (suppText.length()== 0) {
                    showMessageIsEmpty();
                } else {
                    sendBtnSupp.setEnabled(false);
                    SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    token = mSettings.getString("token", "");
                    final AlertDialog.Builder builder = new AlertDialog.Builder(SupportQuestion.this);
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("question", suppText.getText().toString());
                    client.addHeader("Authorization","Bearer "+token);
                    client.post("http://bodyline14.ru/mobile/questionsupport", params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            sendBtnSupp.setEnabled(true);
                            if (statusCode == 200) {
                                String str = new String(responseBody);
                                JSONObject json = new JSONObject();
                                try {
                                    json = new JSONObject(str);
                                    Integer err_id = json.getInt("err_id");
                                    if (err_id == 0) {

                                        String Message = "Сообщение отправлено";
                                        builder.setMessage(Message)
                                                .setCancelable(false)
                                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        finish();
                                                    }
                                                });

                                        AlertDialog alert = builder.create();
                                        if (!isFinishing()) {
                                            alert.show();
                                        }
                                    } else {
                                        showErrID(err_id);
                                    }

                                } catch (Exception ex){
                                    ex.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            String Message = "Интернет соединение отсутствует";
                            builder.setMessage(Message)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            sendBtnSupp.setEnabled(true);
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            if (!isFinishing()) {
                                alert.show();
                            }
                        }
                    });
                }
            }
        });
    }

    public void showMessageIsEmpty() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String Message = "Сообщение не может быть пустым";
        builder.setMessage(Message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

    private void showErrID(final Integer err_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        String message = "";

        if (err_id == 1) {
            message = "Аккаунт заблокирован";
        } else if (err_id == 2) {
            message = "Токен не найден";
        } else if (err_id == 3) {
            message = "Данные неверны";
        } else if (err_id == 4) {
            message = "Заполните все поля";
        } else if (err_id == 5) {
            message = "Пользователь уже зарегистрирован в системе";
        } else if (err_id == 6) {
            message = "Ошибка работы системы";
        } else if (err_id == 7) {
            message = "Смс можно отправить раз в 5 минут";
        } else if (err_id == 8) {
            message = "В доступе отказано";
        } else if (err_id == 9) {
            message = "Пользователь не найден";
        } else if (err_id == 10) {
            message = "Ошибка загрузки файла";
        } else if (err_id == 11) {
            message = "Сезон уже приобретен вами";
        } else if (err_id == 12) {
            message = "День недоступен (прошедший или грядущий)";
        } else if (err_id == 13) {
            message = "Вы можете задать только 1 вопрос в день";
        } else if (err_id == 14) {
            message = "Версия приложения устарела";
        }

        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("CuratorQuestion:", "ошибка"+err_id);
                        sendBtnSupp.setEnabled(true);
                    }
                });
        AlertDialog alert = builder.create();
        if(!isFinishing()) {
            alert.show();
        }
    }
}





