package com.dmitr.bodyline.measure;

import com.google.android.exoplayer2.ExoPlayer;

public interface PlayerCallback {

    /**
     * @param playWhenReady Whether playback will proceed when ready.
     * @param playbackState One of the {@link State} constants defined in the {@link ExoPlayer}
     * interface.
     */
    void onPlayerStateChanged(boolean playWhenReady, @State int playbackState);

    /**
     * Invoked when an error occurs. The playback state will transition to {@link
     * ExoPlayer#STATE_IDLE} immediately after this method is invoked. The player instance can still
     * be used, and {@link ExoPlayer#release()} must still be called on the player should it no longer
     * be required.
     *
     * @param error The error.
     */
    boolean onPlayerError(Exception error);

}
