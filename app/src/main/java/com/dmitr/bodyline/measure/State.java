package com.dmitr.bodyline.measure;

import androidx.annotation.IntDef;

import com.google.android.exoplayer2.ExoPlayer;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by eneim on 6/9/16.
 */
@IntDef({
        /**
         * The player is neither prepared or being prepared.
         */
        ExoPlayer.STATE_IDLE,
        /**
         * The player is prepared but not able to immediately play from the current position. The cause
         * is {@link TrackRenderer} specific, but this state typically occurs when more data needs
         * to be buffered for playback to start.
         */
        ExoPlayer.STATE_BUFFERING,
        /**
         * The player is prepared and able to immediately play from the current position. The player will
         * be playing if {@link #getPlayWhenReady()} returns true, and paused otherwise.
         */
        ExoPlayer.STATE_READY,
        /**
         * The player has finished playing the media.
         */
        ExoPlayer.STATE_ENDED
}) @Retention(RetentionPolicy.SOURCE) public @interface State {
}