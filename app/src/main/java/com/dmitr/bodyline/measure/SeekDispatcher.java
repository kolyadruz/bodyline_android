package com.dmitr.bodyline.measure;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;

public interface SeekDispatcher {

    /**
     * @param player The player to seek.
     * @param windowIndex The index of the window.
     * @param positionMs The seek position in the specified window, or {@link C#TIME_UNSET} to seek
     *     to the window's default position.
     * @return True if the seek was dispatched. False otherwise.
     */
    boolean dispatchSeek(ExoPlayer player, int windowIndex, long positionMs);

}