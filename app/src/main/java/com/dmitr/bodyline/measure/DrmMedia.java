package com.dmitr.bodyline.measure;

public interface DrmMedia {

    String getType();

    String getLicenseUrl();

    String[] getKeyRequestPropertiesArray();

}
